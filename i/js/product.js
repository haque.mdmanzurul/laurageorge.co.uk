function switchImage(id, pid)
{
	$("#mainProductImage").attr('src', 'i/products/'+pid+'/'+id+'_t.jpg');
	$("#mainProductEnlarge").attr('href', 'i/products/'+pid+'/'+id+'_b.jpg');
	
	$("#colourSelect").val(id);
}

$(function() {
	$("#colourSelect").change(function() {
		switchImage($(this).val(), PID);
	});
});

$(function(){
	$.superbox();
});
