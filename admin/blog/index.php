<?php require_once("../_header.php"); ?>
<h1>Blog Entries</h1>
<?php
$Query = @mysql_query("SELECT * FROM `blog` ORDER BY `date` DESC");
if(mysql_num_rows($Query)!=0)
{
	echo '<table width="" border="0" cellspacing="0" cellpadding="0" id="tlist">
	<tr>
	<th width="400">Title</th>
	<th width="200">Date Posted</th>
	<th width="150">Action</th>
	</tr>';

	while($Row = mysql_fetch_array($Query))
	{
		echo '<tr>';
		echo '<td>'.htmlentities(stripslashes($Row['title'])).'</td>';
		echo '<td>'.date('d/m/Y @ h:i', $Row['date']).'</td>';
		echo '<td><a href="edit.php?id='.$Row['id'].'">[Edit]</a> | <a href="delete.php?id='.$Row['id'].'" onclick="return confirm(\'Are you sure you want to delete?\')">[Delete]</a></td>';
		echo '</td>';
	}
	
	echo '</table>';
}
else {
	echo '<p>Woops! You have no blog posts.</p>';
}

echo '<h1>Add a new Blog Post</h1>Click <a href="add.php">here</a> to add a new blog post.';

?>

<?php require_once("../_footer.php"); ?>
