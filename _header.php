<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $_TITLE; ?> | Laura George</title>
<meta name="keywords" content="<?php echo $_KEYWORDS; ?>" />
<meta name="description" content="<?php echo $_DESCRIPTION; ?>" />

<meta name="norton-safeweb-site-verification" 
content="ei7xxa2-f5w-isvrs1u4si2ablt7mk91w6awdv5pwieivrsice7rnsz9zurwvv0w72wj899g91r-95kxu6fa0l1m4q7i4spn-ejlt6ojv2xchqx0n0gyoccn0zanb6lh" />

<?php echo $_JS; ?>
<?php echo $_CSS; ?>
</head>

<body>
<div id="wrap">
<a href="/"><img src="/i/body/logo.gif" alt="Laura George" width="442" height="73" /></a>
<div id="mailinglist">
	<form action="http://email.newsletterhub.co.uk/email/subscribe.php" method="post">
		join our mailing list
		<input type="text" name="FormValue_Fields[EmailAddress]" value="" id="FormValue_EmailAddress"><input name="FormButton_Subscribe" type="image" value="Subscribe" src="/i/body/gobtn.gif" class="clear" style="position:absolute;height:auto;width:auto;" />
		<input type="hidden" name="FormValue_ListID" value="70" id="FormValue_ListID">
		<input type="hidden" name="FormValue_Command" value="Subscriber.Add" id="FormValue_Command">
	</form>
</div>

<ul id="navigation">
	<li><a href="/collections.html">collections</a></li>
	<li><a href="/about.html">about us</a></li>
	<li><a href="/contact.html">contact us</a></li>
	<li><a href="/news/press.html">news</a></li>
	<li><a href="/information/">info</a></li>
	<li><a href="/blog.html">Blog</a></li>
	<li><a href="/stockists.html">stockists</a></li>
	<li class="end"><a href="/brochure/" target='_lb'>Lookbook</a></li>
</ul>

<img src="/i/body/header_mini.jpg" width="885" height="128" alt="Laura George" />

<div id="left">

	
<?php
	$SideLinks['/collections.html'] = 'Collections:';
	$LinkQuery = @mysql_query("SELECT * FROM `categories` ORDER BY `order`");
	while($LinkRow = @mysql_fetch_assoc($LinkQuery))
	{
		$LinkRow = makeW3C($LinkRow);
		$SideLinks['/collections.html?id=' . $LinkRow['id']] = $LinkRow['name'];
	}
	
	showSideLinks($SideLinks, 1);
	$SideLinks=NULL;
?>
	
<?php
	$SideLinks = array("/about.html"=>"About Us",
					   "/contact.html"=>"Contact Us",
					   "/news/"=>array(
					   ""=>"News",
					   "/news/press.html"=>"Press",
					   "/news/events.html"=>"Trends",
					   "/news/testimonials.html"=>"Testimonials"),
					   "/information/"=>array(
					   ""=>"Info",
					   "/information/fitting_and_care.html"=>"Fitting &amp; Care",
					   "/information/history_and_tradition.html"=>"History &amp; Tradition",
					   "/information/couture.html"=>"Couture"),
					   "/stockists.html"=>"Stockists");
	
	showSideLinks($SideLinks);
?>

<?php
	if(isset($_SESSION['basket']) && strstr($_SERVER['REQUEST_URI'], 'basket')==false)
	{
		echo '<h2 class="darker" style="font-size:13px;border-bottom:3px solid #EEE;">SHOPPING BASKET</h2>';
		$x = 1;
		foreach($_SESSION['basket'] as $ProductID => $Product)
		{				
			$GetProduct = mysql_query("SELECT * FROM `products` WHERE `id` = '" . $ProductID . "'");
			if(mysql_num_rows($GetProduct) != 0)
			{
				$ProductRow = mysql_fetch_assoc($GetProduct);
				$ProductRow = makeW3C($ProductRow);
				
				foreach($Product as $Product2)
				{
					echo '<div class="leftprod">';
					echo '<a href="product.html?id='.$ProductID.'"><img src="/i/products/' . $ProductID . '/'.$Product2['Colour'].'_t.jpg" alt="'.$ProductRow['name'].'" width="130" /></a>';
					echo '<h3 class="darker">' . $ProductRow['name'] . '</h3><span>&pound;' . number_format($ProductRow['price'], 2) . '</span>';
					echo '<br />Quantity: ' . $Product2['Quantity'];
					
					break;
				}
				
				echo '</div>';
				
				if($x==2) { break; }
			}
			$x++;
		}
		foreach($_SESSION['basket'] as $ProductID => $Product)
		{
			foreach($Product as $Product2)
			{
				$TotalProductsPrice2 += ($Product2['Quantity'] * $ProductRow['price']);
				$TotalProducts2 += $Product2['Quantity'];
			}
		}
		
		echo '<p align="center" style="font-weight:bold;">' . $TotalProducts2 . ' item(s) - &pound;' . number_format($TotalProductsPrice2, 2) . '</p>';
	
		echo '<div align="center" style="margin-bottom: 15px;"><a href="/basket.html"><img src="/i/other/view_basket.gif" alt="View Basket" border="0" title="Click to view your basket" /></a></div>';
	}
?>
</div>
