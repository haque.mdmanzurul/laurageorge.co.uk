<?php require_once("../_header.php"); ?>
<h1>Vouchers</h1>
<p>Below are a list of your vouchers.</p>

<?php

$Query = @mysql_query("SELECT * FROM `vouchers` ORDER BY `code`");
if(mysql_num_rows($Query) != 0)
{
?>
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><strong>Voucher Code</strong></td>
		<td><strong>Fixed Price</strong></td>
		<td><strong>Percentage Off</strong></td>
		<td><strong>Active?</strong></td>
		<td><strong>Action</strong></td>
	</tr>
<?php
	while($Row=mysql_fetch_assoc($Query))
	{
		$Active = ($Row['active'] == 'Y') ? 'Yes' : 'No';
?>
	<tr>
		<td><?php echo $Row['code']; ?></td>
		<td>&pound;<?php echo number_format($Row['price'], 2); ?></td>
		<td><?php echo $Row['percent']; ?>%</td>
		<td><?php echo $Active; ?></td>
		<td><a href="edit.php?id=<?php echo $Row['id']; ?>">[Edit]</a></td>
	</tr>
<?php } ?>
</table>

<?php	
}
else
	echo '<p>You have no vouchers.</p>';

?>

<?php require_once("../_footer.php"); ?>