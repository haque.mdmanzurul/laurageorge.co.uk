<?php require_once("_functions.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php echo $_TITLE; ?></title>
<meta name="keywords" content="<?php echo $_KEYWORDS; ?>" />
<meta name="description" content="<?php echo $_DESCRIPTION; ?>" />
<?php echo $_JS; ?>
<?php echo $_CSS; ?>
</head>
<body>
<div id="content">

		<div id="top">

        		<div id="admin_menu_2"></div>
                <div id="content_info"></div>
                <div id="client_logo"><a href="/admin/index.html"></a></div>
                <div id="title"><h1>Admin Centre</h1>
                </div>
                <div id="customer_service"><p>Customer Service 0845 838 2240</p>                
                </div>

                <div id="loyalty_matters"></div>
   		 		<div id="admin_menu">
                		<ul id="menu" style="font-size:11px;">
                        <li class='selected'><a href="/admin/index.php">Overview</a> | </li>
                        <li class='menu_item_1'><a href="/admin/slider/index.php">Slider</a> | </li>
                        <li class="menu_item_1"><a href="/admin/orders/index.php">Orders</a> | </li>
						<li class="menu_item_1"><a href="/admin/products/index.php">Products</a> | </li>
						<li class="menu_item_1"><a href="/admin/products/add.php">Add Product</a> | </li>
						<li class="menu_item_1"><a href="/admin/pages/index.php">Pages</a> | </li>
						<li class="menu_item_1"><a href="/admin/categories/index.php">Categories</a> | </li>
						<li class="menu_item_1"><a href="/admin/categories/add.php">Add Category</a> | </li>
						<li class="menu_item_1"><a href="/admin/colours/index.php">Colours</a> | </li>
						<li class="menu_item_1"><a href="/admin/colours/add.php">Add Colour</a> | </li>
						<li class="menu_item_1"><a href="/admin/vouchers/index.php">Vouchers</a> | </li>
						<li class="menu_item_1"><a href="/admin/vouchers/add.php">Add Voucher</a> | </li>
						<li class="menu_item_1"><a href="/admin/front/index.php">Front Page</a> | </li>
						<li class="menu_item_1"><a href="/admin/blog/index.php">Blog</a> | </li>
						<li class="menu_item_1"><a href="/admin/lookbook/">Look Book</a> | </li>
						<li class="menu_item_1"><a href="/admin/delivery/index.php">Delivery Prices</a> | </li>
						<li class="menu_item_1"><a href="/plesk_stat/webstat">Statistics</a> | </li>
                        </ul>
			</div>
        </div>
		
        <div id="main_content">
