<?php require_once("../_header.php"); ?>
<h1>Viewing an Order</h1>

<?php

$_ID=mysql_real_escape_string($_GET['id']);
$_STATUS=mysql_real_escape_string($_POST['status']);
$_NOTES=mysql_real_escape_string($_POST['OrderNotes']);

if($_POST['UpStatus'])
{
	@mysql_query("UPDATE `orders` SET `status` = '$_STATUS' WHERE `id` = $_ID");
	if($_POST['status']=='D')
	{
		$Query = @mysql_query("SELECT * FROM `orders` o WHERE o.id = $_ID");
		$Row = mysql_fetch_assoc($Query);
$Headers = 'From: info@laurageorge.co.uk' . "\r\n" .
    'Reply-To: info@laurageorge.co.uk' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();	
	$Message = "Thank you for your recent order (#$_ID) with Laura George.  

We are pleased to confirm that your order has been dispatched and should arrive shortly. 

If you have any questions in the meantime please contact us on +44 (0)7534 65 22 82. 

Thank you,
Laura George";
		@mail($Row['email'], "Confirmation of dispatch for order #$_ID with Laura George", $Message, $Headers);
	}
}
elseif($_POST['UpdateNotes'])
	@mysql_query("UPDATE `orders` SET `notes` = '$_NOTES' WHERE `id` = $_ID");

$Query = @mysql_query("SELECT * FROM `orders` o WHERE o.id = $_ID");
if(mysql_num_rows($Query) != 0)
{
	$Row = mysql_fetch_assoc($Query);
	$OrderDetails = $Row;
?>
<?php
	echo '<h2>Order Details</h2>';
?>
<table width="500" border="0" cellspacing="0" cellpadding="5" style="float:left;">
	<tr>
		<td width="150" align="right"><strong>Order Number:</strong></td>
		<td><?php echo $Row['id']; ?></td>
	</tr>
	<tr>
		<td align="right"><strong>Order Date:</strong></td>
		<td><?php echo date('d/m/Y @ h:i:sA', $Row['date']); ?></td>
	</tr>
	<tr>
		<td align="right"><strong>Status:</strong></td>
		<td><form action="" method="post"><?php echo buildStatus($Row['status']); ?> <input name="UpStatus" type="submit" value="Update Status" /></form></td>
	</tr>
	<tr>
		<td align="right"><strong>Customer:</strong></td>
		<td><?php $Name = explode("\n", $Row['delivery']); echo $Name[0]; ?></td>
	</tr>
	<tr>
		<td align="right"><strong>Customer Email:</strong></td>
		<td><?php echo $Row['email']; ?></td>
	</tr>
	<tr>
		<td align="right"><strong>Customer Telephone:</strong></td>
		<td><?php echo $Row['phone']; ?></td>
	</tr>
	<tr>
		<td align="right" valign="top"><strong>Delivery Address:</strong></td>
		<td><?php echo stripslashes(nl2br($Row['delivery'])); ?></td>
	</tr>
</table>
<div style="clear:both;"></div>
<h2>Order Notes</h2>
<form action="" method="post">
<textarea name="OrderNotes" cols="50" rows="7" style="width: 99%; height:120px;"><?php echo stripslashes($Row['notes']); ?></textarea>
<input name="UpdateNotes" type="submit" value="     Update Order Notes     " />
</form>

<h2>Order Items</h2>
<table width="100%" border="0" cellspacing="0" cellpadding="5" class="darker">
	<tr>
		<td width="180" align="center" style="border-bottom:1px solid #EEE;"><strong>Product</strong></td>
		<td width="100" align="center" style="border-bottom:1px solid #EEE;"><strong>Colour</strong></td>
		<td width="50" align="center" style="border-bottom:1px solid #EEE;"><strong>Size</strong></td>
		<td width="180" align="center" style="border-bottom:1px solid #EEE;"><strong>Quantity</strong></td>
		<td align="center" style="border-bottom:1px solid #EEE;"><strong>Price</strong></td>
	</tr>
<?php
	$Query = @mysql_query("SELECT * FROM `order_items` oi, `products` p WHERE oi.o_id = $_ID AND oi.p_id = p.id");
	if(mysql_num_rows($Query)!=0)
	{
		$_SIZES = getSizes();
		$_COLOURS = getColours();			
			
		while($Row = mysql_fetch_assoc($Query))
		{
			$Product = makeW3C($Row);
			$TotalProductsPrice += ($Product['quantity'] * $Product['price']);
			$TotalProducts += $Product['quantity'];
			
?>
	<tr>
		<td align="center"><img src="/i/products/<?php echo $Product['id']; ?>/<?php echo $Product['colour']; ?>_t.jpg" alt="<?php echo $Product['name']; ?>" width="170" /></td>
		<td align="center"><?php echo $_COLOURS[$Product['colour']]['Name']; ?></td>
		<td align="center"><?php echo $_SIZES[$Product['size']]; ?></td>
		<td align="center"><?php echo $Product['quantity']; ?></td>
		<td align="center">&pound;<?php echo number_format($Product['price'] * $Product['quantity'], 2); ?></td>
	</tr>
<?php
		}
?>	
</table>
<hr />
	<table width="400" border="0" align="right" cellpadding="5" cellspacing="0">
		<tr>
			<td width="300" align="right"><strong>SUB TOTAL:</strong></td>
			<td>&pound;<?php echo number_format($TotalProductsPrice, 2); ?></td>
		</tr>
		<tr>
			<td align="right"><strong>DELIVERY:</strong></td>
			<td>&pound;<?php echo number_format($OrderDetails['deliveryttl'], 2); ?></td>
		</tr>
<?php if($OrderDetails['voucher'] != '') { ?>
	<tr>
		<td align="right"><strong>VOUCHER USED</strong></td>
		<td><?php echo $OrderDetails['voucher']; ?></td>
	</tr>
<?php } ?>
		<tr>
			<td align="right" style="border-top:1px solid #000;border-bottom:1px solid #000;"><strong>TOTAL:</strong></td>
			<td style="border-top:1px solid #000;border-bottom:1px solid #000;"><strong>&pound;<?php echo number_format($OrderDetails['total'] + $OrderDetails['deliveryttl'], 2); ?></strong></td>
		</tr>
	</table>
	<div style="clear:both;display:block;"></div>
<?php
	}
	else
		echo 'This order has no items? Not possible. Please contact the customer directly.';
?>
<?php
}
else
	echo '<p>Sorry, this order does not exist.</p>';

?>

<?php require_once("../_footer.php"); ?>