<?php
require_once("_functions.php");

$dir = "../i/products/";
$dh  = opendir($dir);
while (false !== ($filename = readdir($dh))) {
    $files[] = $filename;
}

header('Content-type: image/jpeg');

foreach($files as $f)
{
	if(strstr($f, "thumb_")	== false)
	{
		if($f != '.' && $f != '..')
		{
			$_ID = substr($f, 0, -4);
			
		   $image = $norm_img = open_image(urldecode('../i/products/' . $f));
		   
		   if ($image == false) { echo 'IMGAE COULD NOT BE RESIZED'; }
		   
			// Get original width and height
			$width = imagesx($image);
			$height = imagesy($image);
			
			$new_width = 143;
			$new_height = 143;
			
			// create a new true color image
			$canvas = imagecreatetruecolor( $new_width, $new_height );
			
	
			if($width > $new_width)
			{
				$_P = ($new_width/$width);
				$_W = $_P * $width;
				$_H = $_P * $height;
				
				$_TS = imagecreatetruecolor($_W, $_H);
				imagecopyresampled($_TS, $image, 0, 0, 0, 0, $_W, $_H, $width, $height);
				$width = imagesx($_TS);
				$height = imagesy($_TS);
				$image = $_TS;
			}
			
			if($height > $new_height)
			{
				$_P = ($new_height/$height);
				$_W = $_P * $width;
				$_H = $_P * $height;
				
				$_TS = imagecreatetruecolor($_W, $_H);
		  		$im_bg = imagecolorallocate($_TS, 255, 255, 255);
		  		imagefill($_TS, 0, 0, $im_bg); 
				imagecopyresampled($_TS, $image, 0, 0, 0, 0, $_W, $_H, $width, $height);
				$width = imagesx($_TS);
				$height = imagesy($_TS);
				$image = $_TS;
			}
			
			//$nw = ((143 - $width) / 2)
		   
		   $out = imagecreatetruecolor(143, 143);
		   $im_bg = imagecolorallocate($out, 255, 255, 255);
		   imagefill($out, 0, 0, $im_bg); 
		   @imagecopyresampled($out, $image, ((143 - $width) / 2), ((143 - $height) / 2), 0, 0, $_W, $_H, $width, $height );
			
			// Output
			//imagejpeg($out, null, 100);
			
			imagejpeg($out, '../i/products/thumb_' . $_ID . '.jpg', 80);
			
			chmod('../i/products/thumb_' . $_ID . '.jpg', 0777);
		}
	}
}

?>
