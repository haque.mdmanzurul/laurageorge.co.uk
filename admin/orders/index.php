<?php require_once("../_header.php"); ?>
<h1>Your Orders</h1>
<p>Below are a list of your recent orders. You can sort them by date or status.</p>

<p style="padding:10px;"><strong>View By:</strong> <a href="index.php?by=N">New</a> | <a href="index.php?by=P">Pending</a> | <a href="index.php?by=PP">Paid</a> | <a href="index.php?by=D">Dispatched</a> | <a href="index.php?by=C">Cancelled</a></p>

<form action="" method="post" style="padding:10px;">
<strong>Search:</strong>
<input name="q" type="text" size="30" />
<input name="Search" type="submit" value="     Search     " />
</form><br /><br />

<?php
if(ctype_alpha($_GET['status']))
	$Status = mysql_real_escape_string($_GET['status']);
else
	$Status = 'N';

$ViewBy = mysql_real_escape_string($_GET['by']);

$_STARTYEAR = 2009;
$_ENDYEAR = date('Y');

if(isset($_GET['month']) && isset($_GET['year']))
{
	$_THISMONTH = mktime(0, 0, 0, $_GET['month'], 1, $_GET['year']);
	$_THISMONTHEND = mktime(23, 59, 59, $_GET['month'], 31, $_GET['year']);
}
else
{
	$_THISMONTH = mktime(0, 0, 0, date('m'), 1, date('Y'));
	$_THISMONTHEND = mktime(23, 59, 59, date('m'), 31, date('Y'));
}
$_MONTHS = array(1=>'January', 'February','March','April','May','June','July','August','September','October','November','December');
?>

<form action="index.php" method="get" id="byyear">
	View orders of status: <?php echo buildStatus($_GET['status']); ?>
	
	Month: <select name="month">
	<?php
	foreach($_MONTHS as $key=>$value)
	{
		if($_GET['month']==($key) || date('m', $_THISMONTH) == ($key))
			echo '<option value="'.($key).'" selected>'.$value.'</option>';
		else
			echo '<option value="'.($key).'">'.$value.'</option>';
	}
	?>
	</select>
	Year: <select name="year">
	<?php for($y=$_STARTYEAR; $y<(date('Y')+1); $y++)	{ 
	
	if($_GET['year']==$y || $y == $_ENDYEAR)
		echo '<option value="'.$y.'" selected>'.$y.'</option>'; 
	else
		echo '<option value="'.$y.'">'.$y.'</option>'; 
	
	}	?>
	</select>
	<input type="Submit" value="View Orders" />
</form>
<?php if(isset($_GET['year'])==false && isset($_POST['Search'])==false && isset($_GET['by'])==false) { ?>
<script type="text/javascript">
<!--
document.forms[1].submit();
//-->
</script>
<?php } ?>
<br /><br />
<?php
	if($_POST['Search'])
		$Query = "SELECT o.* FROM `orders` o WHERE (o.id LIKE '%".c($_POST['q'])."%' OR o.delivery LIKE '%".c($_POST['q'])."%' OR o.email LIKE '%".c($_POST['q'])."%') ORDER BY o.id DESC";
	elseif(ctype_alpha($_GET['by']))
		$Query = "SELECT o.* FROM `orders` o WHERE o.status = '$ViewBy' ORDER BY o.id DESC";
	else
		$Query = "SELECT o.* FROM `orders` o WHERE o.status = '$Status' AND o.date >= '$_THISMONTH' AND o.date <= '$_THISMONTHEND' ORDER BY o.id DESC";
		
		$TOTAL = mysql_num_rows(mysql_query($Query));
	
if(isset($_GET['pagenum']))
	$pagenum = $_GET['pagenum']; 
$page_rows = 50; 
$last = ceil($TOTAL/$page_rows);
if ($pagenum < 1)
	$pagenum = 1;
elseif ($pagenum > $last)
	$pagenum = $last;
$max = 'limit ' .($pagenum - 1) * $page_rows .',' .$page_rows; 

	$GetOrders = mysql_query("$Query $max");
	
	if(mysql_num_rows($GetOrders) != 0)
	{
		echo '<table width="" border="0" cellspacing="0" cellpadding="0" id="tlist">
<tr>
<th width="150">Order ID</th>
<th width="200">Date</th>
<th width="200">Customer\s Name</th>
<th width="100">Total</th>
</tr>';

		while($OrderOverview = mysql_fetch_array($GetOrders))
		{
			$Name = explode("\n", $OrderOverview['delivery']);
			echo '<tr>';
			echo '<td><a href="view.php?id='.$OrderOverview['id'].'">#' . $OrderOverview['id'] . '</a></td>';
			echo '<td>' . date('d/m/Y @ h:iA', $OrderOverview['date']) . '</td>';
			echo '<td>' . $Name[0] . '</td>';
			echo '<td>&pound;' . number_format($OrderOverview['total'], 2) . '</td>';
			echo '</td>';
		}
		
		echo '</table>';
		
		echo "<p style=\"font-size:14px;font-weight:bold;text-align:center;\"> -- Page $pagenum of $last -- <br />";
		
		if ($pagenum != 1)
		{
			echo " <a href='{$_SERVER['REQUEST_URI']}&pagenum=1'> <<-First</a> ";
			echo " <a href='{$_SERVER['REQUEST_URI']}&pagenum=".($pagenum-1)."'> <-Previous</a> ";
		}
		
		echo " ---- ";
		
		if ($pagenum != $last)
		{
			echo " <a href='{$_SERVER['REQUEST_URI']}&pagenum=".($pagenum+1)."'>Next -></a> ";
			echo " <a href='{$_SERVER['REQUEST_URI']}&pagenum=$last'>Last ->></a> ";
		}
		
		echo "</p>";
	}
	else
	{
		echo 'Woops! You have no orders!';
	}

?>

<?php require_once("../_footer.php"); ?>