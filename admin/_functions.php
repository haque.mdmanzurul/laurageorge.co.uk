<?php
	session_start();
	
	//ini_set('display_errors', 1);
	//error_reporting(E_ALL);

	//error_reporting(E_ALL ^ E_NOTICE);

	//$mysql_connection = @mysql_connect('localhost', 'laurageorge', 'hK0Vkv5qH91X');
	$mysql_connection = @mysql_connect('localhost', 'root', '');
	$mysql_database = @mysql_select_db('laurageorge', $mysql_connection);
	
	//$alter = mysql_query('ALTER TABLE `home_slider` ADD `order` INT NOT NULL AFTER `description` ');	
	
	function c($text)
	{
		$text = mysql_real_escape_string($text);
		return $text;
	}
	
	function getPages()
	{
		$_PAGES[1]['Name'] = 'Blog';
		$_PAGES[1]['Blocks'] = 'Y';
		$_PAGES[2]['Name'] = 'About';
		$_PAGES[2]['Blocks'] = 'N';
		$_PAGES[3]['Name'] = 'Privacy';
		$_PAGES[3]['Blocks'] = 'N';
		$_PAGES[4]['Name'] = 'Press';
		$_PAGES[4]['Blocks'] = 'Y';
		$_PAGES[5]['Name'] = 'Events';
		$_PAGES[5]['Blocks'] = 'Y';
		$_PAGES[6]['Name'] = 'Testimonials';
		$_PAGES[6]['Blocks'] = 'Y';
		$_PAGES[7]['Name'] = 'Info';
		$_PAGES[7]['Blocks'] = 'N';
		$_PAGES[8]['Name'] = 'Fitting &amp; Care';
		$_PAGES[8]['Blocks'] = 'N';
		$_PAGES[9]['Name'] = 'History &amp; Tradition';
		$_PAGES[9]['Blocks'] = 'N';
		$_PAGES[10]['Name'] = 'Coture';
		$_PAGES[10]['Blocks'] = 'N';
		$_PAGES[11]['Name'] = 'Stockists';
		$_PAGES[11]['Blocks'] = 'Y';
		$_PAGES[12]['Name'] = 'Terms &amp; Conditions';
		$_PAGES[12]['Blocks'] = 'N';
		
		return $_PAGES;	
	}
	
	function makeW3C($Row)
	{
		foreach($Row as $key=>$value)
			$Row[$key] = htmlentities(stripslashes($value));
		
		return $Row;
	}

	function doJS($file)
	{
		$time = @filemtime($_SERVER['DOCUMENT_ROOT'] . '/admin/i/js/' . $file . '.js');
		return '<script src="/admin/i/js/'.$file.'.js?'.$time.'" type="text/javascript"></script>' . "\n";
	}
	
	function doCSS($file)
	{
		$time = @filemtime($_SERVER['DOCUMENT_ROOT'] . '/admin/i/css/' . $file . '.css');
		return '<link href="/admin/i/css/'.$file.'.css?'.$time.'" rel="stylesheet" type="text/css" />' . "\n";
	}
	
	function showCountries($County)
	{
		$_COUNTRIES = getCountries();
		$HTML = '<select name="country">';
		foreach($_COUNTRIES as $ID=>$Name)
		{
			if($ID == $County)
				$HTML .= '<option value="'.$ID.'" selected="selected">'.htmlentities($Name).'</option>';
			else
				$HTML .= '<option value="'.$ID.'">'.htmlentities($Name).'</option>';
		}
		$HTML .= '</select>';
		return $HTML;
	}
	
	function get($url)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	}
	function getLatLong($code)
	{
		$data = get("http://maps.google.co.uk/maps/geo?q=".urlencode($code)."&output=csv&key=ABQIAAAALaEMvyF1IheqRB4Bg3fjNxRUDPypFzjd22PTHAEXYI2eJnLDFhRjItxLenZx4_gp2qaOAHFSZ_CWwA");
		 
		if(null !== ($data = explode(",", $data)))
		{
			$long = $data[3];
			$lat = $data[2];
			return array('Latitude'=>$lat,'Longitude'=>$long);
		}
		else
			return false;
	}
	
	function saveImage($underscoreFilesGlobal, $FileName)
	{
		include $_SERVER['DOCUMENT_ROOT'] . "/admin/resize.php";
		 
		// Increase the allowed memory size for the bigger images
		ini_set('mem_size', 32000);
		 
		try {
			$image = new imageResizer($underscoreFilesGlobal['img']['tmp_name']);
		 
			$image->resize(234, 250, 234, 150);
			$image->save($_SERVER['DOCUMENT_ROOT'] . '/i/pages/'.$FileName.'.jpg', JPG);
		}
		catch(Exception $e) {
			// Catch and display any exceptional behaviour
			//print $e->getMessage();
			//exit();
		}
		 
		// Destroy object (executes the destructor) and more importantly, frees up memory
		$image = null;	
	}
	
function open_image ($file) {
    # JPEG:
    $im = @imagecreatefromjpeg($file);
    if ($im !== false) { return $im; }

    # GIF:
    $im = @imagecreatefromgif($file);
    if ($im !== false) { return $im; }

    # PNG:
    $im = @imagecreatefrompng($file);
    if ($im !== false) { return $im; }

    # GD File:
    $im = @imagecreatefromgd($file);
    if ($im !== false) { return $im; }

    # GD2 File:
    $im = @imagecreatefromgd2($file);
    if ($im !== false) { return $im; }

    # WBMP:
    $im = @imagecreatefromwbmp($file);
    if ($im !== false) { return $im; }

    # XBM:
    $im = @imagecreatefromxbm($file);
    if ($im !== false) { return $im; }

    # XPM:
    $im = @imagecreatefromxpm($file);
    if ($im !== false) { return $im; }

    # Try and load from string:
	$img = @file_get_contents($file);
	if($img!=FALSE)
	{
		$im = @imagecreatefromstring(@file_get_contents($file));
		if ($im !== false) { return $im; }
		else
		return false;
	}

    return false;
} 
	
	function buildStatus($Pkey)
	{
		$_STATUS = getStatus();
		
		$HTML = '<select name="status">';
		foreach($_STATUS as $Key=>$Value)
		{
			if($Pkey==$Key)
				$HTML .= '<option value="'.$Key.'" selected>'.htmlentities($Value).'</option>';
			else
				$HTML .= '<option value="'.$Key.'">'.htmlentities($Value).'</option>';
		}
		$HTML .= '</select>';
		return $HTML;
	}
	
	function DeliveryAddress($DeliveryID)
	{
		$Query = @mysql_query("SELECT * FROM `customer_delivery` WHERE `id` = $DeliveryID");
		if(mysql_num_rows($Query)!=0)
		{
			$Row = mysql_fetch_assoc($Query);
			return stripslashes($Row['title'] . ' ' . $Row['fname'] . ' ' . $Row['lname'] . '<br />' . $Row['address1'] . '<br />' . $Row['address2'] . '<br />' . $Row['town'] . '<br />' . $Row['county'] . '<br />' . $Row['postcode'] . '<br />' . $Row['country']);
		}
		else
			return 'Same as Billing Address';
	}
	
	function buildCustomerStatus($Pkey)
	{
		$_STATUS = getCustomerStatus();
		
		$HTML = '<select name="cstatus">';
		foreach($_STATUS as $Key=>$Value)
		{
			if($Pkey==$Key)
				$HTML .= '<option value="'.$Key.'" selected>'.htmlentities($Value).'</option>';
			else
				$HTML .= '<option value="'.$Key.'">'.htmlentities($Value).'</option>';
		}
		$HTML .= '</select>';
		return $HTML;
	}
	
	function getColours()
	{
		$Query = @mysql_query("SELECT * FROM `colours`");	
		while($Row = @mysql_fetch_assoc($Query))
		{
			$_COLOURS[$Row['id']]['Name'] = $Row['name'];
			$_COLOURS[$Row['id']]['Hex'] = $Row['hex'];
		}
			
		return $_COLOURS;
	}
	
	function getSizes()
	{
		$Query = @mysql_query("SELECT * FROM `sizes`");	
		while($Row = @mysql_fetch_assoc($Query))
			$_SIZES[$Row['id']] = $Row['name'];
			
		return $_SIZES;
	}
	
	function buildSizes($Sizes)
	{
		$_SIZES = getSizes();
		$Si = explode(',', $Sizes);
		$HTML = "";
		
		foreach($_SIZES as $Key=>$Name)
		{
			if(in_array($Key, $Si))
				$HTML .= '<input name="size['.$Key.']" type="checkbox" value="'.$Key.'" checked="checked" />' . $Name . '<br />';
			else
				$HTML .= '<input name="size['.$Key.']" type="checkbox" value="'.$Key.'" />' . $Name . '<br />';
		}
			
		return $HTML;
	}
	
	function buildColours($Colours, $PID = 0)
	{
		$_COLOURS = getColours();
		$Cu = explode(',', $Colours);
		$HTML = "";

		foreach($_COLOURS as $Key=>$Name)
		{
			if(in_array($Key, $Cu))
				$HTML .= '<div style="width:150px;display:inline-block;"><span class="colourblock" style="background-color:#'.$Name['Hex'].';" id="cbc">&nbsp;</span> <input name="colour['.$Key.']" type="checkbox" value="'.$Key.'" checked="checked" /><a href="../../i/products/'.$PID.'/'.$Key.'_b.jpg" rel="superbox[gallery][my_gallery]">' . $Name['Name'] . '</a></div> <input type="file" name="cimg['.$Key.']" width="50px" /><br />';
			else
				$HTML .= '<div style="width:150px;display:inline-block;"><span class="colourblock" style="background-color:#'.$Name['Hex'].';" id="cbc">&nbsp;</span> <input name="colour['.$Key.']" type="checkbox" value="'.$Key.'" />' . $Name['Name'] . '</div> <input type="file" name="cimg['.$Key.']" width="50px" /><br />';
		}
			
		return $HTML;
	}
	
	function getStatus()
	{
		return $_STATUS = array('N'=>'New', 'P'=>'Pending', 'PP'=>'Paid', 'D'=>'Dispatched', 'C'=>'Cancelled');
	}

	function getCategories()
	{
		global $MySQL_Connection;
		//Get Page Categories
		$Query = @mysql_query("SELECT c.* FROM `categories` c ORDER BY c.name ASC");
		while($Row=mysql_fetch_assoc($Query))
		{
			$_CATEGORIES[$Row['id']]['Name'] = $Row['name'];
		}
		return $_CATEGORIES;
	}
	
	function buildCategories($Pkey)
	{
		$_CATEGORIES = getCategories();
		$CategoryHTML = '<select name="category">';
		foreach($_CATEGORIES as $Key=>$Value)
		{
			if($Pkey==$Key)
				$CategoryHTML .= '<option value="'.$Key.'" selected>'.htmlentities($Value['Name']).'</option>';
			else
				$CategoryHTML .= '<option value="'.$Key.'">'.htmlentities($Value['Name']).'</option>';
		}
		$CategoryHTML .= '</select>';
		return $CategoryHTML;
	}
	
	function getCountries()
	{
	 return array(
'GB'=>'United Kingdom',
'AF'=>'Afghanistan',
'AL'=>'Albania',
'DZ'=>'Algeria',
'AS'=>'American Samoa',
'AD'=>'Andorra',
'AO'=>'Angola',
'AI'=>'Anguilla',
'AQ'=>'Antarctica',
'AG'=>'Antigua And Barbuda',
'AR'=>'Argentina',
'AM'=>'Armenia',
'AW'=>'Aruba',
'AU'=>'Australia',
'AT'=>'Austria',
'AZ'=>'Azerbaijan',
'BS'=>'Bahamas',
'BH'=>'Bahrain',
'BD'=>'Bangladesh',
'BB'=>'Barbados',
'BY'=>'Belarus',
'BE'=>'Belgium',
'BZ'=>'Belize',
'BJ'=>'Benin',
'BM'=>'Bermuda',
'BT'=>'Bhutan',
'BO'=>'Bolivia',
'BA'=>'Bosnia And Herzegovina',
'BW'=>'Botswana',
'BV'=>'Bouvet Island',
'BR'=>'Brazil',
'IO'=>'British Indian Ocean Territory',
'BN'=>'Brunei',
'BG'=>'Bulgaria',
'BF'=>'Burkina Faso',
'BI'=>'Burundi',
'KH'=>'Cambodia',
'CM'=>'Cameroon',
'CA'=>'Canada',
'CV'=>'Cape Verde',
'KY'=>'Cayman Islands',
'CF'=>'Central African Republic',
'TD'=>'Chad',
'CL'=>'Chile',
'CN'=>'China',
'CX'=>'Christmas Island',
'CC'=>'Cocos (Keeling) Islands',
'CO'=>'Columbia',
'KM'=>'Comoros',
'CG'=>'Congo',
'CK'=>'Cook Islands',
'CR'=>'Costa Rica',
'CI'=>'Cote D\'Ivorie (Ivory Coast)',
'HR'=>'Croatia (Hrvatska)',
'CU'=>'Cuba',
'CY'=>'Cyprus',
'CZ'=>'Czech Republic',
'CD'=>'Democratic Republic Of Congo (Zaire)',
'DK'=>'Denmark',
'DJ'=>'Djibouti',
'DM'=>'Dominica',
'DO'=>'Dominican Republic',
'TP'=>'East Timor',
'EC'=>'Ecuador',
'EG'=>'Egypt',
'SV'=>'El Salvador',
'GQ'=>'Equatorial Guinea',
'ER'=>'Eritrea',
'EE'=>'Estonia',
'ET'=>'Ethiopia',
'FK'=>'Falkland Islands (Malvinas)',
'FO'=>'Faroe Islands',
'FJ'=>'Fiji',
'FI'=>'Finland',
'FR'=>'France',
'FX'=>'France, Metropolitan',
'GF'=>'French Guinea',
'PF'=>'French Polynesia',
'TF'=>'French Southern Territories',
'GA'=>'Gabon',
'GM'=>'Gambia',
'GE'=>'Georgia',
'DE'=>'Germany',
'GH'=>'Ghana',
'GI'=>'Gibraltar',
'GR'=>'Greece',
'GL'=>'Greenland',
'GD'=>'Grenada',
'GP'=>'Guadeloupe',
'GU'=>'Guam',
'GT'=>'Guatemala',
'GN'=>'Guinea',
'GW'=>'Guinea-Bissau',
'GY'=>'Guyana',
'HT'=>'Haiti',
'HM'=>'Heard And McDonald Islands',
'HN'=>'Honduras',
'HK'=>'Hong Kong',
'HU'=>'Hungary',
'IS'=>'Iceland',
'IN'=>'India',
'ID'=>'Indonesia',
'IR'=>'Iran',
'IQ'=>'Iraq',
'IE'=>'Ireland',
'IL'=>'Israel',
'IT'=>'Italy',
'JM'=>'Jamaica',
'JP'=>'Japan',
'JO'=>'Jordan',
'KZ'=>'Kazakhstan',
'KE'=>'Kenya',
'KI'=>'Kiribati',
'KW'=>'Kuwait',
'KG'=>'Kyrgyzstan',
'LA'=>'Laos',
'LV'=>'Latvia',
'LB'=>'Lebanon',
'LS'=>'Lesotho',
'LR'=>'Liberia',
'LY'=>'Libya',
'LI'=>'Liechtenstein',
'LT'=>'Lithuania',
'LU'=>'Luxembourg',
'MO'=>'Macau',
'MK'=>'Macedonia',
'MG'=>'Madagascar',
'MW'=>'Malawi',
'MY'=>'Malaysia',
'MV'=>'Maldives',
'ML'=>'Mali',
'MT'=>'Malta',
'MH'=>'Marshall Islands',
'MQ'=>'Martinique',
'MR'=>'Mauritania',
'MU'=>'Mauritius',
'YT'=>'Mayotte',
'MX'=>'Mexico',
'FM'=>'Micronesia',
'MD'=>'Moldova',
'MC'=>'Monaco',
'MN'=>'Mongolia',
'MS'=>'Montserrat',
'MA'=>'Morocco',
'MZ'=>'Mozambique',
'MM'=>'Myanmar (Burma)',
'NA'=>'Namibia',
'NR'=>'Nauru',
'NP'=>'Nepal',
'NL'=>'Netherlands',
'AN'=>'Netherlands Antilles',
'NC'=>'New Caledonia',
'NZ'=>'New Zealand',
'NI'=>'Nicaragua',
'NE'=>'Niger',
'NG'=>'Nigeria',
'NU'=>'Niue',
'NF'=>'Norfolk Island',
'KP'=>'North Korea',
'MP'=>'Northern Mariana Islands',
'NO'=>'Norway',
'OM'=>'Oman',
'PK'=>'Pakistan',
'PW'=>'Palau',
'PA'=>'Panama',
'PG'=>'Papua New Guinea',
'PY'=>'Paraguay',
'PE'=>'Peru',
'PH'=>'Philippines',
'PN'=>'Pitcairn',
'PL'=>'Poland',
'PT'=>'Portugal',
'PR'=>'Puerto Rico',
'QA'=>'Qatar',
'RE'=>'Reunion',
'RO'=>'Romania',
'RU'=>'Russia',
'RW'=>'Rwanda',
'SH'=>'Saint Helena',
'KN'=>'Saint Kitts And Nevis',
'LC'=>'Saint Lucia',
'PM'=>'Saint Pierre And Miquelon',
'VC'=>'Saint Vincent And The Grenadines',
'SM'=>'San Marino',
'ST'=>'Sao Tome And Principe',
'SA'=>'Saudi Arabia',
'SN'=>'Senegal',
'SC'=>'Seychelles',
'SL'=>'Sierra Leone',
'SG'=>'Singapore',
'SK'=>'Slovak Republic',
'SI'=>'Slovenia',
'SB'=>'Solomon Islands',
'SO'=>'Somalia',
'ZA'=>'South Africa',
'GS'=>'South Georgia And South Sandwich Islands',
'KR'=>'South Korea',
'ES'=>'Spain',
'LK'=>'Sri Lanka',
'SD'=>'Sudan',
'SR'=>'Suriname',
'SJ'=>'Svalbard And Jan Mayen',
'SZ'=>'Swaziland',
'SE'=>'Sweden',
'CH'=>'Switzerland',
'SY'=>'Syria',
'TW'=>'Taiwan',
'TJ'=>'Tajikistan',
'TZ'=>'Tanzania',
'TH'=>'Thailand',
'TG'=>'Togo',
'TK'=>'Tokelau',
'TO'=>'Tonga',
'TT'=>'Trinidad And Tobago',
'TN'=>'Tunisia',
'TR'=>'Turkey',
'TM'=>'Turkmenistan',
'TC'=>'Turks And Caicos Islands',
'TV'=>'Tuvalu',
'UG'=>'Uganda',
'UA'=>'Ukraine',
'AE'=>'United Arab Emirates',
'GB'=>'United Kingdom',
'US'=>'United States',
'UM'=>'United States Minor Outlying Islands',
'UY'=>'Uruguay',
'UZ'=>'Uzbekistan',
'VU'=>'Vanuatu',
'VA'=>'Vatican City (Holy See)',
'VE'=>'Venezuela',
'VN'=>'Vietnam',
'VG'=>'Virgin Islands (British)',
'VI'=>'Virgin Islands (US)',
'WF'=>'Wallis And Futuna Islands',
'EH'=>'Western Sahara',
'WS'=>'Western Samoa',
'YE'=>'Yemen',
'YU'=>'Yugoslavia',
'ZM'=>'Zambia',
'ZW'=>'Zimbabwe'
);
	}
function base64Decode($scrambled) {
  // Initialise output variable
  $output = "";
  
  // Fix plus to space conversion issue
  $scrambled = str_replace(" ","+",$scrambled);
  
  // Do encoding
  $output = base64_decode($scrambled);
  
  // Return the result
  return $output;
}
function base64Encode($plain) {
  // Initialise output variable
  $output = "";
  
  // Do encoding
  $output = base64_encode($plain);
  
  // Return the result
  return $output;
}


/*  The SimpleXor encryption algorithm                                                                                **
**  NOTE: This is a placeholder really.  Future releases of Form will use AES or TwoFish.  Proper encryption      **
**  This simple function and the Base64 will deter script kiddies and prevent the "View Source" type tampering        **
**  It won't stop a half decent hacker though, but the most they could do is change the amount field to something     **
**  else, so provided the vendor checks the reports and compares amounts, there is no harm done.  It's still          **
**  more secure than the other PSPs who don't both encrypting their forms at all                                      */

function simpleXor($InString, $Key) {
  // Initialise key array
  $KeyList = array();
  // Initialise out variable
  $output = "";
  
  // Convert $Key into array of ASCII values
  for($i = 0; $i < strlen($Key); $i++){
    $KeyList[$i] = ord(substr($Key, $i, 1));
  }

  // Step through string a character at a time
  for($i = 0; $i < strlen($InString); $i++) {
    // Get ASCII code from string, get ASCII code from key (loop through with MOD), XOR the two, get the character from the result
    // % is MOD (modulus), ^ is XOR
    $output.= chr(ord(substr($InString, $i, 1)) ^ ($KeyList[$i % strlen($Key)]));
  }

  // Return the result
  return $output;
}


	$_TITLE = 'Overview : loyaltyMATTERS\' CMS';
	$_KEYWORDS = 'cms, content management system, website design';
	$_DESCRIPTION = 'A Content Management System that controls clients websites with ease';

	if(isset($_CSS))
	{
		$_CSS .= doCSS('admin');
	}
	else
	{
		$_CSS = doCSS('admin');
	}

	if(isset($_JS))
	{
		$_JS .= doJS('jquery');
	}
	else
	{
		$_JS = doJS('jquery');
	}

	$_JS .= doJS('jquery.superbox-min');
	$_CSS .= doCSS('jquery.superbox');
	$_JS .= doJS('adminbody');
?>
