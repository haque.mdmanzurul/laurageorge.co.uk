<?php require_once("../_header.php"); ?>
<h1>Colours</h1>
<p>Below is a list of your colours in alphabetical order.</p>

<?php
$Query = @mysql_query("SELECT * FROM `colours` ORDER BY `name` ASC");
if(mysql_num_rows($Query)!=0)
{
	echo '<table width="" border="0" cellspacing="0" cellpadding="0" id="tlist">
	<tr>
	<th width="600">Colour Name</th>
	<th width="150">Sample</th>
	<th width="150">Action</th>
	</tr>';

	while($Row = mysql_fetch_array($Query))
	{
		echo '<tr>';
		echo '<td>'.htmlentities(stripslashes($Row['name'])).'</td>';
		echo '<td><span class="colourblock" style="background-color:#'.$Row['hex'].';">&nbsp;</span></td>';
		echo '<td><a href="edit.php?id='.$Row['id'].'">[Edit]</a></td>';
		echo '</td>';
	}
	
	echo '</table>';
}
else
	echo '<p>Woops! You have no colours.</p>';
?>

<?php require_once("../_footer.php"); ?>