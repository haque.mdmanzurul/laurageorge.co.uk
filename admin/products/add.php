<?php require_once("../_header.php"); ?>
<h1>Add a Print</h1>

<?php

if(array_key_exists("Update", $_POST) && $_POST['Update'])
{
	ini_set('mem_size', 32000);

	//echo '<pre>';
	//print_r($_FILES);
	//exit;
	
	if(empty($_POST['name']))
		echo '<p>Please go back and fill in all fields.</p>';
	else
	{
		$EndSize = "";
		foreach($_POST['size'] as $ID=>$Value)
			$EndSize .= $ID . ',';
		$EndSize = substr($EndSize, 0, -1);
		
		$EndColour = "";
		if(array_key_exists("colour", $_POST))
		{
			foreach($_POST['colour'] as $ID=>$Value)
				$EndColour .= $ID . ',';
		}

		$EndColour = substr($EndColour, 0, -1);
		
		@mysql_query("INSERT INTO `products` SET `name` = '".c(htmlentities($_POST['name'], ENT_COMPAT, 'UTF-8'))."', 
												`description` = '".c(htmlentities($_POST['description'], ENT_COMPAT, 'UTF-8'))."', 
												`c_id` = '".c($_POST['category'])."', 
												`price` = '".c($_POST['price'])."',
												`sizes` = '$EndSize',
												`colours` = '$EndColour'") or die(mysql_error());
		$_ID = mysql_insert_id();
		
		for($i=0;$i<4;$i++) {
		
		if($i == 0) {
			$is = '';
		} else {
			$is = $i;
		}
		
		$file_type = $_FILES['img'.$is]['type'];
		$file_name = $_FILES['img'.$is]['name'];
		$file_size = $_FILES['img'.$is]['size'];
		$file_tmp = $_FILES['img'.$is]['tmp_name'];
		
		
	   //keep image type
	   if($file_size > 0)
	   {
		   $image = $norm_img = open_image(urldecode($file_tmp));
		   if ($image == false) { echo 'IMGAE COULD NOT BE RESIZED'; }
		   
			// Get original width and height
			$width = imagesx($image);
			$height = imagesy($image);
			
			$new_width = 300;
			$new_height = 199;
			
			// create a new true color image
			$canvas = imagecreatetruecolor( $new_width, $new_height );
			
			// don't allow new width or height to be greater than the original
			if( $new_width > $width ) { $new_width = $width; }
			if( $new_height > $height ) { $new_height = $height; }

			$_W = $new_width;
			$_H = $new_height;
			
			if($width > $new_width)
			{
				$_P = ($new_width/$width);
				$_W = $_P * $width;
				$_H = $_P * $height;
				
				$_TS = imagecreatetruecolor($_W, $_H);
				imagecopyresampled($_TS, $image, 0, 0, 0, 0, $_W, $_H, $width, $height);
				$width = imagesx($_TS);
				$height = imagesy($_TS);
				$image = $_TS;
			}
			
			if($height > $new_height)
			{
				$_P = ($new_height/$height);
				$_W = $_P * $width;
				$_H = $_P * $height;
				
				$_TS = imagecreatetruecolor($_W, $_H);
		  		$im_bg = imagecolorallocate($_TS, 255, 255, 255);
		  		imagefill($_TS, 0, 0, $im_bg); 
				imagecopyresampled($_TS, $image, 0, 0, 0, 0, $_W, $_H, $width, $height);
				$width = imagesx($_TS);
				$height = imagesy($_TS);
				$image = $_TS;
			}

		   $out = imagecreatetruecolor(300, 199);
		   $im_bg = imagecolorallocate($out, 255, 255, 255);
		   imagefill($out, 0, 0, $im_bg); 
		   imagecopyresampled($out, $image, ((300 - $width) / 2), ((199 - $height) / 2), 0, 0, $_W, $_H, $width, $height );
			
			@mkdir('../../i/products/' . $_ID . '/', 0777);
			@imagejpeg($out, '../../i/products/' . $_ID . '/thumb'.$is.'.jpg', 80);
			@chmod('../../i/products/' . $_ID . '/thumb.jpg', 0777);
			
			if($i > 0) {
				move_uploaded_file($file_tmp, '../../i/products/' . $_ID . '/alt'.$i.'.jpg')	;
			}
		}
		}
		
		foreach($_FILES['cimg']['name'] as $ColourID=>$File)
		{
			$file_type = $_FILES['cimg']['type'][$ColourID];
			$file_name = $_FILES['cimg']['name'][$ColourID];
			$file_size = $_FILES['cimg']['size'][$ColourID];
			$file_tmp = $_FILES['cimg']['tmp_name'][$ColourID];
			
		   //keep image type
		   if($file_size > 0)
		   {
			   $image = $norm_img = open_image(urldecode($file_tmp));
			   if ($image == false) { echo 'IMGAE COULD NOT BE RESIZED'; }
			   
				// Get original width and height
				$width = imagesx($image);
				$height = imagesy($image);
				
				$new_width = 300;
				$new_height = 199;
				
				// create a new true color image
				$canvas = imagecreatetruecolor( $new_width, $new_height );
				
				// don't allow new width or height to be greater than the original
				if( $new_width > $width ) { $new_width = $width; }
				if( $new_height > $height ) { $new_height = $height; }

				$_W = $new_width;
				$_H = $new_height;
				
				if($width > $new_width)
				{
					$_P = ($new_width/$width);
					$_W = $_P * $width;
					$_H = $_P * $height;
					
					$_TS = imagecreatetruecolor($_W, $_H);
					imagecopyresampled($_TS, $image, 0, 0, 0, 0, $_W, $_H, $width, $height);
					$width = imagesx($_TS);
					$height = imagesy($_TS);
					$image = $_TS;
				}
				
				if($height > $new_height)
				{
					$_P = ($new_height/$height);
					$_W = $_P * $width;
					$_H = $_P * $height;
					
					$_TS = imagecreatetruecolor($_W, $_H);
					$im_bg = imagecolorallocate($_TS, 255, 255, 255);
					imagefill($_TS, 0, 0, $im_bg); 
					imagecopyresampled($_TS, $image, 0, 0, 0, 0, $_W, $_H, $width, $height);
					$width = imagesx($_TS);
					$height = imagesy($_TS);
					$image = $_TS;
				}

				$out = imagecreatetruecolor(300, 199);
				$im_bg = imagecolorallocate($out, 255, 255, 255);
				imagefill($out, 0, 0, $im_bg); 
				@imagecopyresampled($out, $image, ((300 - $width) / 2), ((199 - $height) / 2), 0, 0, $_W, $_H, $width, $height );
				
				@mkdir('../../i/products/' . $_ID . '/', 0777);
				imagejpeg($out, '../../i/products/' . $_ID . '/'.$ColourID.'_t.jpg', 80);
				imagejpeg($norm_img, '../../i/products/' . $_ID . '/'.$ColourID.'_b.jpg', 100);
				
			}
		}
		
		echo '<p style="font-weight:bold;">Item was Added!</p>';
	}
}

?>
<form action="" method="post" enctype="multipart/form-data">
<h2>Product Details</h2>
<table width="400" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td width="150" align="right" valign="top"><strong>Name:</strong></td>
		<td><input type="text" name="name" value="<?php echo stripslashes($Row['name']); ?>" /></td>
	</tr>
	<tr>
		<td align="right" valign="top"><strong>Category:</strong></td>
		<td><?php echo buildCategories($Row['c_id']); ?></td>
	</tr>
	<tr>
		<td align="right" valign="top"><strong>Description:</strong></td>
		<td><textarea name="description" cols="60" rows="7"><?php echo stripslashes($Row['name']); ?></textarea></td>
	</tr>
	<tr>
		<td align="right" valign="top"><strong>Price:</strong></td>
		<td><input type="text" name="price" value="<?php echo stripslashes($Row['price']); ?>" /></td>
	</tr>
	
</table>
<div style="padding:1px; float:top; width:30px;">
<tr>
		
		<td><strong>Main Image:</strong></td>
		<td><input name="img" type="file" /></td>
		
	
		<td> <strong>thumb1:</strong></td>
		<td> <input name="img1" type="file" /></td>
		<td> <strong>thumb2:</strong></td>
		<td><input name="img2" type="file" /></td>
		<td><strong>thumb3:</strong></td>
		<td><input name="img3" type="file" /></td>
		
	</tr>
</div>
<div style="padding:10px;float:left;width:150px;">
	<h2>Product Sizes</h2>
	<?php echo buildSizes($Row['sizes']); ?>
</div>

<div style="padding:10px;float:left;width:400px;">
	<h2>Product Colours &amp; Images</h2>
	<?php echo buildColours($Row['colours'], $_ID); ?>
</div>

<div style="clear:both;"></div>

<input type="submit" name="Update" value="     Add Product     " />
</form>

<?php require_once("../_footer.php"); ?>