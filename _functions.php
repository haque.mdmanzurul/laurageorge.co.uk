<?php
	session_cache_expire(360);
	session_start();
	
	$mysql_connection = @mysql_connect('localhost', 'root', '');
	$mysql_database = @mysql_select_db('laurageorge', $mysql_connection);
	


	function doJS($file)
	{
		//$time = filemtime($_SERVER['DOCUMENT_ROOT'] . '/i/js/' . $file . '.js');
		return '<script src="/i/js/'.$file.'.js?/" type="text/javascript"></script>' . "\n";
	}
	
	function doCSS($file)
	{
		//$time = filemtime($_SERVER['DOCUMENT_ROOT'] . '/i/css/' . $file . '.css');
		return '<link href="/i/css/'.$file.'.css" rel="stylesheet" type="text/css" />' . "\n";
	}
	
	function getPages()
	{
		$_PAGES[1]['Name'] = 'Blog';
		$_PAGES[1]['Blocks'] = 'Y';
		$_PAGES[2]['Name'] = 'About';
		$_PAGES[2]['Blocks'] = 'N';
		$_PAGES[3]['Name'] = 'Privacy';
		$_PAGES[3]['Blocks'] = 'N';
		$_PAGES[4]['Name'] = 'Press';
		$_PAGES[4]['Blocks'] = 'Y';
		$_PAGES[5]['Name'] = 'Events';
		$_PAGES[5]['Blocks'] = 'Y';
		$_PAGES[6]['Name'] = 'Testimonials';
		$_PAGES[6]['Blocks'] = 'Y';
		$_PAGES[7]['Name'] = 'Info';
		$_PAGES[7]['Blocks'] = 'N';
		$_PAGES[8]['Name'] = 'Fitting &amp; Care';
		$_PAGES[8]['Blocks'] = 'N';
		$_PAGES[9]['Name'] = 'History &amp; Tradition';
		$_PAGES[9]['Blocks'] = 'N';
		$_PAGES[10]['Name'] = 'Coture';
		$_PAGES[10]['Blocks'] = 'N';
		$_PAGES[11]['Name'] = 'Stockists';
		$_PAGES[11]['Blocks'] = 'Y';
		$_PAGES[12]['Name'] = 'Terms &amp; Conditions';
		$_PAGES[12]['Blocks'] = 'N';
		
		return $_PAGES;	
	}
	
	function showPage($PID)
	{
		$_PAGES = getPages();
		
		$Query = @mysql_query("SELECT * FROM `pages` WHERE `id` = {$PID}");
		if(@mysql_num_rows($Query) != 0)
		{
			$Row = @mysql_fetch_assoc($Query);
			$Row = makeW3C($Row);
			
			$tmp = '<h1>'.$Row['title'].'</h1>';
			
			$Image = ($_PAGES[$PID]['Blocks']=='Y') ? '1_' . $PID . '.jpg' : $PID . '.jpg';
			
			if(file_exists($_SERVER['DOCUMENT_ROOT'] . '/i/pages/' . $Image))
				$tmp .= '<img src="/i/pages/'.$Image.'" alt="" style="float:right;margin-left:15px;" />';
				
			if($_PAGES[$PID]['Blocks']=='Y')
			{
				$Query2 = @mysql_query("SELECT * FROM `page_blocks` WHERE `p_id` = {$PID} AND `viewable` = 'Y' ORDER BY `id` DESC");
				if(@mysql_num_rows($Query2) != 0)
				{
					while($Row2 = @mysql_fetch_assoc($Query2))
					{
						$Row2 = makeW3C($Row2);
						
						$PageBlocks .= '<div style="clear:both;"></div>';
						
						if(file_exists($_SERVER['DOCUMENT_ROOT'] . '/i/pages/' . $PID . '_' . $Row2['id'] . '.jpg'))
							$PageBlocks .= '<img src="/i/pages/' . $PID . '_' . $Row2['id'] . '.jpg" alt="" style="float:right;margin-left:15px;" />';
						
						$PageBlocks .= '<h1>'.$Row2['title'].'</h1>';
						$PageBlocks .= $Row2['content'];
					}
				}
			}
			
			$_PAGE['Title'] = $Row['title'];
			$_PAGE['Keywords'] = $Row['keywords'];
			$_PAGE['Description'] = $Row['description'];
			$_PAGE['Content'] = html_entity_decode($tmp . $Row['content'] . $PageBlocks);
		}
		else
		{
			$_PAGE['Title'] = "Page not found";
			$_PAGE['Keywords'] = "page, not, found";
			$_PAGE['Description'] = "This page cannot be found.";
			$_PAGE['Content'] = "<h1>Page not found</h1><p>This page cannot be found.</p>";
		}
		
		return $_PAGE;
	}
	
	function makeW3C($Row)
	{
		foreach($Row as $key=>$value)
			$Row[$key] = htmlentities(stripslashes($value));
		
		return $Row;
	}
	
	function getColours()
	{
		$Query = @mysql_query("SELECT * FROM `colours`");	
		while($Row = @mysql_fetch_assoc($Query))
		{
			$_COLOURS[$Row['id']]['Name'] = $Row['name'];
			$_COLOURS[$Row['id']]['Hex'] = $Row['hex'];
		}
			
		return $_COLOURS;
	}
	
	function showSideLinks($SideLinks, $Type = 0)
	{
		echo '<ul class="links">';
		
		$_DOCX = ($Type==1) ? $_SERVER['REQUEST_URI'] : $_SERVER['PHP_SELF'];
		
		foreach($SideLinks as $URL=>$Name)
		{
			if(is_array($Name))
			{
				if($URL=='/news/')
				{
					if(strstr($_DOCX, $URL))
						echo '<li><a href="#" class="on" onclick="$(\'#news_section\').toggle();">'.$Name[''].'</a><ul style="display:block;" id="news_section">';
					else
						echo '<li><a href="#" onclick="$(\'#news_section\').toggle();">'.$Name[''].'</a><ul style="display:none;" id="news_section">';
				}
				else
				{
					if(strstr($_DOCX, $URL))
						echo '<li><a href="'.$URL.'" class="on">'.$Name[''].'</a><ul>';
					else
						echo '<li><a href="'.$URL.'">'.$Name[''].'</a><ul style="display:none;">';
				}
				
				foreach($Name as $URL2=>$Name2)
				{
					if($URL2!= '')
						echo ($_DOCX == $URL2) ? '<li><a href="'.$URL2.'" class="on">'.$Name2.'</a></li>' : '<li><a href="'.$URL2.'">'.$Name2.'</a></li>';
				}
				
				echo '</ul></li>';
			}
			else
			{
				if(is_array($Name))
					$Name = $Name[''];
				
				echo ($_DOCX == $URL) ? '<li><a href="'.$URL.'" class="on">'.$Name.'</a></li>' : '<li><a href="'.$URL.'">'.$Name.'</a></li>';
			}
		}
		echo '</ul>';
	}
	
	function showColours($Colours)
	{
		$_COLOURS = getColours();
		$HTML = '<select name="colour" id="colourSelect">';
		$ColourArray = explode(',', $Colours);
		foreach($ColourArray as $Colour)
		{
			if($Colour != '')
				$HTML .= '<option value="'.$Colour.'">'.htmlentities($_COLOURS[$Colour]['Name']).'</option>';
		}
		$HTML .= '</select>';
		return $HTML;
	}
	
	function showSizes($Sizes)
	{
		$_SIZES = getSizes();
		$HTML = '<select name="size">';
		$SizeArray = explode(',', $Sizes);
		foreach($SizeArray as $Size)
		{
			if($Size != '')
				$HTML .= '<option value="'.$Size.'">'.htmlentities($_SIZES[$Size]).'</option>';
		}
		$HTML .= '</select>';
		return $HTML;
	}
	
	function showCountries($County)
	{
		$_COUNTRIES = getCountries();
		$HTML = '<select name="country">';
		foreach($_COUNTRIES as $ID=>$Name)
		{
			if($ID == $County)
				$HTML .= '<option value="'.$ID.'" selected="selected">'.htmlentities($Name).'</option>';
			else
				$HTML .= '<option value="'.$ID.'">'.htmlentities($Name).'</option>';
		}
		$HTML .= '</select>';
		return $HTML;
	}
	
	function getDeliveryPrice($CountryCode)
	{
		$Query = @mysql_query("SELECT * FROM `delivery` WHERE `cc` = '$CountryCode'");
		$Row = @mysql_fetch_assoc($Query);
		$EndPrice = (isset($Row['price'])==false) ? 6.50 : $Row['price'];
		
		return $EndPrice;
	}
	
	function printColours($Colours, $Type = 0, $PID = 0)
	{
		$_COLOURS = getColours();
		$ColourArray = explode(',', $Colours);
		foreach($ColourArray as $Colour)
		{
			if($Colour != '')
			{
				echo ($Type==1) ? '<a class="colourblock" style="background-color:#'.$_COLOURS[$Colour]['Hex'].';" onclick="javascript:switchImage('.$Colour.', '.$PID.');">&nbsp;</a>' : '<span class="colourblock" style="background-color:#'.$_COLOURS[$Colour]['Hex'].';">&nbsp;</span>';
			}
		}
	}
	
	function getSizes()
	{
		$Query = @mysql_query("SELECT * FROM `sizes`");	
		while($Row = @mysql_fetch_assoc($Query))
			$_SIZES[$Row['id']] = $Row['name'];
			
		return $_SIZES;
	}
	
	function is_valid_email($email) {
	  $result = TRUE;
	  if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", $email)) {
		$result = FALSE;
	  }
	  return $result;
	}
	
	function redirect($url) {
	   if (!headers_sent())
		   header('Location: '.$url);
	   else {
		   echo '<script type="text/javascript">';
		   echo 'window.location.href="'.$url.'";';
		   echo '</script>';
		   echo '<noscript>';
		   echo '<meta http-equiv="refresh" content="0;url='.$url.'" />';
		   echo '</noscript>';
	   }
	}
	
	if(isset($_GET['SD']) && $_GET['SD']==TRUE)
		session_destroy();
		
		
	function c($s)
	{
		return mysql_real_escape_string($s);	
	}
	
	function base64Decode($scrambled)
	{
		$scrambled = str_replace(" ","+", $scrambled);
		$output = base64_decode($scrambled);
		return $output;
	}
	
	function base64Encode($plain)
	{
		$output = base64_encode($plain);
		return $output;
	}
	
	function simpleXor($String, $Key)
	{
		$KeyList = array();
		
		for($i = 0; $i < strlen($Key); $i++)
			$KeyList[$i] = ord(substr($Key, $i, 1));
		
		for($i = 0; $i < strlen($String); $i++)
			$output.= chr(ord(substr($String, $i, 1)) ^ ($KeyList[$i % strlen($Key)]));
		
		return $output;
	}
	
	function getCountries()
	{
	 return array(
'GB'=>'United Kingdom',
'AF'=>'Afghanistan',
'AL'=>'Albania',
'DZ'=>'Algeria',
'AS'=>'American Samoa',
'AD'=>'Andorra',
'AO'=>'Angola',
'AI'=>'Anguilla',
'AQ'=>'Antarctica',
'AG'=>'Antigua And Barbuda',
'AR'=>'Argentina',
'AM'=>'Armenia',
'AW'=>'Aruba',
'AU'=>'Australia',
'AT'=>'Austria',
'AZ'=>'Azerbaijan',
'BS'=>'Bahamas',
'BH'=>'Bahrain',
'BD'=>'Bangladesh',
'BB'=>'Barbados',
'BY'=>'Belarus',
'BE'=>'Belgium',
'BZ'=>'Belize',
'BJ'=>'Benin',
'BM'=>'Bermuda',
'BT'=>'Bhutan',
'BO'=>'Bolivia',
'BA'=>'Bosnia And Herzegovina',
'BW'=>'Botswana',
'BV'=>'Bouvet Island',
'BR'=>'Brazil',
'IO'=>'British Indian Ocean Territory',
'BN'=>'Brunei',
'BG'=>'Bulgaria',
'BF'=>'Burkina Faso',
'BI'=>'Burundi',
'KH'=>'Cambodia',
'CM'=>'Cameroon',
'CA'=>'Canada',
'CV'=>'Cape Verde',
'KY'=>'Cayman Islands',
'CF'=>'Central African Republic',
'TD'=>'Chad',
'CL'=>'Chile',
'CN'=>'China',
'CX'=>'Christmas Island',
'CC'=>'Cocos (Keeling) Islands',
'CO'=>'Columbia',
'KM'=>'Comoros',
'CG'=>'Congo',
'CK'=>'Cook Islands',
'CR'=>'Costa Rica',
'CI'=>'Cote D\'Ivorie (Ivory Coast)',
'HR'=>'Croatia (Hrvatska)',
'CU'=>'Cuba',
'CY'=>'Cyprus',
'CZ'=>'Czech Republic',
'CD'=>'Democratic Republic Of Congo (Zaire)',
'DK'=>'Denmark',
'DJ'=>'Djibouti',
'DM'=>'Dominica',
'DO'=>'Dominican Republic',
'TP'=>'East Timor',
'EC'=>'Ecuador',
'EG'=>'Egypt',
'SV'=>'El Salvador',
'GQ'=>'Equatorial Guinea',
'ER'=>'Eritrea',
'EE'=>'Estonia',
'ET'=>'Ethiopia',
'FK'=>'Falkland Islands (Malvinas)',
'FO'=>'Faroe Islands',
'FJ'=>'Fiji',
'FI'=>'Finland',
'FR'=>'France',
'FX'=>'France, Metropolitan',
'GF'=>'French Guinea',
'PF'=>'French Polynesia',
'TF'=>'French Southern Territories',
'GA'=>'Gabon',
'GM'=>'Gambia',
'GE'=>'Georgia',
'DE'=>'Germany',
'GH'=>'Ghana',
'GI'=>'Gibraltar',
'GR'=>'Greece',
'GL'=>'Greenland',
'GD'=>'Grenada',
'GP'=>'Guadeloupe',
'GU'=>'Guam',
'GT'=>'Guatemala',
'GN'=>'Guinea',
'GW'=>'Guinea-Bissau',
'GY'=>'Guyana',
'HT'=>'Haiti',
'HM'=>'Heard And McDonald Islands',
'HN'=>'Honduras',
'HK'=>'Hong Kong',
'HU'=>'Hungary',
'IS'=>'Iceland',
'IN'=>'India',
'ID'=>'Indonesia',
'IR'=>'Iran',
'IQ'=>'Iraq',
'IE'=>'Ireland',
'IL'=>'Israel',
'IT'=>'Italy',
'JM'=>'Jamaica',
'JP'=>'Japan',
'JO'=>'Jordan',
'KZ'=>'Kazakhstan',
'KE'=>'Kenya',
'KI'=>'Kiribati',
'KW'=>'Kuwait',
'KG'=>'Kyrgyzstan',
'LA'=>'Laos',
'LV'=>'Latvia',
'LB'=>'Lebanon',
'LS'=>'Lesotho',
'LR'=>'Liberia',
'LY'=>'Libya',
'LI'=>'Liechtenstein',
'LT'=>'Lithuania',
'LU'=>'Luxembourg',
'MO'=>'Macau',
'MK'=>'Macedonia',
'MG'=>'Madagascar',
'MW'=>'Malawi',
'MY'=>'Malaysia',
'MV'=>'Maldives',
'ML'=>'Mali',
'MT'=>'Malta',
'MH'=>'Marshall Islands',
'MQ'=>'Martinique',
'MR'=>'Mauritania',
'MU'=>'Mauritius',
'YT'=>'Mayotte',
'MX'=>'Mexico',
'FM'=>'Micronesia',
'MD'=>'Moldova',
'MC'=>'Monaco',
'MN'=>'Mongolia',
'MS'=>'Montserrat',
'MA'=>'Morocco',
'MZ'=>'Mozambique',
'MM'=>'Myanmar (Burma)',
'NA'=>'Namibia',
'NR'=>'Nauru',
'NP'=>'Nepal',
'NL'=>'Netherlands',
'AN'=>'Netherlands Antilles',
'NC'=>'New Caledonia',
'NZ'=>'New Zealand',
'NI'=>'Nicaragua',
'NE'=>'Niger',
'NG'=>'Nigeria',
'NU'=>'Niue',
'NF'=>'Norfolk Island',
'KP'=>'North Korea',
'MP'=>'Northern Mariana Islands',
'NO'=>'Norway',
'OM'=>'Oman',
'PK'=>'Pakistan',
'PW'=>'Palau',
'PA'=>'Panama',
'PG'=>'Papua New Guinea',
'PY'=>'Paraguay',
'PE'=>'Peru',
'PH'=>'Philippines',
'PN'=>'Pitcairn',
'PL'=>'Poland',
'PT'=>'Portugal',
'PR'=>'Puerto Rico',
'QA'=>'Qatar',
'RE'=>'Reunion',
'RO'=>'Romania',
'RU'=>'Russia',
'RW'=>'Rwanda',
'SH'=>'Saint Helena',
'KN'=>'Saint Kitts And Nevis',
'LC'=>'Saint Lucia',
'PM'=>'Saint Pierre And Miquelon',
'VC'=>'Saint Vincent And The Grenadines',
'SM'=>'San Marino',
'ST'=>'Sao Tome And Principe',
'SA'=>'Saudi Arabia',
'SN'=>'Senegal',
'SC'=>'Seychelles',
'SL'=>'Sierra Leone',
'SG'=>'Singapore',
'SK'=>'Slovak Republic',
'SI'=>'Slovenia',
'SB'=>'Solomon Islands',
'SO'=>'Somalia',
'ZA'=>'South Africa',
'GS'=>'South Georgia And South Sandwich Islands',
'KR'=>'South Korea',
'ES'=>'Spain',
'LK'=>'Sri Lanka',
'SD'=>'Sudan',
'SR'=>'Suriname',
'SJ'=>'Svalbard And Jan Mayen',
'SZ'=>'Swaziland',
'SE'=>'Sweden',
'CH'=>'Switzerland',
'SY'=>'Syria',
'TW'=>'Taiwan',
'TJ'=>'Tajikistan',
'TZ'=>'Tanzania',
'TH'=>'Thailand',
'TG'=>'Togo',
'TK'=>'Tokelau',
'TO'=>'Tonga',
'TT'=>'Trinidad And Tobago',
'TN'=>'Tunisia',
'TR'=>'Turkey',
'TM'=>'Turkmenistan',
'TC'=>'Turks And Caicos Islands',
'TV'=>'Tuvalu',
'UG'=>'Uganda',
'UA'=>'Ukraine',
'AE'=>'United Arab Emirates',
'GB'=>'United Kingdom',
'US'=>'United States',
'UM'=>'United States Minor Outlying Islands',
'UY'=>'Uruguay',
'UZ'=>'Uzbekistan',
'VU'=>'Vanuatu',
'VA'=>'Vatican City (Holy See)',
'VE'=>'Venezuela',
'VN'=>'Vietnam',
'VG'=>'Virgin Islands (British)',
'VI'=>'Virgin Islands (US)',
'WF'=>'Wallis And Futuna Islands',
'EH'=>'Western Sahara',
'WS'=>'Western Samoa',
'YE'=>'Yemen',
'YU'=>'Yugoslavia',
'ZM'=>'Zambia',
'ZW'=>'Zimbabwe'
);
	}
$_CSS ="";
$_JS = "";
	$_TITLE = 'Laura George - Exquisite Bridal Garters';
	$_KEYWORDS = 'Exquisite Bridal Garters';
	$_DESCRIPTION = 'Exquisite Bridal Garters';

	$_CSS .= doCSS('body');
	$_JS .= doJS('jquery');
	
?>
