
<div id="footer">
	<a href="/collections.html">COLLECTIONS</a> | <a href="/about.html">ABOUT US</a> | <a href="/contact.html">CONTACT US</a> | <a href="/news/">NEWS</a> | <a href="/information/index.html">INFO</a>  | <a href="/information/terms_and_conditions.html">TERMS &amp; CONDITIONS</a> | <a href="/information/privacy_and_security.html">PRIVACY POLICY</a><br />
	Address: 50 Harlow Crescent, Harrogate, North Yorkshire, HG2 0AJ, UK. Phone +44 (0) 7534 65 22 82
	<p><strong>&copy; Laura George. All rights reserved. | site by <a href="http://www.loyaltymatters.co.uk/" target="_blank">loyaltyMATTERS</a></strong></p>
</div>

</div>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-15011131-1");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
