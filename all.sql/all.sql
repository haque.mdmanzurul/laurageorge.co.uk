-- Database Export Class----------------- 
-- Database Name laurageorge----------------- 
-- Class Created BY Very Shafrudin--------------- 
-- Struktur Tabel blog -------------------- 
CREATE TABLE IF NOT EXISTS `blog` ( 
 `id` int(11) NOT NULL auto_increment, 
`title` varchar(200) NOT NULL , 
`content` text NOT NULL , 
`date` int(11) NOT NULL , 
`viewable` set('Y','N') NOT NULL  , 
PRIMARY KEY (`id`), 
KEY `date` (`date`), 
KEY `viewable` (`viewable`) 
 ); 
-- Data Tabel blog -------------------- 
INSERT INTO `blog` (`id`, `title`, `content`, `date`, `viewable` ) VALUES ('3','19th January 2012 - New Blog','Welcome to the new and exciting Laura George blog, launched today for 2012. We aim to keep you updated with all our new designs and upcoming events. Along with all that is new in lingerie and the bridal world. Cant wait to get started!   Laura x','1327003975','Y' ), 
 ('4','Something Blue - Exclusive Boutique Collection','At the moment we are in the process of launching the \'Something Blue\'garter collection. New for 2012 and exclusive to our bridal boutique stockists. All wedding garters include an element of \'Something Blue\' as we have found this to be very popular with brides. Olivia is one of our best sellers.','1327607939','Y' ), 
 ('5','Boutique Wedding Garter - Iris','Here is the first of the collection of wedding garters that are exclusive to our stockists. (Please see stockist page for info). Iris Wedding garter is made from ivory lace and soft tulle. This garter design is embellished with blue satin ribbon and pearl ring. Iris wedding garter is also available in white.','1327950734','Y' ), 
 ('6','Boutique Wedding Garter - Poppy','Here is a peek at our second exclusive boutique wedding garter. Handmade from antique style lace, this bridal garter is available in white or ivory. It is embellished with a small blue velvet bow and crystal. Ideal for the something blue element or your wedding!','1328122350','Y' ), 
 ('7','Boutique Wedding Garter - Evie','Here is the third of the \'Something Blue\' garter collection that is exclusive to our stockists. Made from spot net tulle and embellished with a a beautiful small blue bow, highlighted with a blue Swarovski crystal. This wedding garter is a favorite of ours!','1328382814','Y' ), 
 ('8','New Stockist - Orchid Designs','We are pleased to announce we have another stockist of Laura George Garters - Orchid Designs in Clitheroe. For full details check out our stockist page!','1329479353','Y' ), 
 ('9','Moda Show 2012','We went to the Moda show in Birmingham last week to check out all the latest trends that are coming through in fashion and accessories. We saw a lot of chiffon and silk which will be great for making wedding garters and lingerie.','1330335816','Y' ), 
 ('10','Bed Confetti','Why not try some of our bed confetti, its great for setting the mood for romance on your wedding night!','1330939586','Y' ), 
 ('11','Love Catcher','We have introduced a beautiful love catcher which can be hung above your bed on your wedding night!','1331031004','Y' ), 
 ('12','Offer of the Month','Here at Laura George the team are in the process of putting together offer of the month! Something new that we have wanted to launch for 2012!','1331808844','Y' ), 
 ('13','April Offer','From now untill the end of April we are offering Olivia Swarovski crystal heart garter for just �18. This gorgeous wedding garter is normally �22. It is one of our best selling wedding garters and comes in white, ivory, pale blue and pale pink.','1332238396','Y' ), 
 ('15','The Harrogate Wedding Lounge','One of the most beautiful wedding boutiques in Harrogate is now even more beautiful - can you  believe it! Owner of The Harrogate Wedding lounge Hayley has just had a total refurb which includes an extra bridal dressing suite. Brides come from miles around for the unforgettable experience of finding THE dream dress. I would recommend The Harrogate Wedding Lounge to anyone as this is where I found my very own wedding dress! The service is amazing, to book in just call 01423 888242 or check out the website www.theharrogateweddinglounge.com you will not be disapointed!','1333534221','Y' ), 
 ('16','May Offer','We are very excited about our second May offer as it was very popular with our brides last month. For May we are offering Maisie garter for a fantastic �18.','1335860062','Y' ), 
 ('17','New Stockist - Mrs Jones Bridal Boutique','We are really pleased to announce that we have a new stockist for our wedding garters. The exclusive boutique Mrs Jones Bridal in Berkshire. For all info see our stockists page. ','1336570686','Y' ), 
 ('18','Lingerie Collection','Here at Laura George we are working hard to create an exclusive wedding lingerie collection. All designs will be made from silk and exquisite french chantilly lace. Then embellished with hand tied bows and Swarovski crystals.','1336570957','Y' ), 
 ('19','Mrs Jones Bridal - Accessories evening','Last week saw the launch of Laura George Wedding Garters at Mrs Jones Bridal Boutique accessories evening. The event was a massive success and enjoyed by all Mrs Jones Brides.','1336572716','Y' ), 
 ('24','Wedding Lingerie Lookbook','You saw it here first - our all new wedding lingerie lookbook. With the feel of a wedding day album our great new book gives you a taste of our wedding lingerie designs.','1346148670','Y' ), 
 ('21','New Stockist - Bridezillas Bristol','We are really pleased to announce that Bridezilla\'s in Bristol have been stocking our stunning garters for a few weeks now, along with lots of other gorgoues bridal accessories. For details please see our stockist page.','1343832982','Y' ), 
 ('22','Mrs G gets personal','We are realy excited about the launch of our personalised briefs. All are made from silk with a beautiful silk heart and real Swarovski crystals. There is also the option to design your own - just send us a quick email to www.laurageorge.co.uk and we will be able to put in your heart whatever you desire! ','1344166928','Y' ), 
 ('23','Ivory and Lace - Exciting Move','Ivory and Lace in Bedfordshire are moving as of September this year they will be located at Milton Barn. Please see our Stockists page for full details!','1345640432','Y' ), 
 ('25','Bridal Buyer','We are really pleased with our coverage from Bridal Buyer ahead of the British Bridal Exhibition. Check out our all updated directory on their website.','1346327881','Y' ), 
 ('26','Lucy - Limited edition Wedding Garter','Goodbye old friend! Lucy wedding garter is discontinued after a very popular season. This limited edition garter is out of stock, but you may be able to find one of the remaining garters on sale at some of our fabulous stockists!','1347197964','Y' ), 
 ('27','Madison - Swarovski Baroque crystal pendant and pearls','Welcome Madison! Check out our latest design, this is the creme de la creme of wedding garters. Made from soft tulle, plush backed elastic, double bow combination, Swarovski pearls and Swarovski Baroque pendant. A truly indulgent design. A slightly more expensive wedding garter - but worth every penny.','1347198183','Y' ), 
 ('28','North of England Wedding Awards','We\'d like to say a massive congratulations to Louise and all her team at Curvy Bridal in North Yorkshire. They have just been named Bridal Gown Retailer of the Year in the North of England Wedding Awards. A much deserved award for a fabulous boutique - whether you are a size 8 or 28 Curvy Bridal will have a gown (not to mention wedding garter!) to make you feel like a Princess on your special day. For all Curvy Bridal details see our Stockist page.','1349294645','Y' ), 
 ('29','Bridezillas - Stunning Window Display','We are so excited about the stunning window display in Bridesillas, why not pop down to Keynesham to take a look at one of our favorite stockists. Amanda has created one of the most striking displays with our personalised wedding breifs, spelling out Mrs B, Mrs R, Mrs I, Mrs D, Mrs E this Mrs bride is causing a bit of a stir on the high street! We love it! ','1357995885','Y' ), 
 ('30','Ellie Sanderson - The Dressing Room','We are really excited to announce that Ellie Sanderson has just opened a fantastic new boutique. The Dressing Room is just a short walk from the main boutique in Beaconsfield. The Dressing Room will offer a myriad of fabulous bridal accessories, our beautiful pieces included. The Dressing Room is a sumptuous environment perfect for finding all those final pieces to complete your individual bridal look. Book your appointment today on 01494 678157 or email thedressingroom@elliesanderson.com ','1360701145','Y' ), 
 ('31','Bridal Bus Shoot','We are very excited about the recent photo shoot with Lissa Alexandra Haines - www.lissaalexandraphotography.com This 50\'s inspired photo shoot was taken on some gorgeous vintage buses. Featured is Kitty Wedding garter in red. ','1367294623','Y' ), 
 ('32','On the Buses','We are very excited about the recent photo shoot with Lissa Alexandra Haines - www.lissaalexandraphotography.com This 50\'s inspired photo shoot was taken on some gorgeous vintage buses. Featured is Amelia Wedding Garter in black/red.','1367294678','Y' ), 
 ('33','50\'s Inspired photo shoot','We are very excited about the recent photo shoot with Lissa Alexandra Haines - www.lissaalexandraphotography.com This 50\'s inspired photo shoot was taken on some gorgeous vintage buses. Featured is Cosette Wedding Garter.','1367294757','Y' ), 
 ('34','New Stockist - The Village Boutique','GLAMOROUS, INDULGENT, AFFORDABLE We are very excited to introduce a new stockist - The Village Boutique in Kinver, for all details see our stockist page. This luxurious boutique stocks our gorgeous products alongside other beautiful accessories, shoes and gifts. The boutique offers a unique and delightful shopping experience.','1393774952','Y' ); 
 INSERT INTO `blog` (`id`, `title`, `content`, `date`, `viewable` ) VALUES ('35','New Stockist - Stately Brides','We would like to welcome a fantastic new stockist to Laura George. Stately Brides in Belper, Derbyshire will now be stocking our beautiful lingerie for all their gorgeous brides. For all their details see our stockists page.','1401894400','Y' ); 
-- Struktur Tabel categories -------------------- 
CREATE TABLE IF NOT EXISTS `categories` ( 
 `id` int(11) NOT NULL auto_increment, 
`name` varchar(50) NOT NULL , 
`description` text NOT NULL , 
`order` tinyint(4) NOT NULL  , 
PRIMARY KEY (`id`), 
KEY `order` (`order`) 
 ); 
-- Data Tabel categories -------------------- 
INSERT INTO `categories` (`id`, `name`, `description`, `order` ) VALUES ('1','Wedding Garters','A truly luxurious collection of wedding garters perfect for that wedding garter tradition. All of the wedding garters are handmade in the UK from luxurious fabrics such as silks, soft tulles and Chantilly lace.  All of our wedding garters are then embellished with Swarovski crystals and pearls, hand tied bows and beading. We have ivory garters, white garters, pale pink garters and blue garters - perfect for that \'something blue\' tradition.  All of our wedding garters come packaged in a beautiful branded gift box and are available in five sizes.','1' ), 
 ('2','Amour Des Fleurs Wedding Lingerie','This stunning wedding lingerie collection is handmade in the UK from crinkle silk chiffon and is embellished with a stunning floral applique. The flowers on the applique are laser cut with laser cut detail and then embroided with silver and ivory threads. The applique is then embellished with mirror sequins, clear beads, straw beads and pearls.  Pieces include strapless camisole, kimono, brief and bridal garter','3' ), 
 ('10','Wedding Nightwear','We have a gorgeous collection of wedding lingerie and lounge wear. We have picked some of our best pieces and combined them to make beautiful nightwear sets. Perfect for your wedding night, honeymoon and married life.  ','7' ), 
 ('3','Wedding Loungewear','This is a stunning lounge wear collection is perfect for any bridal occasion, wedding day or honeymoon.  It is the crème de la crème of wedding luxury and perfect for lounging around in. Pieces include silk lounge pant, super soft camisole and hand knitted woolen wrap. Our lounge wear collection can be worn with any of stunning wedding garters.','5' ), 
 ('4','Romance Wedding Lingerie','This wedding lingerie collection made from a full galloon lace and super soft satin silk. The floral lace design is fully corded and the silk has a slight stretch giving the perfect fit. Each lingerie piece is embellished with a tiny hand tied satin bow and real Swarovski crystal to give a truly luxurious finish. This collection includes silk and lace camisole and wrap, silk French knickers and lounge pant.  Any of our beautiful bridal garters can be worn with this collection.','4' ), 
 ('5','Personalised wedding Briefs','These gorgeous silk briefs are the perfect wedding lingerie. Embellished with a silk and heart and stunning Swarovksi crystals. This range is from Mrs A to Mrs Z, we also offer a truly personalised service where we can put in your heart whatever you desire!','6' ), 
 ('6','Boudoir','A truly stunning collection of garters perfect for the wedding night, honeymoon or those brides looking for something alternative.','9' ), 
 ('7','wedding Lingerie offer','Each month we will be launching a new offer, it may be a great giveaway or a voucher code that can be used at the checkout!','10' ), 
 ('8','Fairytale','This collection of wedding lingerie is handmade from crinkle silk chiffon, combined with the most delicate ethereal lace, hand tied satin bows and real Swarovski crystals.','8' ), 
 ('9','Chantilly Lace Wedding Lingerie','This exquisite collection of handmade wedding lingerie is a the most stunning of our ranges. High end and totally desirable. The combination of silk chiffon and Chantilly lace is above anything else you will see when looking for your wedding lingerie pieces.','2' ); 
 -- Struktur Tabel colours -------------------- 
CREATE TABLE IF NOT EXISTS `colours` ( 
 `id` int(11) NOT NULL auto_increment, 
`name` varchar(30) NOT NULL , 
`hex` varchar(6) NOT NULL  , 
PRIMARY KEY (`id`) 
 ); 
-- Data Tabel colours -------------------- 
INSERT INTO `colours` (`id`, `name`, `hex` ) VALUES ('1','Red','E31631' ), 
 ('2','Purple','7C58C6' ), 
 ('3','Silver','A7A6A1' ), 
 ('4','Gold','CCA564' ), 
 ('5','Black','000000' ), 
 ('6','Pink','FCE3E2' ), 
 ('7','Fuschia','F975B1' ), 
 ('8','Aubergine','615384' ), 
 ('9','White','FFFFFF' ), 
 ('10','Ivory','EADEC6' ), 
 ('11','Baby Blue','BCCDDD' ), 
 ('12','Dark Pink','F06578' ); 
 -- Struktur Tabel delivery -------------------- 
CREATE TABLE IF NOT EXISTS `delivery` ( 
 `id` int(11) NOT NULL auto_increment, 
`cc` varchar(3) NOT NULL , 
`price` decimal(10,2) NOT NULL  , 
PRIMARY KEY (`id`) 
 ); 
-- Data Tabel delivery -------------------- 
INSERT INTO `delivery` (`id`, `cc`, `price` ) VALUES ('1','GB','2.50' ); -- Struktur Tabel front -------------------- 
CREATE TABLE IF NOT EXISTS `front` ( 
 `p_id` int(11) NOT NULL  , 
UNIQUE KEY `p_id` (`p_id`) 
 ); 
-- Data Tabel front -------------------- 
INSERT INTO `front` (`p_id` ) VALUES ('1' ), 
 ('2' ), 
 ('3' ), 
 ('4' ); 
 -- Struktur Tabel lookbook -------------------- 
CREATE TABLE IF NOT EXISTS `lookbook` ( 
 `id` int(11) NOT NULL auto_increment, 
`small` varchar(255) NULL , 
`large` varchar(255) NULL , 
`display` smallint(6) NULL  , 
PRIMARY KEY (`id`) 
 ); 
-- Data Tabel lookbook -------------------- 
INSERT INTO `lookbook` (`id`, `small`, `large`, `display` ) VALUES ('108','/brochure/lookbook/jpegs/smaller/Beach2.jpg','/brochure/lookbook/jpegs/Beach2.jpg','14' ), 
 ('107','/brochure/lookbook/jpegs/smaller/Beach.jpg','/brochure/lookbook/jpegs/Beach.jpg','15' ), 
 ('104','/brochure/lookbook/jpegs/smaller/confetti.jpg','/brochure/lookbook/jpegs/confetti.jpg','10' ), 
 ('105','/brochure/lookbook/jpegs/smaller/Teddy-wrap.jpg','/brochure/lookbook/jpegs/Teddy-wrap.jpg','13' ), 
 ('101','/brochure/lookbook/jpegs/smaller/Bedroom.jpg','/brochure/lookbook/jpegs/Bedroom.jpg','12' ), 
 ('102','/brochure/lookbook/jpegs/smaller/Lounge-cami.jpg','/brochure/lookbook/jpegs/Lounge-cami.jpg','11' ), 
 ('98','/brochure/lookbook/jpegs/smaller/leaving.jpg','/brochure/lookbook/jpegs/leaving.jpg','8' ), 
 ('99','/brochure/lookbook/jpegs/smaller/castle.jpg','/brochure/lookbook/jpegs/castle.jpg','9' ), 
 ('94','/brochure/lookbook/jpegs/smaller/ring-2_edited-1.jpg','/brochure/lookbook/jpegs/ring-2_edited-1.jpg','6' ), 
 ('95','/brochure/lookbook/jpegs/smaller/Mrs-G.jpg','/brochure/lookbook/jpegs/Mrs-G.jpg','5' ), 
 ('133','/brochure/lookbook/jpegs/smaller/LAP-Copyright-017.jpg','/brochure/lookbook/jpegs/LAP-Copyright-017.jpg','34' ), 
 ('97','/brochure/lookbook/jpegs/smaller/cake-toast_edited-1.jpg','/brochure/lookbook/jpegs/cake-toast_edited-1.jpg','7' ), 
 ('109','/brochure/lookbook/jpegs/smaller/Front-cover-2.jpg','/brochure/lookbook/jpegs/Front-cover-2.jpg','1' ), 
 ('91','/brochure/lookbook/jpegs/smaller/Wed-morn-1.jpg','/brochure/lookbook/jpegs/Wed-morn-1.jpg','2' ), 
 ('92','/brochure/lookbook/jpegs/smaller/wed-morn-2.jpg','/brochure/lookbook/jpegs/wed-morn-2.jpg','3' ), 
 ('93','/brochure/lookbook/jpegs/smaller/wedding.jpg','/brochure/lookbook/jpegs/wedding.jpg','4' ), 
 ('110','/brochure/lookbook/jpegs/smaller/image.jpg','/brochure/lookbook/jpegs/image.jpg','16' ), 
 ('117','/brochure/lookbook/jpegs/smaller/layout4.jpg','/brochure/lookbook/jpegs/layout4.jpg','19' ), 
 ('116','/brochure/lookbook/jpegs/smaller/layout3.jpg','/brochure/lookbook/jpegs/layout3.jpg','18' ), 
 ('115','/brochure/lookbook/jpegs/smaller/layout2.jpg','/brochure/lookbook/jpegs/layout2.jpg','17' ), 
 ('118','/brochure/lookbook/jpegs/smaller/layout5.jpg','/brochure/lookbook/jpegs/layout5.jpg','20' ), 
 ('119','/brochure/lookbook/jpegs/smaller/layout6.jpg','/brochure/lookbook/jpegs/layout6.jpg','21' ), 
 ('120','/brochure/lookbook/jpegs/smaller/layout7.jpg','/brochure/lookbook/jpegs/layout7.jpg','22' ), 
 ('122','/brochure/lookbook/jpegs/smaller/layout8.jpg','/brochure/lookbook/jpegs/layout8.jpg','23' ), 
 ('123','/brochure/lookbook/jpegs/smaller/layout9.jpg','/brochure/lookbook/jpegs/layout9.jpg','24' ), 
 ('124','/brochure/lookbook/jpegs/smaller/layout10.jpg','/brochure/lookbook/jpegs/layout10.jpg','25' ), 
 ('125','/brochure/lookbook/jpegs/smaller/layout11.jpg','/brochure/lookbook/jpegs/layout11.jpg','26' ), 
 ('126','/brochure/lookbook/jpegs/smaller/layout13.jpg','/brochure/lookbook/jpegs/layout13.jpg','27' ), 
 ('127','/brochure/lookbook/jpegs/smaller/layout14.jpg','/brochure/lookbook/jpegs/layout14.jpg','28' ), 
 ('128','/brochure/lookbook/jpegs/smaller/layout15.jpg','/brochure/lookbook/jpegs/layout15.jpg','29' ); 
 INSERT INTO `lookbook` (`id`, `small`, `large`, `display` ) VALUES ('129','/brochure/lookbook/jpegs/smaller/layout16.jpg','/brochure/lookbook/jpegs/layout16.jpg','30' ), 
 ('130','/brochure/lookbook/jpegs/smaller/layout17.jpg','/brochure/lookbook/jpegs/layout17.jpg','31' ), 
 ('131','/brochure/lookbook/jpegs/smaller/layout18.jpg','/brochure/lookbook/jpegs/layout18.jpg','32' ), 
 ('132','/brochure/lookbook/jpegs/smaller/layout19.jpg','/brochure/lookbook/jpegs/layout19.jpg','33' ); 
 -- Struktur Tabel order_items -------------------- 
CREATE TABLE IF NOT EXISTS `order_items` ( 
 `o_id` int(11) NOT NULL , 
`p_id` int(11) NOT NULL , 
`quantity` int(11) NOT NULL , 
`size` int(11) NOT NULL , 
`colour` int(11) NOT NULL  , 
KEY `o_id` (`o_id`), 
KEY `p_id` (`p_id`), 
KEY `size` (`size`), 
KEY `colour` (`colour`) 
 ); 
-- Data Tabel order_items -------------------- 
INSERT INTO `order_items` (`o_id`, `p_id`, `quantity`, `size`, `colour` ) VALUES ('1','6','1','0','9' ), 
 ('2','6','1','0','9' ), 
 ('3','6','1','0','9' ), 
 ('3','16','1','0','9' ), 
 ('3','10','1','0','9' ), 
 ('3','14','3','0','10' ), 
 ('4','1','2','1','1' ), 
 ('5','1','2','1','1' ), 
 ('6','1','2','1','1' ), 
 ('6','13','1','5','10' ), 
 ('7','1','2','1','1' ), 
 ('7','13','1','5','10' ), 
 ('8','1','2','1','1' ), 
 ('8','13','1','5','10' ), 
 ('9','1','2','1','1' ), 
 ('9','13','1','5','10' ), 
 ('10','1','2','1','1' ), 
 ('10','13','1','5','10' ), 
 ('10','4','1','1','1' ), 
 ('11','1','2','1','1' ), 
 ('11','13','1','5','10' ), 
 ('11','4','1','1','1' ), 
 ('12','1','2','1','1' ), 
 ('12','13','1','5','10' ), 
 ('12','4','1','1','1' ), 
 ('13','1','2','1','1' ), 
 ('13','13','1','5','10' ), 
 ('13','4','1','1','1' ), 
 ('14','1','2','1','1' ), 
 ('14','13','1','5','10' ); 
 INSERT INTO `order_items` (`o_id`, `p_id`, `quantity`, `size`, `colour` ) VALUES ('14','4','1','1','1' ), 
 ('15','7','1','1','7' ), 
 ('16','6','1','3','9' ), 
 ('17','6','1','1','9' ), 
 ('18','6','1','1','9' ), 
 ('19','10','1','3','9' ), 
 ('20','6','1','1','9' ), 
 ('21','8','1','1','10' ), 
 ('22','9','1','2','11' ), 
 ('23','15','1','2','10' ), 
 ('24','14','1','4','9' ), 
 ('25','6','1','3','9' ), 
 ('26','15','1','1','10' ), 
 ('27','12','1','4','10' ), 
 ('28','15','1','4','10' ), 
 ('29','11','1','2','11' ), 
 ('30','6','1','3','9' ), 
 ('31','11','1','2','10' ), 
 ('32','15','1','3','10' ), 
 ('33','15','1','3','10' ), 
 ('34','10','1','3','9' ), 
 ('35','16','1','5','11' ), 
 ('36','6','1','2','10' ), 
 ('37','11','1','3','9' ), 
 ('38','11','1','3','9' ), 
 ('39','11','1','2','6' ), 
 ('40','1','1','5','1' ), 
 ('41','11','1','2','10' ), 
 ('42','12','1','2','9' ), 
 ('43','11','1','3','9' ); 
 INSERT INTO `order_items` (`o_id`, `p_id`, `quantity`, `size`, `colour` ) VALUES ('44','4','1','3','8' ), 
 ('45','16','1','2','11' ), 
 ('46','6','1','4','9' ), 
 ('47','8','1','4','9' ), 
 ('48','16','1','1','11' ), 
 ('49','15','1','2','10' ), 
 ('50','11','1','3','10' ), 
 ('51','9','1','3','11' ), 
 ('52','9','1','3','11' ), 
 ('53','15','1','2','9' ), 
 ('54','14','1','2','10' ), 
 ('55','6','1','3','10' ), 
 ('56','6','1','3','9' ), 
 ('57','3','1','5','5' ), 
 ('58','14','1','2','10' ), 
 ('59','3','1','5','5' ), 
 ('60','16','1','2','9' ), 
 ('61','16','1','2','9' ), 
 ('62','16','1','2','9' ), 
 ('63','16','1','1','6' ), 
 ('63','16','1','2','10' ), 
 ('63','16','1','2','11' ), 
 ('63','11','1','1','11' ), 
 ('63','11','1','2','6' ), 
 ('63','11','1','3','9' ), 
 ('63','4','1','1','7' ), 
 ('64','14','1','1','10' ), 
 ('65','16','1','3','11' ), 
 ('66','14','1','2','10' ), 
 ('67','16','1','2','6' ); 
 INSERT INTO `order_items` (`o_id`, `p_id`, `quantity`, `size`, `colour` ) VALUES ('68','16','1','2','6' ), 
 ('69','16','1','2','6' ), 
 ('70','6','1','2','9' ), 
 ('71','16','1','4','11' ), 
 ('72','16','1','4','11' ), 
 ('72','12','1','2','9' ), 
 ('72','4','1','3','8' ), 
 ('72','8','1','5','10' ), 
 ('72','9','1','4','6' ), 
 ('72','7','1','4','6' ), 
 ('73','6','1','2','9' ), 
 ('74','11','1','3','10' ), 
 ('75','11','1','2','10' ), 
 ('76','7','1','4','7' ), 
 ('77','6','1','1','9' ), 
 ('78','16','1','3','11' ), 
 ('79','11','1','4','11' ), 
 ('80','6','1','3','9' ), 
 ('81','11','1','1','11' ), 
 ('82','11','1','1','6' ), 
 ('83','16','1','4','6' ), 
 ('84','9','1','4','10' ), 
 ('85','16','1','3','10' ), 
 ('86','11','1','3','9' ), 
 ('87','6','1','1','10' ), 
 ('88','2','1','1','3' ), 
 ('89','16','1','2','10' ), 
 ('90','11','1','2','9' ), 
 ('91','8','1','4','10' ), 
 ('92','11','1','2','10' ); 
 INSERT INTO `order_items` (`o_id`, `p_id`, `quantity`, `size`, `colour` ) VALUES ('93','15','1','1','10' ), 
 ('94','6','1','5','9' ), 
 ('95','9','1','2','11' ), 
 ('96','11','1','4','10' ), 
 ('97','6','1','2','9' ), 
 ('98','19','1','3','9' ), 
 ('99','14','1','2','10' ), 
 ('100','11','1','2','11' ), 
 ('101','11','1','2','9' ), 
 ('102','3','1','3','5' ), 
 ('103','14','1','3','10' ), 
 ('104','2','1','4','3' ), 
 ('105','16','1','3','6' ), 
 ('106','11','1','2','9' ), 
 ('107','6','1','2','10' ), 
 ('108','16','1','2','11' ), 
 ('109','11','1','5','11' ), 
 ('110','9','1','3','11' ), 
 ('111','16','1','1','9' ), 
 ('112','13','1','2','9' ), 
 ('113','3','1','3','5' ), 
 ('114','19','1','3','9' ), 
 ('115','19','1','5','9' ), 
 ('116','6','1','5','9' ), 
 ('117','16','1','2','9' ), 
 ('118','16','1','3','11' ), 
 ('119','16','1','2','9' ), 
 ('120','9','1','5','10' ), 
 ('121','19','1','3','10' ), 
 ('122','6','3','3','10' ); 
 INSERT INTO `order_items` (`o_id`, `p_id`, `quantity`, `size`, `colour` ) VALUES ('122','6','2','4','10' ), 
 ('122','10','2','3','10' ), 
 ('122','10','2','4','9' ), 
 ('122','14','3','3','10' ), 
 ('122','12','3','3','9' ), 
 ('122','12','2','4','10' ), 
 ('122','16','2','3','9' ), 
 ('122','16','1','5','10' ), 
 ('123','11','1','3','10' ), 
 ('124','12','1','1','9' ), 
 ('125','5','1','4','1' ), 
 ('126','11','1','3','11' ), 
 ('127','6','1','2','9' ), 
 ('128','9','1','1','11' ), 
 ('129','16','1','1','11' ), 
 ('130','16','1','2','11' ), 
 ('131','16','1','2','11' ), 
 ('132','8','1','1','10' ), 
 ('133','6','1','4','10' ), 
 ('134','6','1','4','10' ), 
 ('135','14','1','3','10' ), 
 ('136','11','1','5','10' ), 
 ('137','11','1','5','10' ), 
 ('138','11','1','3','10' ), 
 ('139','12','1','2','10' ), 
 ('140','11','1','3','10' ), 
 ('141','11','1','4','10' ), 
 ('142','8','1','5','9' ), 
 ('143','16','1','3','11' ), 
 ('144','16','1','3','11' ); 
 INSERT INTO `order_items` (`o_id`, `p_id`, `quantity`, `size`, `colour` ) VALUES ('145','16','1','3','9' ), 
 ('146','6','1','1','9' ), 
 ('147','4','1','4','1' ), 
 ('148','8','1','3','10' ), 
 ('149','2','1','1','3' ), 
 ('150','8','1','2','9' ), 
 ('151','16','1','2','9' ), 
 ('152','11','1','1','11' ), 
 ('153','8','1','2','9' ), 
 ('154','16','1','4','11' ), 
 ('155','6','1','2','9' ), 
 ('156','6','1','2','9' ), 
 ('157','6','1','2','9' ), 
 ('158','11','1','2','11' ), 
 ('159','8','1','4','9' ), 
 ('160','10','1','2','9' ), 
 ('160','10','1','3','9' ), 
 ('161','10','1','2','9' ), 
 ('161','10','1','3','9' ), 
 ('162','15','1','5','9' ), 
 ('163','15','1','1','9' ), 
 ('164','19','1','2','9' ), 
 ('165','11','1','5','6' ), 
 ('166','16','1','2','11' ), 
 ('167','6','1','2','9' ), 
 ('168','11','1','1','10' ), 
 ('169','9','1','3','9' ), 
 ('170','15','1','2','10' ), 
 ('171','6','1','2','10' ), 
 ('172','6','1','2','10' ); 
 INSERT INTO `order_items` (`o_id`, `p_id`, `quantity`, `size`, `colour` ) VALUES ('173','8','1','2','9' ), 
 ('174','8','1','3','9' ), 
 ('175','11','1','1','11' ), 
 ('176','16','1','5','11' ), 
 ('177','9','1','1','11' ), 
 ('178','14','1','4','10' ), 
 ('179','6','1','1','10' ), 
 ('180','16','1','4','11' ), 
 ('181','16','1','4','11' ), 
 ('182','16','1','2','6' ), 
 ('183','16','1','1','11' ), 
 ('184','11','1','1','9' ), 
 ('185','5','1','2','1' ), 
 ('186','10','1','2','9' ), 
 ('187','6','1','1','10' ), 
 ('188','8','1','3','9' ), 
 ('188','14','1','2','9' ), 
 ('188','14','1','3','10' ), 
 ('188','14','1','4','9' ), 
 ('188','16','1','2','11' ), 
 ('188','16','1','3','6' ), 
 ('188','16','1','4','9' ), 
 ('188','19','1','2','10' ), 
 ('188','19','1','4','9' ), 
 ('188','19','1','3','9' ), 
 ('188','5','1','2','7' ), 
 ('188','5','1','3','1' ), 
 ('188','5','1','4','6' ), 
 ('189','11','1','2','9' ), 
 ('190','7','1','3','6' ); 
 INSERT INTO `order_items` (`o_id`, `p_id`, `quantity`, `size`, `colour` ) VALUES ('191','16','1','2','11' ), 
 ('192','11','1','5','11' ), 
 ('193','16','1','1','11' ), 
 ('194','16','1','1','11' ), 
 ('195','8','1','2','10' ), 
 ('196','12','1','1','10' ), 
 ('197','12','1','1','10' ), 
 ('198','16','1','1','6' ), 
 ('199','16','1','3','11' ), 
 ('200','19','1','5','10' ), 
 ('201','8','1','3','9' ), 
 ('202','19','1','5','9' ), 
 ('203','6','1','3','9' ), 
 ('204','9','1','2','11' ), 
 ('205','8','1','1','9' ), 
 ('206','14','1','2','10' ), 
 ('207','11','1','3','9' ), 
 ('208','6','1','2','9' ), 
 ('209','19','1','4','9' ), 
 ('210','6','1','3','10' ), 
 ('211','8','1','5','10' ), 
 ('212','6','1','5','9' ), 
 ('213','16','1','2','11' ), 
 ('214','11','1','3','11' ), 
 ('215','19','1','2','10' ), 
 ('216','9','1','3','11' ), 
 ('217','16','1','3','6' ), 
 ('218','4','1','2','7' ), 
 ('219','4','1','2','7' ), 
 ('220','4','1','2','7' ); 
 INSERT INTO `order_items` (`o_id`, `p_id`, `quantity`, `size`, `colour` ) VALUES ('221','6','1','2','10' ), 
 ('222','14','1','3','9' ), 
 ('223','6','1','3','10' ), 
 ('224','4','1','3','7' ), 
 ('225','14','1','2','9' ), 
 ('226','4','1','1','7' ), 
 ('227','9','1','4','10' ), 
 ('228','14','1','5','10' ), 
 ('229','16','1','2','6' ), 
 ('230','14','1','4','10' ), 
 ('231','19','1','2','9' ), 
 ('232','19','1','1','9' ), 
 ('233','14','1','1','9' ), 
 ('234','2','1','1','3' ), 
 ('235','12','1','2','10' ), 
 ('236','19','2','1','10' ), 
 ('237','16','1','2','11' ), 
 ('238','6','1','1','9' ), 
 ('239','6','1','2','9' ), 
 ('240','6','1','3','10' ), 
 ('241','6','1','2','10' ), 
 ('242','6','1','5','10' ), 
 ('243','11','1','3','11' ), 
 ('244','12','1','3','10' ), 
 ('245','8','1','2','10' ), 
 ('246','16','1','3','6' ), 
 ('247','16','1','3','11' ), 
 ('248','11','1','1','9' ), 
 ('249','16','1','4','11' ), 
 ('250','16','1','3','11' ); 
 INSERT INTO `order_items` (`o_id`, `p_id`, `quantity`, `size`, `colour` ) VALUES ('251','14','1','2','10' ), 
 ('252','11','1','2','10' ), 
 ('253','19','1','4','10' ), 
 ('254','16','1','3','6' ), 
 ('255','16','1','3','11' ), 
 ('256','9','1','2','11' ), 
 ('257','16','1','2','11' ), 
 ('258','11','1','4','9' ), 
 ('259','6','1','1','9' ), 
 ('260','9','1','3','9' ), 
 ('261','6','1','3','10' ), 
 ('262','6','1','3','10' ), 
 ('263','6','1','1','9' ), 
 ('264','11','1','3','9' ), 
 ('265','11','1','2','9' ), 
 ('266','12','1','3','10' ), 
 ('267','14','1','2','9' ), 
 ('268','6','1','4','10' ), 
 ('269','14','1','2','9' ), 
 ('270','11','1','2','11' ), 
 ('271','9','1','1','11' ), 
 ('272','9','1','4','11' ), 
 ('273','16','1','1','11' ), 
 ('274','4','1','1','7' ), 
 ('275','2','1','5','3' ), 
 ('276','16','1','2','6' ), 
 ('277','11','1','1','11' ), 
 ('278','8','1','2','9' ), 
 ('279','14','1','3','10' ), 
 ('280','14','1','3','10' ); 
 INSERT INTO `order_items` (`o_id`, `p_id`, `quantity`, `size`, `colour` ) VALUES ('281','16','1','2','10' ), 
 ('282','16','1','2','11' ), 
 ('283','16','1','2','11' ), 
 ('284','15','1','2','10' ), 
 ('285','11','1','2','11' ), 
 ('286','8','1','1','9' ), 
 ('287','19','1','3','10' ), 
 ('288','11','1','2','11' ), 
 ('289','6','1','1','9' ), 
 ('290','3','1','5','5' ), 
 ('291','16','1','3','6' ), 
 ('292','14','1','4','10' ), 
 ('293','15','1','2','9' ), 
 ('294','8','2','4','10' ), 
 ('294','19','2','4','10' ), 
 ('294','11','2','3','10' ), 
 ('294','9','2','4','10' ), 
 ('295','11','1','2','11' ), 
 ('295','16','1','2','11' ), 
 ('296','16','1','2','6' ), 
 ('297','9','1','1','9' ), 
 ('298','2','1','4','3' ), 
 ('299','11','1','3','10' ), 
 ('300','15','1','2','9' ), 
 ('301','11','1','3','10' ), 
 ('302','8','1','2','10' ), 
 ('303','11','1','3','10' ), 
 ('304','11','1','1','10' ), 
 ('305','15','1','2','9' ), 
 ('306','19','1','3','9' ); 
 INSERT INTO `order_items` (`o_id`, `p_id`, `quantity`, `size`, `colour` ) VALUES ('307','16','1','3','11' ), 
 ('308','8','1','2','9' ), 
 ('309','11','1','2','9' ), 
 ('310','11','1','1','10' ), 
 ('311','11','1','1','10' ), 
 ('312','6','1','3','9' ), 
 ('313','11','1','1','9' ), 
 ('314','16','1','4','6' ), 
 ('315','8','1','3','9' ), 
 ('316','8','1','4','9' ), 
 ('317','8','1','4','9' ), 
 ('318','11','1','1','11' ), 
 ('319','14','1','3','10' ), 
 ('320','16','1','4','6' ), 
 ('321','11','1','2','10' ), 
 ('322','16','1','3','11' ), 
 ('323','6','1','2','10' ), 
 ('324','6','1','3','10' ), 
 ('325','19','1','3','10' ), 
 ('326','19','1','3','10' ), 
 ('327','15','1','2','9' ), 
 ('328','14','1','3','10' ), 
 ('329','15','1','1','9' ), 
 ('330','8','1','3','10' ), 
 ('331','19','1','3','10' ), 
 ('332','11','1','3','10' ), 
 ('333','16','1','1','11' ), 
 ('334','16','1','2','11' ), 
 ('335','6','1','2','9' ), 
 ('336','6','1','3','10' ); 
 INSERT INTO `order_items` (`o_id`, `p_id`, `quantity`, `size`, `colour` ) VALUES ('337','15','1','2','9' ), 
 ('338','11','1','2','10' ), 
 ('339','14','1','2','9' ), 
 ('340','6','1','1','9' ), 
 ('341','16','1','2','10' ), 
 ('342','16','1','2','10' ), 
 ('343','16','1','2','10' ), 
 ('344','16','1','5','6' ), 
 ('345','9','1','3','11' ), 
 ('346','16','1','3','11' ), 
 ('347','6','1','3','9' ), 
 ('348','9','1','1','10' ), 
 ('349','11','1','1','11' ), 
 ('350','10','1','1','9' ), 
 ('351','10','1','1','9' ), 
 ('352','11','1','4','11' ), 
 ('353','10','1','1','9' ), 
 ('354','16','1','2','11' ), 
 ('355','11','1','2','11' ), 
 ('356','19','1','2','10' ), 
 ('357','19','1','2','10' ), 
 ('358','4','1','2','7' ), 
 ('359','11','1','2','10' ), 
 ('360','8','1','3','10' ), 
 ('361','16','1','4','11' ), 
 ('362','11','1','2','9' ), 
 ('363','8','1','2','9' ), 
 ('364','15','1','1','9' ), 
 ('365','11','1','4','10' ), 
 ('366','11','1','3','9' ); 
 INSERT INTO `order_items` (`o_id`, `p_id`, `quantity`, `size`, `colour` ) VALUES ('367','14','1','2','9' ), 
 ('368','9','1','1','11' ), 
 ('369','4','1','3','1' ), 
 ('370','16','1','5','11' ), 
 ('371','6','1','1','10' ), 
 ('372','16','1','3','11' ), 
 ('373','8','1','2','10' ), 
 ('374','6','1','4','9' ), 
 ('375','6','1','3','9' ), 
 ('376','6','1','1','9' ), 
 ('377','19','1','2','10' ), 
 ('378','19','1','3','10' ), 
 ('379','19','1','3','10' ), 
 ('380','9','1','2','10' ), 
 ('380','9','1','4','11' ), 
 ('380','9','1','4','9' ), 
 ('380','9','1','3','6' ), 
 ('380','14','1','3','10' ), 
 ('380','14','1','4','10' ), 
 ('380','19','1','1','10' ), 
 ('380','19','1','4','10' ), 
 ('380','19','1','5','9' ), 
 ('380','19','1','3','9' ), 
 ('380','19','1','2','10' ), 
 ('380','12','1','3','10' ), 
 ('380','12','1','2','10' ), 
 ('380','8','1','2','10' ), 
 ('380','8','1','4','10' ), 
 ('380','13','1','3','10' ), 
 ('380','13','1','4','10' ); 
 INSERT INTO `order_items` (`o_id`, `p_id`, `quantity`, `size`, `colour` ) VALUES ('381','9','1','3','6' ), 
 ('382','11','1','3','9' ), 
 ('383','9','1','3','11' ), 
 ('384','6','1','2','9' ), 
 ('385','8','2','2','10' ), 
 ('385','8','2','4','9' ), 
 ('385','8','2','3','10' ), 
 ('385','6','3','3','10' ), 
 ('385','6','2','4','10' ), 
 ('385','16','3','3','11' ), 
 ('385','16','1','3','6' ), 
 ('385','16','1','2','6' ), 
 ('385','14','2','3','10' ), 
 ('385','19','1','2','9' ), 
 ('385','19','2','3','10' ), 
 ('385','12','2','3','10' ), 
 ('385','12','1','4','10' ), 
 ('385','15','1','2','9' ), 
 ('385','15','1','3','10' ), 
 ('385','15','2','4','10' ), 
 ('386','8','1','2','10' ), 
 ('387','6','1','3','9' ), 
 ('388','6','1','2','9' ), 
 ('389','6','1','2','9' ), 
 ('390','15','1','4','9' ), 
 ('391','16','1','2','11' ), 
 ('392','16','1','2','11' ), 
 ('393','12','1','2','10' ), 
 ('394','14','1','2','10' ), 
 ('395','6','1','2','9' ); 
 INSERT INTO `order_items` (`o_id`, `p_id`, `quantity`, `size`, `colour` ) VALUES ('396','9','1','2','11' ), 
 ('397','11','1','4','9' ), 
 ('398','8','1','3','10' ), 
 ('399','8','1','3','10' ), 
 ('400','19','1','5','9' ), 
 ('401','6','1','3','10' ), 
 ('402','8','1','2','10' ), 
 ('403','6','1','3','10' ), 
 ('404','8','1','1','9' ), 
 ('405','8','1','1','9' ), 
 ('406','6','1','1','10' ), 
 ('407','3','1','2','5' ), 
 ('408','11','1','1','11' ), 
 ('409','14','1','3','10' ), 
 ('410','11','1','1','9' ), 
 ('411','19','1','5','10' ), 
 ('412','14','1','2','10' ), 
 ('413','11','1','3','11' ), 
 ('414','6','1','1','10' ), 
 ('415','6','1','1','10' ), 
 ('416','6','1','1','10' ), 
 ('417','6','1','1','10' ), 
 ('418','9','1','3','11' ), 
 ('419','11','1','1','6' ), 
 ('420','3','1','5','5' ), 
 ('421','11','1','2','10' ), 
 ('422','8','1','2','10' ), 
 ('423','28','1','1','9' ), 
 ('424','11','1','3','9' ), 
 ('425','28','1','2','11' ); 
 INSERT INTO `order_items` (`o_id`, `p_id`, `quantity`, `size`, `colour` ) VALUES ('426','28','1','2','10' ), 
 ('427','11','1','2','9' ), 
 ('428','11','1','1','10' ), 
 ('429','29','1','3','9' ), 
 ('430','6','1','2','9' ), 
 ('431','16','1','4','11' ), 
 ('432','9','2','3','11' ), 
 ('432','29','1','3','10' ), 
 ('433','15','1','1','10' ), 
 ('434','19','1','3','10' ), 
 ('435','11','1','3','9' ), 
 ('436','11','1','3','9' ), 
 ('437','6','1','3','10' ), 
 ('438','28','1','3','11' ), 
 ('439','8','1','3','9' ), 
 ('440','8','1','2','10' ), 
 ('441','6','1','3','10' ), 
 ('442','16','1','3','11' ), 
 ('443','28','1','2','11' ), 
 ('444','28','1','4','9' ), 
 ('445','16','1','2','9' ), 
 ('446','16','1','4','11' ), 
 ('447','6','1','1','10' ), 
 ('448','8','1','2','9' ), 
 ('449','8','1','2','9' ), 
 ('450','15','1','2','10' ), 
 ('451','11','1','4','11' ), 
 ('452','28','1','2','11' ), 
 ('453','11','1','2','10' ), 
 ('454','8','1','3','10' ); 
 INSERT INTO `order_items` (`o_id`, `p_id`, `quantity`, `size`, `colour` ) VALUES ('455','19','1','5','9' ), 
 ('456','3','1','3','5' ), 
 ('457','28','1','4','10' ), 
 ('458','28','1','2','11' ), 
 ('459','28','1','3','11' ), 
 ('459','4','1','3','7' ), 
 ('459','19','1','3','10' ), 
 ('459','11','1','3','11' ), 
 ('460','6','1','2','10' ), 
 ('461','6','1','2','10' ), 
 ('462','8','1','3','9' ), 
 ('463','28','1','3','9' ), 
 ('464','28','1','3','11' ), 
 ('465','11','1','5','9' ), 
 ('466','11','1','5','9' ), 
 ('467','11','1','3','9' ), 
 ('468','11','1','3','10' ), 
 ('469','28','1','1','9' ), 
 ('470','42','1','3','10' ), 
 ('471','8','1','4','10' ), 
 ('472','42','1','2','11' ), 
 ('473','42','1','2','9' ), 
 ('474','42','1','3','9' ), 
 ('475','42','1','2','11' ), 
 ('476','14','1','3','10' ), 
 ('477','42','1','3','11' ), 
 ('478','42','1','3','11' ), 
 ('479','28','1','2','11' ), 
 ('480','28','1','3','11' ), 
 ('481','11','1','2','11' ); 
 INSERT INTO `order_items` (`o_id`, `p_id`, `quantity`, `size`, `colour` ) VALUES ('482','19','1','1','10' ), 
 ('483','42','1','1','10' ), 
 ('484','42','1','1','10' ), 
 ('485','42','1','1','10' ), 
 ('486','6','1','4','9' ), 
 ('487','16','1','3','6' ), 
 ('487','16','1','3','11' ), 
 ('487','8','1','4','10' ), 
 ('487','6','1','2','9' ), 
 ('487','11','1','3','10' ), 
 ('487','11','1','3','9' ), 
 ('487','29','1','3','10' ), 
 ('487','3','1','3','5' ), 
 ('487','7','1','3','6' ), 
 ('487','19','1','4','9' ), 
 ('488','11','1','2','9' ), 
 ('489','48','1','3','9' ), 
 ('489','14','1','3','10' ), 
 ('490','28','1','4','11' ), 
 ('491','4','1','4','7' ), 
 ('492','8','1','3','9' ), 
 ('493','11','1','2','10' ), 
 ('494','7','1','3','7' ), 
 ('494','10','1','3','10' ), 
 ('495','5','1','3','6' ), 
 ('495','10','1','3','10' ), 
 ('496','42','1','2','11' ), 
 ('496','42','1','3','11' ), 
 ('496','42','1','4','11' ), 
 ('496','16','2','3','10' ); 
 INSERT INTO `order_items` (`o_id`, `p_id`, `quantity`, `size`, `colour` ) VALUES ('496','16','1','3','9' ), 
 ('496','16','3','3','11' ), 
 ('496','11','2','3','10' ), 
 ('496','11','1','3','11' ), 
 ('496','28','2','3','10' ), 
 ('496','8','2','3','10' ), 
 ('496','37','4','63','10' ), 
 ('496','32','2','63','1' ), 
 ('496','32','1','63','12' ), 
 ('497','42','1','2','11' ), 
 ('497','42','1','3','11' ), 
 ('497','42','1','4','11' ), 
 ('497','16','2','3','10' ), 
 ('497','16','1','3','9' ), 
 ('497','16','3','3','11' ), 
 ('497','11','2','3','10' ), 
 ('497','11','1','3','11' ), 
 ('497','28','2','3','10' ), 
 ('497','8','2','3','10' ), 
 ('497','37','4','63','10' ), 
 ('497','32','2','63','1' ), 
 ('497','32','1','63','12' ), 
 ('498','42','1','2','11' ), 
 ('498','42','1','3','11' ), 
 ('498','42','1','4','11' ), 
 ('498','16','2','3','10' ), 
 ('498','16','1','3','9' ), 
 ('498','16','3','3','11' ), 
 ('498','11','2','3','10' ), 
 ('498','11','1','3','11' ); 
 INSERT INTO `order_items` (`o_id`, `p_id`, `quantity`, `size`, `colour` ) VALUES ('498','28','2','3','10' ), 
 ('498','8','2','3','10' ), 
 ('498','37','4','63','10' ), 
 ('498','32','2','63','1' ), 
 ('498','32','1','63','12' ), 
 ('499','10','1','1','10' ), 
 ('500','10','1','5','10' ), 
 ('501','16','4','3','11' ), 
 ('501','16','2','3','10' ), 
 ('501','16','1','3','9' ), 
 ('501','28','2','3','10' ), 
 ('501','6','2','3','10' ), 
 ('501','42','1','2','11' ), 
 ('501','42','2','3','10' ), 
 ('501','42','1','4','10' ), 
 ('501','42','2','3','11' ), 
 ('501','8','2','3','10' ), 
 ('501','8','1','4','10' ), 
 ('501','32','2','63','1' ), 
 ('501','32','1','63','12' ), 
 ('501','32','1','63','10' ), 
 ('501','37','2','63','9' ), 
 ('501','37','1','63','10' ), 
 ('501','36','3','63','10' ), 
 ('502','14','1','2','10' ), 
 ('503','1','1','1','2' ), 
 ('504','14','1','3','10' ), 
 ('504','19','1','3','10' ), 
 ('504','11','1','3','10' ), 
 ('504','49','1','3','6' ); 
 INSERT INTO `order_items` (`o_id`, `p_id`, `quantity`, `size`, `colour` ) VALUES ('505','11','1','2','9' ), 
 ('506','8','1','4','10' ), 
 ('506','8','1','3','10' ), 
 ('506','8','1','3','9' ), 
 ('506','28','1','3','10' ), 
 ('506','28','1','4','11' ), 
 ('506','6','1','3','10' ), 
 ('506','16','1','3','11' ), 
 ('507','11','1','1','9' ), 
 ('508','11','1','1','9' ), 
 ('509','6','1','1','9' ), 
 ('510','16','1','3','6' ), 
 ('511','42','1','3','10' ), 
 ('511','15','1','3','10' ), 
 ('511','29','1','3','10' ), 
 ('512','16','1','2','11' ), 
 ('513','11','1','3','11' ), 
 ('514','29','1','3','10' ), 
 ('515','6','1','3','10' ), 
 ('516','11','1','2','10' ), 
 ('517','10','1','2','10' ), 
 ('518','12','1','1','9' ), 
 ('519','11','1','3','11' ), 
 ('520','49','1','1','11' ), 
 ('521','28','1','3','10' ), 
 ('522','29','1','2','10' ), 
 ('523','19','1','4','9' ), 
 ('524','19','1','4','9' ), 
 ('525','12','1','2','10' ), 
 ('526','29','1','1','10' ); 
 INSERT INTO `order_items` (`o_id`, `p_id`, `quantity`, `size`, `colour` ) VALUES ('527','6','1','3','10' ), 
 ('528','10','1','1','9' ), 
 ('529','6','1','3','10' ), 
 ('530','16','1','3','11' ), 
 ('531','29','1','3','10' ), 
 ('532','90','1','2','11' ), 
 ('533','90','1','3','10' ), 
 ('534','90','1','3','10' ), 
 ('535','99','1','3','9' ), 
 ('536','99','1','3','10' ), 
 ('536','11','1','3','10' ), 
 ('536','90','1','3','11' ), 
 ('536','8','1','3','10' ), 
 ('537','8','1','2','10' ), 
 ('538','90','1','3','11' ), 
 ('539','6','1','2','9' ), 
 ('540','11','1','3','11' ), 
 ('541','49','1','2','11' ), 
 ('541','78','1','8','10' ), 
 ('542','11','1','5','11' ), 
 ('543','6','1','3','10' ), 
 ('544','29','1','1','9' ), 
 ('545','6','1','2','10' ), 
 ('546','14','1','1','9' ), 
 ('547','14','1','3','9' ), 
 ('548','8','1','2','9' ), 
 ('549','8','2','3','10' ), 
 ('549','16','1','4','10' ), 
 ('549','6','1','3','10' ), 
 ('549','90','2','3','11' ); 
 INSERT INTO `order_items` (`o_id`, `p_id`, `quantity`, `size`, `colour` ) VALUES ('549','56','1','11','10' ), 
 ('549','77','1','10','10' ), 
 ('549','62','1','9','10' ), 
 ('549','58','1','8','10' ), 
 ('549','59','1','7','10' ), 
 ('550','8','2','3','10' ), 
 ('550','16','1','4','10' ), 
 ('550','6','1','3','10' ), 
 ('550','90','2','3','11' ), 
 ('550','56','1','11','10' ), 
 ('550','77','1','10','10' ), 
 ('550','62','1','9','10' ), 
 ('550','58','1','8','10' ), 
 ('550','59','1','7','10' ), 
 ('551','90','1','2','11' ), 
 ('552','6','1','1','10' ), 
 ('553','90','1','4','10' ), 
 ('554','28','1','3','9' ), 
 ('555','28','1','3','9' ), 
 ('556','11','1','2','10' ), 
 ('557','11','1','2','10' ), 
 ('558','90','1','4','11' ), 
 ('559','11','1','3','9' ), 
 ('560','14','1','2','9' ), 
 ('561','11','1','2','11' ), 
 ('562','6','1','2','10' ), 
 ('563','6','1','3','10' ), 
 ('564','28','2','3','11' ), 
 ('565','9','1','2','11' ), 
 ('566','8','1','2','9' ); 
 INSERT INTO `order_items` (`o_id`, `p_id`, `quantity`, `size`, `colour` ) VALUES ('567','9','1','3','11' ), 
 ('568','14','1','3','10' ), 
 ('569','11','1','3','10' ), 
 ('570','6','1','1','9' ), 
 ('571','11','1','2','11' ), 
 ('572','11','1','3','9' ), 
 ('573','9','1','2','11' ), 
 ('574','11','1','3','9' ), 
 ('575','11','1','3','9' ), 
 ('576','14','1','3','9' ), 
 ('576','58','1','9','10' ), 
 ('577','83','1','8','10' ), 
 ('578','83','1','8','10' ), 
 ('579','6','1','3','10' ), 
 ('580','66','1','9','10' ), 
 ('580','90','2','3','11' ), 
 ('580','8','1','3','10' ), 
 ('580','16','1','3','11' ), 
 ('580','28','1','3','9' ), 
 ('581','16','1','1','11' ), 
 ('582','16','1','1','11' ), 
 ('583','6','1','2','9' ), 
 ('584','14','1','1','9' ), 
 ('585','28','1','1','10' ), 
 ('586','28','1','1','10' ), 
 ('587','6','1','3','9' ), 
 ('588','123','1','9','10' ), 
 ('588','124','1','3','10' ), 
 ('588','58','1','9','10' ), 
 ('589','14','1','1','10' ); 
 INSERT INTO `order_items` (`o_id`, `p_id`, `quantity`, `size`, `colour` ) VALUES ('590','28','1','2','11' ), 
 ('591','90','1','2','10' ), 
 ('592','129','1','5','10' ), 
 ('593','90','1','1','11' ), 
 ('594','28','1','1','10' ), 
 ('595','129','1','2','10' ), 
 ('596','6','1','2','9' ), 
 ('597','10','1','3','10' ), 
 ('598','99','1','2','9' ), 
 ('598','129','1','2','9' ), 
 ('599','16','1','1','6' ), 
 ('600','129','1','3','10' ), 
 ('601','130','1','3','9' ), 
 ('602','130','1','3','9' ), 
 ('603','90','1','1','11' ), 
 ('604','129','1','3','10' ), 
 ('605','8','1','2','9' ), 
 ('606','6','1','4','10' ), 
 ('607','6','1','4','10' ), 
 ('608','129','1','3','9' ), 
 ('609','124','1','2','10' ), 
 ('610','28','1','1','11' ), 
 ('611','11','1','5','11' ), 
 ('612','8','1','1','9' ), 
 ('613','28','1','2','11' ), 
 ('614','66','1','9','10' ), 
 ('615','11','1','2','6' ), 
 ('616','11','1','2','9' ), 
 ('617','11','1','2','9' ), 
 ('618','127','1','3','10' ); 
 INSERT INTO `order_items` (`o_id`, `p_id`, `quantity`, `size`, `colour` ) VALUES ('619','129','1','1','9' ), 
 ('620','28','1','2','9' ), 
 ('621','28','1','1','11' ), 
 ('622','130','1','4','9' ), 
 ('623','11','1','3','10' ), 
 ('623','11','1','4','10' ), 
 ('623','11','1','3','11' ), 
 ('623','11','1','4','11' ), 
 ('623','8','1','3','10' ), 
 ('623','8','1','4','10' ), 
 ('623','9','1','3','6' ), 
 ('623','9','1','3','11' ), 
 ('623','14','1','3','10' ), 
 ('624','9','1','3','11' ), 
 ('625','56','1','7','10' ), 
 ('626','16','1','1','9' ), 
 ('627','8','1','2','9' ), 
 ('628','28','1','5','10' ), 
 ('629','130','1','3','9' ), 
 ('630','129','1','1','10' ), 
 ('631','99','1','3','10' ), 
 ('632','124','1','2','10' ), 
 ('633','129','1','1','10' ), 
 ('634','129','1','3','9' ), 
 ('635','99','1','2','10' ), 
 ('636','28','1','1','11' ), 
 ('637','6','1','2','9' ), 
 ('638','129','1','3','9' ), 
 ('639','129','1','3','9' ), 
 ('640','129','1','3','9' ); 
 INSERT INTO `order_items` (`o_id`, `p_id`, `quantity`, `size`, `colour` ) VALUES ('641','6','1','2','10' ), 
 ('642','130','1','1','9' ), 
 ('643','90','1','3','11' ), 
 ('644','61','1','11','10' ), 
 ('644','14','1','4','10' ), 
 ('645','16','1','1','11' ), 
 ('646','9','1','2','11' ), 
 ('646','9','1','3','11' ), 
 ('646','9','1','4','11' ), 
 ('646','9','1','5','11' ), 
 ('646','11','1','2','6' ), 
 ('646','11','1','3','6' ), 
 ('646','11','1','4','6' ), 
 ('646','11','1','5','6' ), 
 ('647','9','1','2','11' ), 
 ('647','9','1','3','11' ), 
 ('647','9','1','4','11' ), 
 ('647','9','1','5','11' ), 
 ('647','11','1','2','6' ), 
 ('647','11','1','3','6' ), 
 ('647','11','1','4','6' ), 
 ('647','11','1','5','6' ), 
 ('648','11','1','5','11' ), 
 ('649','11','1','5','11' ), 
 ('650','129','1','4','10' ), 
 ('651','129','1','2','9' ), 
 ('652','11','1','5','9' ), 
 ('653','15','1','4','10' ), 
 ('654','123','1','10','10' ), 
 ('654','122','1','4','10' ); 
 INSERT INTO `order_items` (`o_id`, `p_id`, `quantity`, `size`, `colour` ) VALUES ('655','11','1','3','9' ), 
 ('656','90','1','1','10' ), 
 ('657','130','1','3','9' ), 
 ('658','1','1','1','1' ), 
 ('659','1','1','1','1' ), 
 ('660','8','1','3','9' ), 
 ('661','11','1','5','10' ), 
 ('662','11','1','5','10' ), 
 ('663','126','1','9','10' ), 
 ('663','127','1','3','10' ), 
 ('664','99','1','2','10' ), 
 ('665','129','1','3','9' ), 
 ('666','99','1','3','9' ), 
 ('667','16','1','2','11' ), 
 ('668','130','1','2','9' ), 
 ('669','127','1','2','10' ), 
 ('670','99','1','1','10' ), 
 ('671','11','1','4','11' ), 
 ('672','90','1','4','11' ), 
 ('673','90','1','4','11' ), 
 ('674','90','1','4','11' ), 
 ('675','90','1','4','11' ), 
 ('676','11','1','5','10' ), 
 ('677','11','1','1','11' ), 
 ('678','130','1','3','10' ), 
 ('679','11','1','2','9' ), 
 ('680','11','1','2','10' ), 
 ('681','13','1','3','9' ), 
 ('682','9','1','1','11' ), 
 ('683','11','1','2','10' ); 
 INSERT INTO `order_items` (`o_id`, `p_id`, `quantity`, `size`, `colour` ) VALUES ('684','8','1','4','9' ), 
 ('685','129','1','1','10' ), 
 ('686','6','1','3','9' ), 
 ('687','29','1','1','10' ), 
 ('688','15','1','1','9' ), 
 ('689','29','1','1','10' ), 
 ('690','15','1','1','9' ), 
 ('691','9','1','1','11' ), 
 ('692','136','4','3','10' ), 
 ('693','6','1','5','9' ), 
 ('694','29','1','3','9' ), 
 ('695','14','1','2','10' ), 
 ('696','11','1','5','11' ), 
 ('697','73','1','10','10' ), 
 ('697','56','1','9','10' ), 
 ('697','61','1','10','10' ), 
 ('697','80','1','9','10' ), 
 ('697','116','1','9','10' ), 
 ('698','15','1','1','9' ), 
 ('699','90','1','3','10' ), 
 ('700','6','1','5','9' ), 
 ('701','130','1','1','10' ), 
 ('702','130','1','1','10' ), 
 ('703','28','1','1','9' ), 
 ('704','11','1','4','11' ), 
 ('705','15','1','4','10' ), 
 ('706','15','1','4','10' ), 
 ('707','99','1','2','10' ), 
 ('708','11','1','3','11' ), 
 ('709','11','1','4','9' ); 
 INSERT INTO `order_items` (`o_id`, `p_id`, `quantity`, `size`, `colour` ) VALUES ('710','99','1','3','10' ), 
 ('711','99','1','3','10' ), 
 ('712','90','1','2','11' ), 
 ('713','90','1','3','11' ), 
 ('714','99','1','3','10' ), 
 ('715','9','1','1','11' ), 
 ('716','142','1','2','11' ), 
 ('717','129','1','2','9' ), 
 ('718','129','2','3','9' ), 
 ('718','129','2','3','10' ), 
 ('718','8','1','2','10' ), 
 ('718','8','1','3','10' ), 
 ('718','136','2','3','10' ), 
 ('718','6','1','3','9' ), 
 ('719','149','1','4','10' ), 
 ('720','150','1','3','9' ), 
 ('721','129','1','3','9' ), 
 ('722','141','1','5','10' ), 
 ('722','58','1','11','10' ), 
 ('723','58','1','11','10' ), 
 ('723','8','1','4','10' ), 
 ('724','99','1','2','9' ), 
 ('725','142','1','1','11' ), 
 ('726','142','1','1','11' ), 
 ('727','69','1','8','10' ), 
 ('728','150','1','3','9' ), 
 ('729','90','1','2','11' ), 
 ('730','90','1','2','11' ), 
 ('731','129','1','2','10' ), 
 ('732','129','1','4','9' ); 
 INSERT INTO `order_items` (`o_id`, `p_id`, `quantity`, `size`, `colour` ) VALUES ('733','129','1','4','9' ); 
-- Struktur Tabel orders -------------------- 
CREATE TABLE IF NOT EXISTS `orders` ( 
 `id` int(11) NOT NULL auto_increment, 
`total` decimal(10,2) NOT NULL , 
`deliveryttl` decimal(10,2) NOT NULL , 
`voucher` varchar(50) NOT NULL , 
`status` set('N','P','PP','C','D') NOT NULL , 
`notes` text NOT NULL , 
`date` int(11) NOT NULL , 
`delivery` text NOT NULL , 
`email` varchar(200) NOT NULL , 
`phone` varchar(30) NOT NULL  , 
PRIMARY KEY (`id`), 
KEY `status` (`status`), 
KEY `date` (`date`) 
 ); 
-- Data Tabel orders -------------------- 
INSERT INTO `orders` (`id`, `total`, `deliveryttl`, `voucher`, `status`, `notes`, `date`, `delivery`, `email`, `phone` ) VALUES ('1','26.88','0.00','','C','','1255956527','Steven Sullivan
104 Station Parade
Harrogate
North Yorkshire
HG1 1HX
GB','steve.s@loyaltymatters.co.uk','0845 838 2240' ), 
 ('2','26.88','0.00','','C','','1255956917','Steven Sullivan
104 Station Parade
Harrogate
North Yorkshire
HG1 1HX
GB','steve.s@loyaltymatters.co.uk','0845 838 2240' ), 
 ('3','160.88','0.00','','D','','1255956976','Steven Sullivan
104 Station Parade
Harrogate
North Yorkshire
HG1 1HX
GB','steve.s@loyaltymatters.co.uk','0845 838 2240' ), 
 ('4','50.50','2.50','','C','','1256028543','Steven Sullivan
104 Station Parade
Harrogate
North Yorkshire
HG1 1HX
GB','steve.s@loyaltymatters.co.uk','0845 838 2240' ), 
 ('5','48.00','5.00','','C','','1256028896','Steven Sullivan
104 Station Parade
Harrogate
North Yorkshire
HG1 1HX
GB','steve.s@loyaltymatters.co.uk','0845 838 2240' ), 
 ('6','73.00','7.50','','N','','1256029488','Steven Sullivan
104 Station Parade
Harrogate
North Yorkshire
HG1 1HX
GB','steve.s@loyaltymatters.co.uk','0845 838 2240' ), 
 ('7','75.39','7.50','PERCENTOFF','N','','1256029703','Steven Sullivan
104 Station Parade
Harrogate
North Yorkshire
HG1 1HX
GB','steve.s@loyaltymatters.co.uk','0845 838 2240' ), 
 ('8','60.39','7.50','PERCENTOFF','N','','1256029932','Steven Sullivan
104 Station Parade
Harrogate
North Yorkshire
HG1 1HX
GB','steve.s@loyaltymatters.co.uk','0845 838 2240' ), 
 ('9','75.39','7.50','PERCENTOFF','N','','1256029960','Steven Sullivan
104 Station Parade
Harrogate
North Yorkshire
HG1 1HX
GB','steve.s@loyaltymatters.co.uk','0845 838 2240' ), 
 ('10','82.07','10.00','PERCENTOFF','N','','1256030392','Steven Sullivan
104 Station Parade
Harrogate
North Yorkshire
HG1 1HX
GB','steve.s@loyaltymatters.co.uk','0845 838 2240' ), 
 ('11','82.07','10.00','PERCENTOFF','N','','1256030426','Steven Sullivan
104 Station Parade
Harrogate
North Yorkshire
HG1 1HX
GB','steve.s@loyaltymatters.co.uk','0845 838 2240' ), 
 ('12','102.07','10.00','PERCENTOFF','N','','1256030481','Steven Sullivan
104 Station Parade
Harrogate
North Yorkshire
HG1 1HX
GB','steve.s@loyaltymatters.co.uk','0845 838 2240' ), 
 ('13','92.07','10.00','PERCENTOFF','N','','1256030527','Steven Sullivan
104 Station Parade
Harrogate
North Yorkshire
HG1 1HX
GB','steve.s@loyaltymatters.co.uk','0845 838 2240' ), 
 ('14','92.07','10.00','PERCENTOFF','D','','1256030733','Steven Sullivan
104 Station Parade
Harrogate
North Yorkshire
HG1 1HX
GB','steve.s@loyaltymatters.co.uk','0845 838 2240' ), 
 ('15','26.00','2.50','','N','','1256055828','Carolyn Saddington
104 Station Parade
Harrogate
North Yorkshire
HG1 1HQ
GB','carolyn@loyaltymatters.co.uk','01423857900' ), 
 ('16','24.00','2.50','','D','','1256125450','Laura George
50 Harlow Crescent
Harrogate
North Yorkshire
HG2 0AJ
GB','laurageorge1@hotmail.co.uk','07810505248' ), 
 ('17','24.00','2.50','','D','','1257164535','Laura George
50 Harlow Crescent
Harrogate
North Yorkshire
HG2 0AJ
GB','laurageorge1@hotmail.co.uk','07810505248' ), 
 ('18','24.00','2.50','','D','','1257164593','Laura George
50 Harlow Crescent
Harrogate
North Yorkshire
HG2 0AJ
GB','laurasingleton1@gmail.com','07810505248' ), 
 ('19','24.00','2.50','','D','','1257166962','Laura George
50 Harlow Crescent
Harrogate
North Yorkshire
HG2 0AJ
GB','laurasingleton1@gmail.com','07810505248' ), 
 ('20','18.00','2.50','FACEBOOK','D','','1260882934','Laura George
50 harlow crescent
Harrogate
North Yorkshire
HG2 0AJ
GB','laurageorge1@hotmail.co.uk','07810505248' ), 
 ('21','19.20','2.50','IDEAS','D','','1262170075','Louise  Taylor
23 Haigh Crescent
Chorley
Chorley
Lancashire
PR7 2QS
GB','louise.taylor@tenongroup.com','07794933308' ), 
 ('22','20.80','2.50','IDEAS','D','','1264347479','Helen Roocroft
85 Silver birch Close,
Whitchurch
Cardiff
Cardiff
CF14 1EP
GB','Helenroocroft@hotmail.com','07779243045' ), 
 ('23','26.00','2.50','','D','','1265146614','Helen Leathers
160 Westbury Road 
Westbury-on-trym
Bristol
Avon
BS9 3AH
GB','helen.leathers@baesystems.com','0117 962 2621' ), 
 ('24','28.00','2.50','','N','','1267366643','tracy ritchie
28 ainslie road 
kildrum
cumbernauld
glasgow
g67 2eb
GB','w-ritchie@sky.com','01236720731' ), 
 ('25','19.20','2.50','IDEAS','D','','1267720864','Natalie Searle
16 Kidsbury Road
Bridgwater
Somerset
TA6 7AG
GB','nataliesearle18@hotmail.com','01278446714' ), 
 ('26','20.80','2.50','IDEAS','D','','1267830702','Alexandra Collins
139 North View Road
Crouch End
London
N8 7LR
GB','muppett00@hotmail.com','07788634228' ), 
 ('27','19.20','2.50','IDEAS','D','','1268037970','Lucy Atwill
39 Seymour Park
Mannamead
Plymouth
Devon
PL3 5BQ
GB','lucy_atwill@hotmail.co.uk','01752 518527' ), 
 ('28','20.80','2.50','IDEAS','D','','1268063713','Fiona Underwood
24 Mulberry Rise
Northwich
Cheshire
CW8 4UQ
GB','princess_fiona15@hotmail.com','07590646406' ), 
 ('29','17.60','2.50','IDEAS','D','','1268595944','Salliann Coleman
23 Oak Way
Ashtead
Surrey
KT21 1LQ
GB','sallianncoleman@yahoo.co.uk','07714344734' ), 
 ('30','24.00','2.50','','D','','1268651722','Jodi Mitzman
24 Kimberley Road
Kimberley Court
London
London
NW6 7SL
GB','jodi@tcbgroup.co.uk','07958321458' ); 
 INSERT INTO `orders` (`id`, `total`, `deliveryttl`, `voucher`, `status`, `notes`, `date`, `delivery`, `email`, `phone` ) VALUES ('31','17.60','2.50','IDEAS','D','','1268847053','Clare Andrews
2 Olde Mill Lodge
Town Street
Upwell
Cambridgeshire
PE14 9AF
GB','piano_mad@hotmail.co.uk','07823883273' ), 
 ('32','20.80','2.50','IDEAS','N','','1268928220','ANgharad Blythin
Awel Y Coed,
27 Lon y Berllan
Abergele
Conwy
LL22 7JF
GB','angharad@glanmorfa.conwy.sch.uk','01745 832051' ), 
 ('33','20.80','2.50','IDEAS','D','','1268928260','Angharad Blythin
Awel Y Coed,
27 Lon y Berllan
Abergele
Conwy
LL22 7JF
GB','angharad@glanmorfa.conwy.sch.uk','01745 832051' ), 
 ('34','24.00','2.50','','D','','1269082345','Amber Mcmillan 
551 free lane
Helmshore 
Rossendale 
Bb4 4lt
GB','Nailteq@yahoo.co.uk','01706227166' ), 
 ('35','26.00','2.50','','D','','1269250635','MRS TRACEY MUAT
EXPRESS EMBROIDERY LTD
UNIT 5 BATH STREET INDUSTRIAL ESTATE
NEWCASTLE UPON TYNE
TYNE & WEAR
NE6 3PH
GB','expressembroider@btconnect.com','07960512485' ), 
 ('36','24.00','2.50','','D','','1269272345','Myriam Osterley
Flat 4 Colts Yard
10 Aylmer Road
Leytonstone
London
E11 3AD
GB','myriam_osterley@yahoo.co.uk','07784148927' ), 
 ('37','17.60','2.50','IDEAS','N','','1269341550','Alexandra Sanjurgo
71 Silverbrook Road
Liverpool
Merseyside
L27 1XH
GB','alexsanjurgo@yahoo.co.uk','4407759456033' ), 
 ('38','17.60','2.50','IDEAS','D','','1269341845','Alexandra Sanjurgo
71 Silverbrook Road
Liverpool
Merseyside
L27 1XH
GB','alexsanjurgo@yahoo.co.uk','4407759456033' ), 
 ('39','22.00','2.50','','D','','1269341957','EMMA ROADNIGHT
3 GILLIATS GREEN
CHORLEYWOOD
HERTS
WD3 5LN
GB','emmaldriveruk@yahoo.co.uk','07771 714987' ), 
 ('40','24.00','2.50','','D','','1269451171','julie hague
200 turf lane
royton
lancashire
ol26eu
GB','juliehague@live.co.uk','07595351617' ), 
 ('41','22.00','2.50','','D','','1269533342','Sarah  McMillan 
ERS Ltd, Westerhill Road, Bishopbriggs
Glasgow
East Dunbartonshire 
G64 2QH
GB','sarah@ersremediation.com','07921387954' ), 
 ('42','19.20','2.50','IDEAS','D','','1269628881','Lisa Dobney
1 Elgarth Drive
Finchampstead
Wokingham
Berkshire
RG40 4HH
GB','fame_85@hotmail.com','07709471616' ), 
 ('43','22.00','2.50','','D','','1269632451','rachel mcbay
19 blackdales avenue
largs
ayrshire
ka30 8hu
GB','rachierachel@googlemail.com','07875058913' ), 
 ('44','20.80','2.50','IDEAS','D','','1269632575','Emma Gooch
34 King Street
Market Rasen

Lincolnshire
LN8 3BB
GB','chocolatebananas_5@hotmail.com','07535671471' ), 
 ('45','26.00','2.50','','D','','1269639903','Hannah Taylor
17 Serpentine road
kendal
cumbria
LA9 4PF
GB','stuandmonie@hotmail.com','07792966578' ), 
 ('46','24.00','2.50','','D','','1269708077','Leanne McGivern
Flat 5 Spring Grove
Charters Road
Ascot
Berkshire
SL5 9QB
GB','leighmcg@hotmail.com','01344 626646' ), 
 ('47','22.00','2.50','','D','','1269859429','Laura  Cullinane
113 Queenswood Gardens
Wanstead
London
e11 3sg
GB','loz0200@hotmail.com','020888888' ), 
 ('48','20.80','2.50','IDEAS','D','','1269938271','Clare Driscoll
3 Ohio Grove
Great Sankey
Warrington

WA58DW
GB','clare_driscoll@yahoo.co.uk','07707588975' ), 
 ('49','26.00','2.50','','D','','1269942893','victoria  ruane
15 dartmoor court bovey tracey
newton abbot
devon
tq13 9fg
GB','david.scott144@btinternet.com','01626 832 184' ), 
 ('50','22.00','2.50','','D','','1270129854','Tracy  Newbrook
10 Hengrave Meadow
Newdale
Telford
Shropshire
TF3 5ER
GB','misstracynew@aol.com','07795461824' ), 
 ('51','26.00','2.50','','N','','1270240748','Angela Newton
7 Meadow View
Radstock
Somerset
BA3 3QT
GB','angelanewton23@hotmail.com','07812243791' ), 
 ('52','26.00','2.50','','D','','1270241035','Angela Newton
7 Meadow View
Radstock
Somerset
BA3 3QT
GB','angelanewton23@hotmail.com','07812243791' ), 
 ('53','26.00','2.50','','D','','1270292692','Claire Mehmet
20 Clive Court
36 Southend Road
Beckenham
Kent
BR3 5AE
GB','clairemehmet999@hotmail.com','07979 856235' ), 
 ('54','28.00','2.50','','D','','1270401859','Terri- Lee Brown
35 Pparika Close
Openshaw
Manchester
Lancashire
M11 2LS
GB','tlbrown@hotmail.co.uk','01612236399' ), 
 ('55','24.00','2.50','','D','','1270497216','Lisa Axelby
3 Bright Close
Breydon Park
Great Yarmouth
Norfolk
NR31 0HH
GB','james.williment@ajs-isc.com','07990924296' ), 
 ('56','24.00','2.50','','D','','1270573257','catherine jones
50 graig park road
malpas
newport
south wales
np20 6hd
GB','mrs-piggy@hotmail.co.uk','07943246145' ), 
 ('57','28.00','2.50','','N','','1270574454','Jane Allen
473 Tonbridge Road 
Maidstone 
Kent
ME16 9LH
GB','jane.4llen@gmail.com','07709450748' ), 
 ('58','28.00','2.50','','N','','1270678019','Georgia Price
39 Etchingham Parl Road
Finchley
london
england
N3 2DU
GB','georgiaspriceisright@hotmail.com','07946830425' ), 
 ('59','28.00','2.50','','D','','1270804826','Jane Allen 
473 Tonbridge Road 
Maidstone
Kent
ME16 9LH 
GB','jane.4llen@gmail.com','07709450748' ), 
 ('60','26.00','2.50','','N','','1270824056','Daniel Ford
28 Harris Close 
Broughton Astley
Leicster
Leicestershire
LE9 6NL
GB','fodders2000@yahoo.co.uk','01455286769' ); 
 INSERT INTO `orders` (`id`, `total`, `deliveryttl`, `voucher`, `status`, `notes`, `date`, `delivery`, `email`, `phone` ) VALUES ('61','26.00','2.50','','N','','1270824098','Daniel Ford
28 Harris Close 
Broughton Astley
Leicester
Leicestershire
LE9 6NL
GB','fodders2000@yahoo.co.uk','01455286769' ), 
 ('62','26.00','2.50','','D','','1270824162','Daniel Ford
28 Harris Close 
Broughton Astley
Leicester
Leicestershire
LE9 6NL
GB','fodders2000@yahoo.co.uk','01455286769' ), 
 ('63','42.50','17.50','DREAMS','D','','1270843672','christine michaels
93 northfield road
cheshunt
herts
en8 7rf
GB','info@favourdreams.com','07944246182' ), 
 ('64','28.00','2.50','','D','','1270913030','rhona laing
Ardbeag
east grange
Kinloss
Moray
IV36 2UD
GB','rhona.laing@btinternet.com','07738552879' ), 
 ('65','26.00','2.50','','N','','1270931578','lanna cape
379 camphill road
nuneaton
warwickshire
cv100ju
GB','lanna.cape@btinternet.com','07888204025' ), 
 ('66','28.00','2.50','','D','','1270999964','Hannah  Farr
Walnut Trees
Woolaston
Lydney
Gloucestershire
GL156ns
GB','hanfarr@hotmail.com','07834818682' ), 
 ('67','26.00','2.50','','D','','1271005664','Laura Wilkinson
44 Gerrard Street
Lancaster
Lancashire
LA1 5LZ
GB','wilkinsonlaura@hotmail.com','01524 380146' ), 
 ('68','26.00','2.50','','N','','1271005988','Laura Wilkinson
44 Gerrard Street
Lancaster
Lancashire
LA1 5LZ
GB','wilkinsonlaura@hotmail.com','01524 380146' ), 
 ('69','26.00','2.50','','N','','1271005993','Laura Wilkinson
44 Gerrard Street
Lancaster
Lancashire
LA1 5LZ
GB','wilkinsonlaura@hotmail.com','01524 380146' ), 
 ('70','24.00','2.50','','D','','1271068185','Valerie Hartland-Kane
88 Huron Road
Flat 1
London

SW17 8RD
GB','valerie@polarmedia.co.uk','44-208 767 1397' ), 
 ('71','26.00','2.50','','D','','1271068217','Gay Parris
Sheilas Cottage
44 Cambridge Road, Owlsmoor
Sandhurst
Berkshire
GU47 0SZ
GB','gay.parris@fordway.com','07720467673' ), 
 ('72','37.50','2.50','BERKELEY','D','','1271078723','Phillipa Springer
22 Tennyson Rd
Dursley
Gloucestershire
GL11 4Z
GB','pippa.veilsofberkeley@btconnect.com','01453548260' ), 
 ('73','24.00','2.50','','D','','1271146649','Ceri Adams
283 Crystal Palace Road, East Dulwich, London
London
London
SE22 9JH
GB','ceriadams86@hotmail.com','07883014829' ), 
 ('74','22.00','2.50','','D','','1271359744','Ben Pekarek
5 Brookside
Beckermet
Cumbria
ca212xe
GB','ben.pekarek@btinternet.com','07803045632' ), 
 ('75','22.00','2.50','','D','','1271412991','Susan  Beck
97 Malmains Way
BECKENHAM
Kent
BR3 6SF
GB','suebeck97@hotmail.co.uk','07803006528' ), 
 ('76','26.00','2.50','','D','','1271423729','peyton moran
118 kippen st
coatdyke 
airdrie
lanarkshire
ml6 9ay
GB','kevinmoran.chs@googlemail.com','07931808889' ), 
 ('77','24.00','2.50','','N','','1271432983','asdf asdf
asfd
asdf
asdf
asdf
GB','afd@asdfdsa.com','asf' ), 
 ('78','26.00','2.50','','D','','1271592926','zara warnock
30A woodford drive
armagh
co.armagh
BT60 2AY
GB','talk2zippy@hotmail.com','02837518920' ), 
 ('79','22.00','2.50','','D','','1271662379','Natasha Gebhard
30 Berkeley Square
London
London
W1J 6EW
GB','natgeb@googlemail.com','07726870195' ), 
 ('80','24.00','2.50','','D','','1271793351','Nicola  Sayer
101 Langley
Bretton
Peterborough
Cambridgeshire
PE3 8QD
GB','nictsayer46@btinternet.com','01733311818' ), 
 ('81','22.00','2.50','','D','','1272047055','jill caulfield
4 lime grove
prudhoe
northumberland
NE42 6PR
GB','jill.caulfield@btinternet.com','01661 836 393' ), 
 ('82','22.00','2.50','','C','','1272091084','Rebekah Vann
7 fore street
st stephen
st austell
cornwall
pl26 7nn
GB','becky.vann@hotmail.co.uk','01726824772' ), 
 ('83','26.00','2.50','','D','','1272141202','Paula Mallett
57 St Maurice Road
Plympton
Plymouth
Devon
PL7 1NT
GB','mallett_83@orange.net','01752317052' ), 
 ('84','20.80','2.50','IDEAS','D','','1272292492','Sarah Dunn
5 park close
banbury
banbury
uk
ox16 0sx
GB','sarahdunn21@hotmail.com','07811447089' ), 
 ('85','26.00','2.50','','D','','1272475247','Fiona Hicks
15 F Shepherds Loan
Dundee
Dundee
DD2  1AW
GB','fix2001@hotmail.com','01382 566238' ), 
 ('86','22.00','2.50','','D','','1272637102','Kirsty Ling
389 Ivyhouse Road
Dagenham
Essex
United Kingdom 
RM9 5SB
GB','kirstyling@yahoo.com','07961784227' ), 
 ('87','24.00','2.50','','D','','1272643487','Katherine Barnett
54 Stroud Road
Gloucester
Gloucestershire
GL1 5AJ
GB','daydreamer_111@hotmail.com','07540425514' ), 
 ('88','24.00','2.50','','D','','1272833168','john black
8/2 loganlea place 
edinburgh
scotland
eh76pb
GB','johnnyblack1@hotmail.co.uk','01316566813' ), 
 ('89','20.80','2.50','IDEAS','N','','1272837304','Emma Smith
25, Culduthel Park
Inverness
Inverness-shire
IV2 4RU
GB','ejs1988@hotmail.com','07947247457' ), 
 ('90','17.60','2.50','IDEAS','D','','1272884451','Emma Woolley
121 Brunswick Road
Altrincham
Cheshire
WA14 1LP
GB','ekw14@hotmail.com','07789 888 689' ); 
 INSERT INTO `orders` (`id`, `total`, `deliveryttl`, `voucher`, `status`, `notes`, `date`, `delivery`, `email`, `phone` ) VALUES ('91','22.00','2.50','','D','','1272913034','Mandy  Hampson
2 Ashley Rise
Strathleven Park
Alexandria
West Dumbartonshire
G83 9NL
GB','mandy.hampson@blueyonder.co.uk','07703007296' ), 
 ('92','17.60','2.50','IDEAS','D','','1272964374','NICOLA LOWRIE
32 HENDERSYDE PARK
KELSO
ROXBURGHSHIRE
TD5 7TU
GB','nicola.lowrie@borders.scot.nhs.uk','01573 226707' ), 
 ('93','20.80','2.50','IDEAS','D','','1272976539','Emma Smith
25, Culduthel Park
Inverness
Inverness-shire
IV2 4RU
GB','ejs1988@hotmail.com','07947247457' ), 
 ('94','24.00','2.50','','D','','1272991233','Pauline  Rigby
19 Tyne Avenue
Blackpool
Lancs
FY3 9DR
GB','paulinerigby@blueyonder.co.uk','01253472950' ), 
 ('95','26.00','2.50','','D','','1273154596','Philippa Davies
Gayhurst
Riversdale
Bourne End
Bucks
SL8 5EB
GB','philippajdavies@aol.com','01628 820390' ), 
 ('96','22.00','2.50','','D','','1273243337','GEMMA PARSONS
5 woodrolfe park
TOLLESBURY
Essex
CM9 8TB
GB','gemmaparsons83@hotmail.com','0123456789' ), 
 ('97','24.00','2.50','','D','','1273439011','Claire Woodhouse
22 Bucklebury Heath
South Woodham Ferrers
Chelmsford
Essex
CM35ZU
GB','cflumps@hotmail.com','01245327849' ), 
 ('98','12.80','2.50','TWITTER','D','','1273479544','XENA HEARE
THE GALLERY
13 WATERLOO RD
NORWICH
NORFOLK
NR3 1EH
GB','xena.heare@hotmail.co.uk','07748346576' ), 
 ('99','22.40','2.50','IDEAS','D','','1273493849','Cheryl Hawking
9 Logwell Court
Standens Barn
Northampton
Northants
NN3 9DJ
GB','lucky_pixie80@hotmail.com','01604 410347' ), 
 ('100','22.00','2.50','','D','','1273503397','LYNDA COY
37 BROOMFIELD AVENUE
FAZELEY
TAMWORTH
STAFFS
B78 3QL
GB','lyndcy@hotmail.co.uk','01827783672' ), 
 ('101','22.00','2.50','','D','','1273576804','Gemma Braithwaite
68 Delphinium Way
Lower Darwen
Darwen
Lancs
BB3 0SX
GB','r5glb@hotmail.com','07866764403' ), 
 ('102','28.00','2.50','','D','','1273581989',' Toni Haynes
3 Gillann Street
Knottingley
West yorkshire
WF11 8AB
GB','toadsush@hotmail.co.uk','01977 678449' ), 
 ('103','28.00','2.50','','D','','1273584718','ciara mckenna
23 dacre close
greenford
middlesex
ub6 9uq
GB','ciara_mckenna@yahoo.co.uk','07725655257' ), 
 ('104','24.00','2.50','','D','','1273613006','Claire Chalmers
0/1, 
17 Innellan Gardens
Glasgow
Lanarkshire
G20 0DX
GB','Queen_of_hearts@btopenworld.com','07517232789' ), 
 ('105','26.00','2.50','','D','','1273695194','Colleen Kneafsey
7 Elmwood Close
Woodley
Reading
Berkshire
RG5 3AL
GB','ckneafsey@laingorourke.com','07917 401621' ), 
 ('106','22.00','2.50','','D','','1273781197','Zara Le Tissier
22 Davis Road
Weybridge
Surrey
KT13 0XH
GB','zaraletissier@hotmail.com','07960841524' ), 
 ('107','24.00','2.50','','D','','1273828890','Kat Dervish
19 Salmons Road
Edmonton
London
N9 7JT
GB','katdervish@hotmail.co.uk','07947276343' ), 
 ('108','26.00','2.50','','D','','1273861722','Alli-Jane Doney
26 Glebe Avenue
Saltash

PL12 6DN
GB','allidoney@btinternet.com','07515 390977' ), 
 ('109','22.00','2.50','','D','','1274044005','Lisa Round
134 St Peters Road
Netherton
Dudley
West Midlands
DY2 9HW
GB','sheri140380@yahoo.co.uk','01384357105' ), 
 ('110','26.00','2.50','','D','','1274100187','Carole Millward
c/o Christopher Nixon Opticians
35 High Street, Starbeck,
Harrogate
North Yorkshire
HG2 7LQ
GB','carole@christophernixon.co.uk','01423885764' ), 
 ('111','26.00','2.50','','D','','1274127779','Helen Hughes
2 MacPhail Close
Wokingham
Berkshire
RG40 5YH
GB','hel_hughes@hotmail.com','07958526651' ), 
 ('112','22.00','2.50','','D','','1274194775','Katie  Morris
13 Bishops Way
Widnes
Cheshire
WA8 3LP
GB','kate_morris22@msn.com','' ), 
 ('113','28.00','2.50','','D','','1274204178','Sharon Doherty
The Bridal Showroom
9 Buncrana Road
Derry
Londonderry
BT48 7QD
GB','dohertysharon@yahoo.co.uk','07767020171' ), 
 ('114','16.00','2.50','','N','','1274273040','robert best
forest cottage
forest lane
hanbury
worcestershire
b604hp
GB','rob_best@live.co.uk','07854505925' ), 
 ('115','16.00','2.50','','D','','1274283919','Linda Giddings
The Sett
Studland Avenue
Wickford
Essex
SS12 0JP
GB','linda.winfield@btconnect.com','01268 765872' ), 
 ('116','24.00','2.50','','N','','1274381652','claire fedigan
29 sherborne road
kitt green
wigan
lancashire
wn50ja
GB','clairefedigan@googlemail.com','07540612195' ), 
 ('117','26.00','2.50','','D','','1274382992','Louise  McSorley
69 Cruachan Road
Rutherglen
Glasgow
Scotland
G73 5hh
GB','louise.mcsorley@live.co.uk','07731708662' ), 
 ('118','26.00','2.50','','D','','1274516988','val davies
bryn madog
brynrefail
caernarfon
gwynedd
ll553pf
GB','eryldavies@onetel.net','01286870807' ), 
 ('119','26.00','2.50','','D','','1274556790','melanie Goldby
171 RIVERSTONE WAY
NORTHAMPTON
northamptonshire
NN4 9QW
GB','melanie.goldby@ammd.com','07885 844327' ), 
 ('120','20.80','2.50','IDEAS','D','','1274610355','Catherine Hagan
46 Springhill Road
Glasgow
Lanarkshire
G69 6NT
GB','catriski@hotmail.com','01417733742' ); 
 INSERT INTO `orders` (`id`, `total`, `deliveryttl`, `voucher`, `status`, `notes`, `date`, `delivery`, `email`, `phone` ) VALUES ('121','16.00','2.50','','D','','1274880255','Susanne Whelan
C/o CLS Holdings plc
86 Bondway
London
London
SW8 1SF
GB','susanne.whelan@yahoo.co.uk','07853 377 285' ), 
 ('122','124.50','2.50','STPLUMS','D','','1274951115','Plums Lingerie 
7 Colomberie
ST Helier
Jersey
JE2 4QB
GB','info@plumslingerie.com','01534 731302' ), 
 ('123','19.80','2.50','PERFECT','D','','1274964661','kate richards
7 cedar close
cheadle
staffs
ST10 1SE
GB','k_richards54@tiscali.co.uk','07834 861236' ), 
 ('124','24.00','2.50','','D','','1274975463','Leanne Westell
23 The Wye 
Daventry
Northants
NN11 4PU
GB','lillypaige18@hotmail.co.uk','07794087311' ), 
 ('125','19.20','2.50','IDEAS','D','','1275039452','Deborah Hedley
Fell View, Border Rigg
Bewcastle
Carlisle
Cumbria
CA6 6PX
GB','dhedley@williamhoward.cumbria.sch.uk','07961255433' ), 
 ('126','22.00','2.50','','D','','1275074914','Alex Tucker
26 Centurion Rise
Hastings
East Sussex
TN34 2UL
GB','alexietucker@yahoo.co.uk','07828419309' ), 
 ('127','24.00','2.50','','D','','1275239488','Kirsten  Button
Gardiners Spring
Thundridge
Ware
Herts
SG12 0UF
GB','kir_elliott@hotmail.com','07775 842415' ), 
 ('128','26.00','2.50','','D','','1275245213','Charlotte Cooke
32 Church Lane
Checkley
Stoke on Trent
Staffordshire
ST10 4NJ
GB','charliecooke@ymail.com','07912249028' ), 
 ('129','26.00','2.50','','D','','1275391276','Sarah  Finnimore
19 Ascot Drive
Tamworth
Staffordshire
B771QP
GB','swileman19@btinternet.com','07950514824' ), 
 ('130','20.80','2.50','LINGERIEBLOG','N','','1275473269','Clare Walters
Support For Carers, Londesborough Road Business Park
64-66 Londesborough Road
Scarborough
North Yorkshire
YO12 5AF
GB','supportforcarers@tiscali.co.uk','07851050365' ), 
 ('131','20.80','2.50','LINGERIEBLOG','D','','1275473841','Clare Walters
Support For Carers
Unit 4 Londesborough Road Busniess Park, 64-66 Londesborough Road
Scarborough
North Yorkshire
YO12 5AF
GB','supportforcarers@tiscali.co.uk','01723 364808' ), 
 ('132','17.60','2.50','LINGERIEBLOG','D','','1275483409','Laura Hicks
AHA Medical Services
6 Cathedral Park
Belmont
Durham
DH1 1TF
GB','lozzajane@hotmail.co.uk','01913709199' ), 
 ('133','24.00','2.50','','N','','1275688643','Samantha  Egelstaff 
merrion
llanddoged road
Llanrwst 
Conwy 
ll26 oyu
GB','egelsta@aol.com','01492641540' ), 
 ('134','24.00','2.50','','D','','1275688644','Samantha  Egelstaff 
merrion
llanddoged road
Llanrwst 
Conwy 
ll26 oyu
GB','egelsta@aol.com','01492641540' ), 
 ('135','28.00','2.50','','N','','1275741293','Tammi Robson
21 Towneley Court
Prudhoe
Northumberland
NE42 5FF
GB','Tammi@email.com','07766690698' ), 
 ('136','22.00','2.50','','N','','1275834885','michelle gilmore
272 leigh road
westhoughton
bolton
lancs
bl5 2jz
GB','michgilmore1@hotmail.com','07841833174' ), 
 ('137','22.00','2.50','','D','','1275835098','michelle gilmore
272 leigh road
westhoughton
bolton
lancs
bl5 2jz
GB','michgilmore1@hotmail.com','07841833174' ), 
 ('138','17.60','2.50','LINGERIEBLOG','N','','1276012897','Hannah Squire
5 Mill Lane
Horton cum Studley
Oxon
OX33 1DH
GB','hannah.squire@nortonrose.com','07970188469' ), 
 ('139','24.00','2.50','','D','','1276099391','irene  ashe
52
ferndale road
newtownabbey
northern ireland
bt365as
GB','stephanieashe@ymail.com','07729891826' ), 
 ('140','22.00','2.50','','D','','1276255723','Claire Fowler
14 Rowington Road
Norwich
Norfolk
NR1 3RR
GB','jclivefowler@aol.com','02897562635' ), 
 ('141','22.00','2.50','','D','','1276337795','Charlotte Pinchin
15 West Bank
Carlton
Goole
North Humberside
DN14 9PZ
GB','cjpinchin@yahoo.co.uk','07789225004' ), 
 ('142','22.00','2.50','','D','','1276452455','Louise Wareham
37 Sway Gardens
Throop
Bourenmouth
Dorset
BH8 0PG
GB','louisewareham@yahoo.co.uk','07792 954036' ), 
 ('143','26.00','2.50','','N','','1276682466','Ellen Morris
14 Jackdaw Close
Shoeburyness
Southend on Sea
Essex
SS3 9YQ
GB','lukeellie@live.co.uk','07974 794373' ), 
 ('144','26.00','2.50','','D','','1276682647','Ellen M
14 Jackdaw Close
Shoeburyness
Southend on Sea
Essex
SS3 9YQ
GB','lukeellie@live.co.uk','07974 794373' ), 
 ('145','26.00','2.50','','D','','1276705740','pamela  liddle fawns
52 croftspar grove 
springboig
glasgow

g32 0jy
GB','juneliddle@btinternet.com','07812160638' ), 
 ('146','24.00','2.50','','D','','1276785906','Vilija Cerniauskaite
30 Chadwick Road
St Helens
Merseyside
WA11 9AP
GB','vilija7@gmail.com','07908020175' ), 
 ('147','26.00','2.50','','D','','1276795255','Kerry Scott
4 thirston place
North shields 
Tyne and wear
Ne29 8jt
GB','Kerryscott@live.co.uk','07522446337' ), 
 ('148','22.00','2.50','','D','','1276866316','Joanne Galvin
Medical Department
Aviation House
Gatwick Airport South
West Sussex
RH6 0YR
GB','joanne.galvin@caa.co.uk','01293 573184' ), 
 ('149','24.00','2.50','','N','','1276867009','lisa russell
142 heartley brook ave
Shiregreen


s50hp
GB','lelerussell@yahoo.com','01302762660' ), 
 ('150','22.00','2.50','','N','','1276867066','mags macdonald
10 aird
Sleat, Isle of Skye
Highland
IV45 8RN
GB','office@clandonald.com','01471 844204' ); 
 INSERT INTO `orders` (`id`, `total`, `deliveryttl`, `voucher`, `status`, `notes`, `date`, `delivery`, `email`, `phone` ) VALUES ('151','26.00','2.50','','D','','1276887731','karen murray
133 church street 
stenhousemuir
larbert
stirlingshire
fk5 4bu
GB','blink182_04@hotmail.co.uk','01324552503' ), 
 ('152','22.00','2.50','','D','','1276983772','Caroline Mulholland
14 Crawford Street
Bowling
Bradford
West Yorkshire
BD4 7JJ
GB','mulholland.caroline@googlemail.com','07889153067' ), 
 ('153','22.00','2.50','','D','','1277033363',' Christine  Lewis
15 Vaux Ave
Dovercourt
Harwich
Essex
CO12 4XP
GB','lewischris@btinternet.com','01255 503970' ), 
 ('154','26.00','2.50','','D','','1277051181','JOANNE KILBRIDE
47 DARTINGTON ROAD
PLATT BRIDGE
WIGAN
LANCASHIRE
WN2 5BA
GB','JOANNE.KILBRIDE@WWL.NHS.UK','07891635045' ), 
 ('155','24.00','2.50','','D','','1277056839','CAROL ALLISON
327 COG LANE
BURNLEY
LANCASHIRE
BB11 5JP
GB','allibongo1@hotmail.com','01282 707454' ), 
 ('156','24.00','2.50','','N','','1277057866','Carol Maciejewski
69 Harefield
Hinchley Wood
Esher
Surrey
KT10 9TG
GB','carol.maciejewski@ellmers.co.uk','07957485849' ), 
 ('157','24.00','2.50','','D','','1277058085','Carol Maciejewski
69 Harefield
Hinchley Wood
Esher
Surrey
KT10 9TG
GB','carol.maciejewski@ellmers.co.uk','07957485849' ), 
 ('158','22.00','2.50','','D','','1277068340','Damien Sherratt
9a Simplemarsh Road
Addlestone
Surrey
KT15 1QH
GB','nikosdamien@btopenworld.com','07721046037' ), 
 ('159','22.00','2.50','','D','','1277130600','Laura See
6th Floor, Melbourne House
46 Aldwych
London
lONDON
WC2B 4LL
GB','lauras@mmoser.com','07946 180 561' ), 
 ('160','48.00','2.50','','N','','1277236933','Laura McKenzie
25 Locket Yett View
Bellshill
Lanarkshire
ML4 3HQ
GB','lmckenzie29@yahoo.co.uk','07989779937' ), 
 ('161','48.00','2.50','','D','','1277241074','Laura McKenzie
25 Locket Yett View
Bellshill
Lanarkshire
ML4 3HQ
GB','lmckenzie29@yahoo.co.uk','07989779937' ), 
 ('162','26.00','2.50','','D','','1277326520','Anne Mulvaney
36 Drayton Road
Bearwood
Smethwick
West Midlands
B66 4AJ
GB','annespam@yahoo.co.uk','07827919639' ), 
 ('163','26.00','2.50','','N','','1277364651','la la
la
aa
aa
hfgfsa
GB','laura@laurageorge.co.uk','07810505248' ), 
 ('164','16.00','2.50','','D','','1277389438','Amy Leaney
29 Lea Road
Sonning Common
Reading
RG4 9LH
GB','amyloons@aol.com','01189721488' ), 
 ('165','22.00','2.50','','D','','1277454706','Alison Goucher
168 RABOURNMEAD DRIVE 
Northolt
Middx
UB5 6YL
GB','aligoucher@hotmail.co.uk','07973229162' ), 
 ('166','26.00','2.50','','D','','1277634496','Hannah  seaman
46 Twelve Acres
Braintree
Essex
CM7 3RN
GB','littlespanner26@hotmail.com','01376 322089' ), 
 ('167','21.60','2.50','WEDDING','D','','1277648278','Paula Thomson
33 Ryehill Gardens
Edinburgh
Lothian
EH6 8ES
GB','PaulaThom@hotmail.co.uk','0131 554 3775' ), 
 ('168','22.00','2.50','','N','','1277751938','Ana Catalano
238A St Pauls Road
London
Greater London
N1 2LJ
GB','ana.catalano@gmail.com','07847572699' ), 
 ('169','26.00','2.50','','D','','1277754494','Margaret Bowler
22 Kingston Drive
Hambleton
SELBY
North Yorkshire
YO8 9JS
GB','margaret_bowler@yahoo.co.uk','01757 228642' ), 
 ('170','26.00','2.50','','D','','1277790378','Emma Jenvey
7 Woundale
Bridgnorth
Shropshire
WV15 5PR
GB','oliviajenvey@hotmail.co.uk','01746 710507' ), 
 ('171','24.00','2.50','','N','','1277807914','Nicola Maria Querin
49 Ashurst Road
Cockfosters
Barnet
Herts
EN4 9LH
GB','nicola.m.q@btinternet.com','07903871917' ), 
 ('172','24.00','2.50','','D','','1277810619','Nicola Maria Querin
49 Ashurst Road
Cockfosters
Barnet
Herts
EN4 9LH
GB','nicola.m.q@btinternet.com','07903871917' ), 
 ('173','22.00','2.50','','D','','1277828024','Donna  Stokes
1225 Yardley Wood Road, Shirley
Solihull
West Midlands
B90 1LA
GB','donnastokes@hotmail.co.uk','07976 454 778' ), 
 ('174','22.00','2.50','','D','','1277842596','Hannah Court
Officebroker.com
Mill Lane
Fazeley
Tamworth
B78 3QD
GB','Hannah_court@hotmail.com','07736068475' ), 
 ('175','22.00','2.50','','D','','1277852393','Jessica Barlow
1 Claremont Grove
Hale
Altrincham
Cheshire
WA15 9HH
GB','jessica_charlotte_barlow@yahoo.co.uk','07804 811 272' ), 
 ('176','26.00','2.50','','D','','1278069317','elizabeth ann curry
41 Hawthorn Avenue
Baglan
port talbot
W Glam
sa21 8ph
GB','eacurry@hotmail.co.uk','01639 770201' ), 
 ('177','23.40','2.50','WEDDING','D','','1278076621','Susie Law
Flat 3
33 Lansdowne Street
Hove
East Sussex
BN3 1FS
GB','susie.law@hotmail.co.uk','07733222755' ), 
 ('178','28.00','2.50','','D','','1278164764','nicola maxwell
Flat 1 Hillview
13 Russell Hill
Purley
Surrey
cr8 2jb
GB','maxwelln@hotmail.co.uk','07725897523' ), 
 ('179','24.00','2.50','','D','','1278249792','Louise Pepperdine
Adie OReilly LLP, 3 The Landings
Burton Waters
Lincoln
Lincolnshire
LN1 2TU
GB','louisepepperdine@hotmail.com','07515985089' ), 
 ('180','26.00','2.50','','N','','1278265658','Yeliz Cemal
6 Doric Drive
tadworth
surrey
KT20 6HH
GB','yeliz.cemal@googlemail.com','07967597133' ); 
 INSERT INTO `orders` (`id`, `total`, `deliveryttl`, `voucher`, `status`, `notes`, `date`, `delivery`, `email`, `phone` ) VALUES ('181','26.00','2.50','','D','','1278265845','Yeliz Cemal
6 Doric Drive
tadworth
surrey
KT20 6HH
GB','yeliz.cemal@googlemail.com','07967597133' ), 
 ('182','26.00','2.50','','D','','1278272273','katie le-hanie
haining rigg
bellingham
hexham
northumberland
ne482dt
GB','wiseman_90@hotmail.com','01434 221250' ), 
 ('183','26.00','2.50','','D','','1278273743','Sarah Lamb
16 Augusta Avenue
Collingtree Park
Northampton
Northamptonshire
NN4 0XP
GB','sarahlamb1981@aol.com','07866455751' ), 
 ('184','22.00','2.50','','D','','1278421996','Diana Weston
4 St Peters Road
Bourne
Lincolnshire
PE10 9JE
GB','diana.weston@bourne-grammar.lincs.sch.uk','07952265737' ), 
 ('185','24.00','2.50','','D','','1278622492','Derek Reid
70 loganlee road
addiewell
west calder
west lothian
eh55 8ht
GB','derek.reid@hotmail.com','07943172476' ), 
 ('186','24.00','2.50','','D','','1278675073','nicola jones
45 yelverton road
birkenhead
wirral
ch42 6pe
GB','nickyjones82@hotmail.co.uk','07855833246' ), 
 ('187','19.20','2.50','LINGERIEBLOG','D','','1278680161','Laura Broomhall
32 Jubilee Avenue
Crewe
Cheshire
CW2 7PR
GB','laurap151102@hotmail.com','01270 668290' ), 
 ('188','76.00','2.50','STDARCY','D','','1278865843','Kelly Broome
102 Canal Way
Ilminster
Somerset
TA19 9DH
GB','darcymays@hotmail.co.uk','07922 763604' ), 
 ('189','22.00','2.50','','D','','1278937715','Laura Griffiths
37 Rooks Lane
Thame
Oxfordshire
OX9 2EA
GB','laura.griffiths@hotmail.co.uk','07725 400 758' ), 
 ('190','26.00','2.50','','D','','1278945170','lucy whittingham
black barns, cross houses
shrewsbury
shropshire
sy5 6jt
GB','oldschoolvibes@hotmail.co.uk','07846708499' ), 
 ('191','26.00','2.50','','D','','1278957579','vicky curry
7 Ansmede Grove
Blurton
Stoke on Trent
staffordshire
ST3 3BE
GB','vicky.curry@tiscali.co.uk','07595695059' ), 
 ('192','22.00','2.50','','D','','1279051657','JENNIFER RITCHIE
19 ECHLINE GARDENS
SOUTH QUEENSFERRY
EDINBURGH

EH30 9UT
GB','jenniferritchie@live.com','07812129621' ), 
 ('193','26.00','2.50','','N','','1279201487','june riktoras
73,ogilvie way
livingston
west lothion
eh54 8hn
GB','june.riktoras@sky.com','07510096769' ), 
 ('194','26.00','2.50','','D','','1279203082','june riktoras
73,ogilvie way
livingston
west lothion
eh54 8hn
GB','june.riktoras@sky.com','07510096769' ), 
 ('195','22.00','2.50','','D','','1279305219','Anouska Marynicz
14 Cambray Road
Balham
London
SW12 0DY
GB','anouskamarynicz@yahoo.co.uk','07739 313837' ), 
 ('196','24.00','2.50','','N','','1279390261','Emma Gledhill
30 Sycamore Rise
Wooldale
Holmfirth
West Yorkshire
HD9 7TJ
GB','glitz2@hotmail.com','01484 690533' ), 
 ('197','24.00','2.50','','D','','1279390409','Emma Gledhill
30 Sycamore Rise
Wooldale
Holmfirth
West Yorkshire
HD9 7TJ
GB','glitz2@hotmail.com','01484 690533' ), 
 ('198','26.00','2.50','','D','','1279488730','Rosy  Crampton
Winter Cottage, 21a High Street 
Great Bedwyn
Marlborough
Wiltshire
SN8 1AW
GB','rosycrampton@btinternet.com','07545696438' ), 
 ('199','26.00','2.50','','D','','1279490451','David  MacDonald
13 Canterbury Road
Willesborough
Ashford
Kent
TN24 0BW
GB','macdonald711@btinternet.com','07540171382' ), 
 ('200','16.00','2.50','','D','','1279557570','Jacqueline Hay
14 Dawnay Garth
Shipton by Beningbrough
York
North Yorkshire
YO30 1BH
GB','jacquelineclk63@aol.co.uk','01904 470071' ), 
 ('201','22.00','2.50','','D','','1279562656','Mrs M L  Miller
32 Southfield, Balderton,
Newark
Notts
NG24 3QB
GB','m.l.miller@btinternet.com','01636 671305' ), 
 ('202','16.00','2.50','','D','','1279567982','Julie Prior
60 Kings Road
London Colney
Hertfordshire
AL2 1EW
GB','sweetjulie_prior@hotmail.co.uk','07946439319' ), 
 ('203','24.00','2.50','','D','','1279621279','Lucy  Vokes
Flat 15 Pringle House
18 Newsholme Drive
Winchmore Hill
London
N21 1TX
GB','Lucyvokes@hotmail.co.uk','07771995267' ), 
 ('204','26.00','2.50','','D','','1279711724','Elisabeth Danson
46 Brook Street
Colne Engaine
nr Colchester
Essex
CO6 2JD
GB','elisabethdanson@aol.com','01787 221525' ), 
 ('205','22.00','2.50','','D','','1279823332','JOANNE BIRKBY
9 ATHOLL AVENUE
CREWE
CHESHIRE
CW2 6JH
GB','joannebirkby@rocketmail.com','01270 500960' ), 
 ('206','28.00','2.50','','D','','1279899902','Kerry Brown
3 Kettle Cottage 
Newdigate Road, Beare Green
Dorking
Surrey
RH54QF
GB','kerryb3@hotmail.co.uk','07795 160332' ), 
 ('207','22.00','2.50','','D','','1279969177','george charlton
36 hilltop road 
bearpark
Durham 
Co.Durham
DH77TA
GB','george.charlton@az-1.co.uk','george.charlton@az-1.co.uk' ), 
 ('208','24.00','2.50','','D','','1280081774','Valerie Green
55  Wellington Street
Hertford
Hertfordshire
SG14 3AN
GB','valeriegreen20@hotmail.com','07789728882' ), 
 ('209','16.00','2.50','','D','','1280092550','Nicola Campbell
21 River Mead
Horsham
West Sussex
RH12 1SP
GB','nicky_campbell@hotmail.co.uk','01403249323' ), 
 ('210','24.00','2.50','','D','','1280220703','Kelly Beasley
26 maypole close
Cradley heath
West midlands
B645as
GB','Kelly.Beasley@hotmail.co.uk','07908109600' ); 
 INSERT INTO `orders` (`id`, `total`, `deliveryttl`, `voucher`, `status`, `notes`, `date`, `delivery`, `email`, `phone` ) VALUES ('211','22.00','2.50','','D','','1280229259','Nicola Routley
242  Highview
Vigo
Meopham
Kent
DA13 0UZ
GB','nikkigeorgina@btinternet.com','01732820414' ), 
 ('212','21.60','2.50','WEDDING','D','','1280264139','Hayley Curtis
209 The Roundabout
Birmingham
West Midlands
B31 2UE
GB','hcsmellycat@yahoo.ie','07772373452' ), 
 ('213','26.00','2.50','','D','','1280336528','Stephen Wilson
6 Orchid Close
Douglas
Isle of Man
im27en
GB','wilsy24@hotmail.com','07624200083' ), 
 ('214','22.00','2.50','','D','','1280397593','Menna Jones
Co-op Pharmacy Ltd 1st Floor PCDS
147 Ravenhill Road, Caereithin
Swansea
West Glamorgan
SA5 5AH
GB','menamena80@googlemail.com','01792587018' ), 
 ('215','16.00','2.50','','D','','1280403217','Michelle  Foy
12 Waxwell Lane
Pinner
Middx
HA5 3EN
GB','michelle.foy@capgemini.co.uk','07940 730 378' ), 
 ('216','26.00','2.50','','D','','1280581461','Jacqueline Ledder
2 Balliol Close
Woodley
Stockport
Cheshire
SK6 1JD
GB','jacquelineledder@googlemail.com','01614940747' ), 
 ('217','20.80','2.50','LINGERIEBLOG','D','','1280754188','Joanna Ripley
121 Huron Road
Turnford
Broxbourne
Hertfordshire
EN10 6FT
GB','jo_ripley@hotmail.com','01992440267' ), 
 ('218','26.00','2.50','','N','','1280777546','Dawn Jenvey
280 Leigh Road
Chandlers Ford
Eastleigh
Hampshire
SO53 3AU
GB','dawnkris@hotmail.co.uk','07540860441' ), 
 ('219','26.00','2.50','','N','','1280777620','Dawn Jenvey
280 Leigh Road
Chandlers Ford
Eastleigh
Hampshire
SO53 3AU
GB','dawnkris@hotmail.co.uk','07540860441' ), 
 ('220','26.00','2.50','','D','','1280777792','Dawn Jenvey
280 Leigh Road
Chandlers Ford
Eastleigh
Hampshire
SO53 3AU
GB','dawnkris@hotmail.co.uk','07540860441' ), 
 ('221','24.00','2.50','','D','','1280781832','Gillian Murray
8 Rock Garden Apartments
Clarence Road
Southsea
Hampshire
PO5 2DN
GB','gillianpmurray@googlemail.com','07967204738' ), 
 ('222','28.00','2.50','','D','','1280784638','Lucy Payne
40 Summit Close
Kingswood
Bristol
Bristol
BS15 9AB
GB','Lpayne55@hotmail.com','07505129018' ), 
 ('223','24.00','2.50','','D','','1280833135','sarah burchill
rose and crown
hollybush hill
stoke poges
bucks
sl2 4pw
GB','sarah.burchill@googlemail.com','01753 662829' ), 
 ('224','26.00','2.50','','D','','1280858718','jenni buchanan
24 cornton crescent 
bridge of allan
stirlingshire 
FK9 4DD 
GB','J3NN181@AOL.COM','07837800294' ), 
 ('225','28.00','6.50','','D','','1280939589','Alicia Anast
407 Valley Ave NE F206
Puyallup
Washington
98372
US','alicia.anast@gmail.com','910-689-8203' ), 
 ('226','26.00','2.50','','D','','1280994596','danielle huggett
5 southfield rd
norton
stockton-on-tees
cleveland
ts202eu
GB','daniellehuggett@hotmail.co.uk','07507124964' ), 
 ('227','26.00','2.50','','N','','1281255172','lynne cuerden
holly bush farm
back lane, weeton
preston
lancashire
pr4 3hs
GB','lynne_cuerden@hotmail.co.uk','01253 836136' ), 
 ('228','28.00','2.50','','D','','1281380836','Andy  Ford
Anthony Nixon Furniture
Birch Road
Barnard Castle
Durham
DL12 8JR
GB','andrewford640@btinternet.com','07922080329' ), 
 ('229','26.00','2.50','','D','','1281436770','victoria ramsay
15 eastwell road
dundee
angus
dd23fn
GB','victoria_ramsay910@hotmail.com','07703016304' ), 
 ('230','28.00','2.50','','D','','1281455190','Naomi  Pratt
Twinkles Nurseries 
Clifford Road Boston Spa 
Leeds
West Yorkshire 
LS23 6DB 
GB','naomi@twinklesnurseries.com','01937849588' ), 
 ('231','16.00','2.50','','D','','1281557466','Amanda Marshall
159 Elm Low Road
Wisbech
Cambs
PE14 0DF
GB','amanda.marshall@sequencehome.co.uk','07796175488' ), 
 ('232','16.00','2.50','','D','','1281630345','Rachel  Tremi
38 Haileybury Rd
Woolton
Liverpool
Merseyside
L25 8SW
GB','r-tremi@hotmail.com','07730877162' ), 
 ('233','28.00','2.50','','D','','1281710465','Gemma  Cook
10 Fernbank
Chruch Road
Buckhurst Hill
Essex
IG9 5RN
GB','gemma.cook@sainsburys.co.uk','07985194356' ), 
 ('234','24.00','2.50','','N','','1281733637','fhaklj vhkljv
jksjvlsa
jfskj
jfakj
ja
GB','fjaj@mgail.com','falkj' ), 
 ('235','24.00','2.50','','N','','1281949881','Meghan Lewis
68 Uplands
Chells Manor
Stevenage
Hertfordshire
SG2 7DW
GB','meghan_a_lewis@hotmail.com','07816022128' ), 
 ('236','32.00','2.50','','D','','1282128388','DEBORA CHENG
2 NOTTINGHAM TERRACE
LONDON
LONDON
NW1 4QB
GB','deb_cheng@hotmail.com','07900156352' ), 
 ('237','26.00','2.50','','D','','1282139886','Sara Kershaw
Simply Seafood No 1 The cockle shed
High Street
Leigh On sea
Essex
S29 2ER 
GB','sara.kershaw555@yahoo.co.uk','07850947376' ), 
 ('238','24.00','2.50','','N','','1282142765','Louise Jeive
JPMorgan
60 Victoria Embankment
London
London
EC4Y0JP
GB','louise.jeive@googlemail.com','07904954585' ), 
 ('239','24.00','2.50','','D','','1282143273','Louise Jeive
JPMorgan
60 Victoria Embankment
London
London
EC4Y0JP
GB','louise.jeive@googlemail.com','07904954585' ), 
 ('240','24.00','2.50','','D','','1282147703','Sarah Williams
38 langdale road
Thame
Oxon
Ox9 3wl
GB','Sarahlouisewilliams@live.co.uk','Sarahlouisewilliams@live.co.uk' ); 
 INSERT INTO `orders` (`id`, `total`, `deliveryttl`, `voucher`, `status`, `notes`, `date`, `delivery`, `email`, `phone` ) VALUES ('241','19.20','2.50','LINGERIEBLOG','D','','1282480003','Alicia Hardy
53A Oxford Avenue
London
Greater London
SW20 8LS
GB','aliciahardy@uk2.net','07800913759' ), 
 ('242','24.00','2.50','','D','','1282651344','Helen Tyler
2 Alexandra House
Old St Johns Road 
St Helier 
Jersey 
JE2 3AL 
GB','helty70@gmail.com','07797764763' ), 
 ('243','22.00','2.50','','D','','1282685324','Maria Perez
71 Parkgate Road
Chester
Cheshire
CH1 4AQ
GB','mpd65@blueyonder.co.uk','07976616233' ), 
 ('244','24.00','2.50','','D','','1282730835','Martha Mackenzie
103 Dovers Park
Bathford
Bath
BaNES
BA1 7UE
GB','mmackenzie_86@hotmail.com','07790294695' ), 
 ('245','22.00','2.50','','D','','1282754915','Suzie Nelson
The Old Orchard
5 Horncop Lane
Kendal
Cumbria
LA9 4SR
GB','suzie.nelson@kingsturge.com','07540955045' ), 
 ('246','26.00','2.50','','D','','1282915744','Charles Maryon
100 St Georges Avenue
Westhoughton 
Bolton
Greater Manchester
Bl5 2ez
GB','Charles.Maryon@hotmail.co.uk','07595262145' ), 
 ('247','26.00','2.50','','D','','1282938072','Helen Peaty
53, Cornelius Vale
Chelmsford
Essex
CM26GY
GB','melons82@hotmail.com','01245469489' ), 
 ('248','22.00','6.50','','D','','1283041942','appartment 205 fergusson
26 adelaide street
fremantle
wa
6160
AU','fiestykitten55@hotmail.com','01254878773' ), 
 ('249','26.00','2.50','','D','','1283196461','CLAIRE EMSLIE
13 GAMBRELL AVENUE
WHITCHURCH
SHROPSHIRE
SY13 1GT
GB','claire_emslie@hotmail.com','07858684780' ), 
 ('250','26.00','2.50','','D','','1283196846','caroline hayler
21 kilmorie road 
forest hill
london
se23 2ss
GB','pixiedustxx@btinternet.com','02082916964' ), 
 ('251','28.00','2.50','','D','','1283341549','Sara Stratton
15-27 Gee Street
London
London
EC1V 3RD
GB','quismiss@gmail.com','075 1563 1593' ), 
 ('252','22.00','2.50','','N','','1283423631','Lindsay Bickerton
21 Alexandra Mansions
347 West End Lane
London
London
NW6 1LU
GB','lindsay.bickerton@cliffordchance.com','07956932850' ), 
 ('253','16.00','2.50','','D','','1283429592','Rebecca Robinson
Flat 3, Wulfruna, St Peter Port 
Guernsey
Guernsey 
GY1 2RX
GB','rebecca.robinson@credit-suisse.com','00447781 437 837' ), 
 ('254','26.00','2.50','','D','','1283508370','Kulbir Morris
Rowan House
Main Street
Aberford leeds
West Yorkshire
LS25 3DA
GB','general@morrismorrisuk.com','07941199187' ), 
 ('255','26.00','2.50','','D','','1283591550','Laura Potter
5 Elfrida Terrace
Haybridge
Wells
Somerset
BA5 1AQ
GB','laura.potter52@gmail.com','01749672370' ), 
 ('256','26.00','2.50','','D','','1283802206','Sarah Hill
18 Bercta Road
London

SE9 3TZ
GB','sarahhill8uk@yahoo.co.uk','07793551761' ), 
 ('257','26.00','2.50','','D','','1284041070','Michelle Burns
BSKYB, Unit 1 West Cross Way 
West Cross Industrial Estate
Brentford
Middlesex
TW8 9DE
GB','michelleburns99@yahoo.co.uk','07779591756' ), 
 ('258','22.00','2.50','','D','','1284059525','Ceri Roberts
52 Parc Gilbertson
Pontardawe
Swansea
Swansea
SA8 4PU
GB','cerihinder@msn.com','07968875208' ), 
 ('259','24.00','2.50','','N','','1284408069','Jessica Bruce
25 yeoman street
redcar

TS10 2BQ
GB','jesbruce18@hotmail.com','07792123865' ), 
 ('260','26.00','2.50','','N','','1284466336','KATE BUTLER
HALL FARM
DAGNALL
NR BERKHAMSTED
HERTFORDSHIRE
HP4 1QU
GB','katiebutler81@googlemail.com','07940962804' ), 
 ('261','24.00','2.50','','N','','1284468306','KATE BUTLER
HALL FARM
DAGNALL
NR BERKHAMSTED
HERTFORDSHIRE
HP4 1QU
GB','katiebutler81@googlemail.com','07940962804' ), 
 ('262','24.00','2.50','','D','','1284468455','KATE BUTLER
HALL FARM
DAGNALL
NR BERKHAMSTED
HERTFORDSHIRE
HP4 1QU
GB','katiebutler81@googlemail.com','07940962804' ), 
 ('263','24.00','2.50','','D','','1284478544','Jessica  Bruce
25 yeoman street
Redcar
Cleveland
TS10 2BQ
GB','Jessbruce18@hotmail.com','07792123865' ), 
 ('264','22.00','2.50','','D','','1284482960','Jolene Laureau
Flat 12 Nucleus House
204 West Hill
London
UK
SW15 3JT
GB','jolenelaureau@yahoo.co.uk','07809430121' ), 
 ('265','22.00','2.50','','D','','1284580449','Sophie Emery
15 Victoria Street
Lindley
Huddersfield
West Yorkshire
HD3 3ED
GB','sophemery@gmail.com','07764487165' ), 
 ('266','24.00','2.50','','D','','1284655047','Victoria Hanson
27 Wadham Road
Putney
London
SW15 2LS
GB','vicsta19@hotmail.com','07788184336' ), 
 ('267','28.00','2.50','','N','','1284908829','Jennifer Jones
2 Lorina Grove
Llandudno

LL30 1UQ
GB','eurig@tiscali.co.uk','01492 875311' ), 
 ('268','24.00','2.50','','D','','1284932829','Nicola Morris
122a Westhill Road
Torquay
Devon
TQ1 4NT
GB','honeypiehome@live.co.uk','01803323476' ), 
 ('269','28.00','2.50','','N','','1285100679','Jennifer Jones

Llandudno

LL30 1UQ
GB','eugig@tiscali.co.uk','01492 875311' ), 
 ('270','22.00','2.50','','D','','1285144921','Milica Blagojevic
12 Church Plantation
Keele
Newcastle under Lyme
Staffordshire
ST5 5AY
GB','m.blagojevic@keele.ac.uk','07920499781' ); 
 INSERT INTO `orders` (`id`, `total`, `deliveryttl`, `voucher`, `status`, `notes`, `date`, `delivery`, `email`, `phone` ) VALUES ('271','26.00','6.50','','N','','1285190169','julia moody
7278 cahaba valley rd. apt. 735 b
birmingham
jefferson
35242
US','moodyju@uab.edu','2566554613' ), 
 ('272','26.00','2.50','','D','','1285330697','Stacie Harris
10 Horseshoe Crescent
Burghfield Common
Reading
Berkshire
RG7 3NQ
GB','stacie_hi@msn.com','07748184466' ), 
 ('273','26.00','2.50','','D','','1285842775','Michelle Glover
29 Key Court
Haughton Green, Denton
Manchester
UK
M34 7GE
GB','heaton.dean@yahoo.com','07921438310' ), 
 ('274','23.40','2.50','WEDDING','D','','1285880804','hannah mackrell
10st marys court
durrington lane
worthing
west sussex
bn13 2qb
GB','hannah_hopwood@yahoo.co.uk','07862293330' ), 
 ('275','24.00','2.50','','D','','1286635150','Rhiannon Turner
66 Wildlake
Orton Malborne
Peterborough
CAMBS
PE2 5PQ
GB','rhiannon_turner@hotmail.co.uk','01733 705870' ), 
 ('276','23.40','2.50','PERFECT','D','','1286635824','claire miles
18  hill  street
stockingford
nuneaton
warwickshire
cv10 8je
GB','claire.miles8003@ntlworld.com','02476747553' ), 
 ('277','22.00','2.50','','D','','1286719363','Rachel Barker
22 Empire Walk
Greenhithe
Kent
DA9 9FU
GB','rachel.barker@shs-sales.co.uk','07703332497' ), 
 ('278','22.00','2.50','','D','','1287428854','Bindi Martin
5 Speakman Way
Prescot
Merseyside
L34 5ND
GB','bindimartin@yahoo.co.uk','07917171768' ), 
 ('279','28.00','2.50','','N','','1287487392','Victoria  Young
39 Beckets View
Northampton
Northamptonshire
NN1 5NQ
GB','victoriaanneyoung@yahoo.co.uk','07525 749432' ), 
 ('280','28.00','2.50','','D','','1287488032','Victoria  Young
39 Beckets View
Northampton
Northamptonshire
NN1 5NQ
GB','victoriaanneyoung@yahoo.co.uk','07525 749432' ), 
 ('281','26.00','2.50','','D','','1287498093','Kulbir  Morris
Rowan House
Main Street
Aberford
West Yorkshire
LS25 3DA
GB','general@morrismorrisuk.com','07941199187' ), 
 ('282','23.40','2.50','PERFECT','N','','1288009362','Claire Scahill
130 Roding Road
Loughton
Essex
IG10 3BS
GB','claire_scahill@hotmail.com','07971804420' ), 
 ('283','23.40','2.50','PERFECT','D','','1288010193','Claire Scahill
130 Roding Road
Loughton
Essex
IG10 3BS
GB','claire_scahill@hotmail.com','07971804420' ), 
 ('284','23.40','2.50','PERFECT','D','','1288375886','Chloe Bristow
52 The Street
Costessey
Norwich
Norfolk
NR8 5DD
GB','chloe_bristow@hotmail.com','07809380140' ), 
 ('285','22.00','2.50','','D','','1288648249','Melanie Weaver
161 Brompton Road
Knightsbridge
London 
SW3 1QP
GB','mweaver78@gmail.com','07788583223' ), 
 ('286','22.00','2.50','','N','','1289327261','sharmain jackson
4 little lumpkid
hadleigh
ipswich
suffolk
ip7 6bf
GB','sharmainjackson@hotmail.com','07518341363' ), 
 ('287','4.00','2.50','STDARCY','D','','1289769764','Kelly Broome
102 Canal Way
Ilminster
Somerset
TA19 9DH
GB','darcymays@hotmail.co.uk','01460 55686' ), 
 ('288','22.00','2.50','','D','','1289770961','Agnieszka  Stec
56 Rimini House
Ffordd Garthorne
Cardiff
Cardiff
CF10 4DH 
GB','aida2five@yahoo.co.uk','07928038384' ), 
 ('289','24.00','2.50','','N','','1289840946','Paul Coleman
test test test
test
Harrogate
North Yorkshire
HG1 1HQ
GB','status@loyaltymatters.co.uk','102001020112123' ), 
 ('290','28.00','2.50','','D','','1290366496','Rebecca Swaine
42 Cobham Road
Blandford Forum
Dorset
DT11 7YB
GB','bexswaine@gmail.com','01258 454067' ), 
 ('291','26.00','2.50','','D','','1290507182','Amy Edmonds
29 Appleton Fields
Bishops Stortford
Hertfordshire
CM23 4DP
GB','groovy-amz@hotmail.co.uk','07544158596' ), 
 ('292','28.00','2.50','','D','','1290695780','Eloise Chugg
Sheepfold
Manor Lane, Bredons Norton, 
Tewkesbury
Gloucestershire
GL20 7HB
GB','eloisechugg@hotmail.com','07834374078' ), 
 ('293','26.00','2.50','','D','','1290721679','Pippa Baddeley
Southfields
Began Road, Old St Mellons
Cardiff
Glamorgan
CF3 6XJ
GB','pippabaddeley@fitnessfirst.com','07890272046' ), 
 ('294','43.00','2.50','STSANDERSON','D','','1291070198','ELLIE sanderson
1 LONDON END 
Beaconsfield 
BEACONSFIELD 
BUCKINGHAMSHIRE 
HP92HN
GB','es@elliesanderson.com','01494674440' ), 
 ('295','48.00','2.50','','D','','1291149043','linda McDiarmid
35 Portland Road
Oxford
Oxfordshire
OX2 7EZ
GB','lindamcdiarmid@googlemail.com','01865439236' ), 
 ('296','26.00','2.50','','D','','1291204025','Natalie Currivan
105 Pearl House
43 Princess Way
Swansea
South Wales
SA1 5HF
GB','njc1983@hotmail.co.uk','07972878824' ), 
 ('297','26.00','2.50','','D','','1292012894','Angela  Ioannou
424-426 Green Lanes
Aroma Patisserie
Palmers Green
London
N13 5XG
GB','angelliki@hotmail.co.uk','07949078525' ), 
 ('298','24.00','2.50','','C','','1293986729','amanda  collins
blue bell cottage, copse farm, 
south litchfield
basingstoke
hants
rg25 3bp
GB','amandacollins1971@btinternet.com','07515722269' ), 
 ('299','22.00','2.50','','D','','1294065999','Phil Richardson
186 Forest Gate
Palmersville
Newcastle
Tyne & Wear
NE12 9EN
GB','phil.t.richardson@btinternet.com','07762562542' ), 
 ('300','26.00','2.50','','D','','1294088226','Natalie  Hales
3 GILBERTS WOOD, EWYAS HAROLD,
HEREFORD
HEREFORDSHIRE
HR20JL
GB','bertwest@hotmail.co.uk','01981241537' ); 
 INSERT INTO `orders` (`id`, `total`, `deliveryttl`, `voucher`, `status`, `notes`, `date`, `delivery`, `email`, `phone` ) VALUES ('301','22.00','2.50','','D','','1295187308','KATY  KILSHAW
7 CAER GOG
PANTYMWYN
MOLD
FLINTSHIRE
CH7 5EX
GB','katykilshaw@hotmail.com','07813 148909' ), 
 ('302','22.00','2.50','','D','','1295640383','Gerald  Davis
45 Cardinal Avenue
Borehamwood
Herts
WD6 1EN
GB','jrcasbard@hotmail.com','020 8953 2202' ), 
 ('303','17.60','2.50','LINGERIEBLOG','D','','1295910016','Mhairi Scott
7 Silverbirch Drive
Ballumbie Castle
Broughty Ferry
Angus
Dd5 3ns
GB','mhairiscott@btinternet.com','07547152292' ), 
 ('304','17.60','2.50','KNICKERSBLOG','D','','1295980257','Rebecca France
133 Leymoor Road
Golcar
Huddersfield
West Yorkshire
HD74QX
GB','becca_france@hotmail.co.uk','07825031189' ), 
 ('305','26.00','2.50','','D','','1296504839','Jessica Furnell
20 Holland House Road
Walton-le-dale
Preston
Lancashire
PR54JG
GB','jessicafurnell@hotmail.com','07773337414' ), 
 ('306','16.00','2.50','','D','','1296584529','Sara Lancaster
43 OTTRELLS MEAD BRADLEY STOKE 
Bradley Stoke
Bristol
Bristol
BS32 0AJ
GB','saralancaster007@yahoo.co.uk','07904047583' ), 
 ('307','26.00','2.50','','D','','1296599029','Helene  Mansfield
64, Bishops Road,Whitchurch,
Cardiff
cardiff
CF14 1 LW
GB','helenemansfield@hotmail.co.uk','029 20418836' ), 
 ('308','19.80','2.50','PERFECT','D','','1296845110','Susan  Mellors
8 South View
Whitwell
Worksop
Notts
S80 4NP
GB','marksuemellors@aol.com','01909723338' ), 
 ('309','22.00','2.50','','D','','1296914241','Carleen Myatt
40 Blantyre Avenue
Worsley
Manchester
Lancashire
M28 3DN
GB','carleen.myatt@sky.com','01617902433' ), 
 ('310','22.00','2.50','','N','','1297098460','Stacey Radcliffe
55 Staunton Rise
dedridge
Livingston
West Lothian
EH54 6PB
GB','livistacey@hotmail.com','07889450895' ), 
 ('311','22.00','2.50','','D','','1297099001','Stacey Radcliffe
55 Staunton Rise
dedridge
Livingston
West Lothian
EH54 6PB
GB','livistacey@hotmail.com','07889450895' ), 
 ('312','21.60','2.50','PERFECT','D','','1297155819','Lisa Hargrave
20
Chiltern Avenue
Shepshed
Leicestershire
LE12 9BW
GB','lhargrave@hotmail.co.uk','07817864181' ), 
 ('313','19.80','2.50','PERFECT','D','','1297184535','Sherrie  Forbes
21 Crowwood Road
Glasgow
North Lanarkshire
G69 9BU
GB','sherrie55@hotmail.co.uk','07730526666' ), 
 ('314','23.40','2.50','PERFECT','D','','1297695556','Karen Baker
3 Glebe Aalin Close
Ballaugh
Ramsey
Isle of Man
IM7 5BY
GB','hwood@mcb.net','07624 432750' ), 
 ('315','19.80','2.50','PERFECT','D','','1297700548','Tina Sparks
13 Misterton Court
West Bridge road
Battersea
London
SW113NL
GB','x_x_sparky_x_x@hotmail.com','07891627117' ), 
 ('316','22.00','2.50','','N','','1298279433','Rachelle Lynne
16 The Avenue
Radlett
Hertfordshire
WD7 7DW
GB','rachellelynne@hotmail.co.uk','07540260781' ), 
 ('317','22.00','2.50','','D','','1298279609','Rachelle Lynne
16 The Avenue
Radlett
Hertfordshire
WD7 7DW
GB','rachellelynne@hotmail.co.uk','07540260781' ), 
 ('318','5.50','2.50','STANGELS','N','','1298320435','Katrina Coe
Rhud Y Galen
Bethal
Caernarfon
North Wales
LL55 1UL
GB','info@crookedangelsbridal.co.uk','01425 618534' ), 
 ('319','28.00','2.50','','C','','1298324384','Karen Ray
16 Hawthorne Place
Epsom
Surrey
KT17 4AA
GB','karenlouiseray@yahoo.co.uk','07737 469209' ), 
 ('320','26.00','2.50','','D','','1298371095','Sarah Burke
sunny-brae
victoria road
balcombe
west sussex
rh17 6lj
GB','sarah.burke@tgb.toyota.co.uk','01444811842' ), 
 ('321','22.00','2.50','','D','','1298386877','Angela Sherwood
Hillwood Farm
Town Green Road
Watton
Norfolk
IP25 6RD
GB','hillwoodfarm@tiscali.co.uk','01953885907' ), 
 ('322','26.00','2.50','','D','','1298721335','Jessica  Rees
The Furze
Pamington
Tewkesbury
Glos
GL20 8LX
GB','jessvet11@hotmail.com','07779 164696' ), 
 ('323','24.00','2.50','','D','','1298803960','Katie Broome
3 Abbey Farm Cottage
WoodWalton
Huntingdon
Cambridgeshire
PE28 5YL
GB','katiebroome1@hotmail.com','07857414996' ), 
 ('324','24.00','2.50','','D','','1298977056','kerry dolton
46 blackmore crescent
southway
plymouth
devon
pl6 6nu
GB','ikklepeep@hotmail.co.uk','07738159691' ), 
 ('325','16.00','2.50','','N','','1299067345','Laura Manger
56 Spring Lane
Kenilworth
Warwickshire
CV8 2HD
GB','laura.s.manger@hotmail.com','07791145842' ), 
 ('326','16.00','2.50','','D','','1299067559','Laura Manger
56 Spring Lane
Kenilworth
Warwickshire
CV8 2HD
GB','laura.s.manger@hotmail.com','07791145842' ), 
 ('327','26.00','2.50','','N','','1299417200','Hannah Gallier
58 Harman Road
Sutton Coldfield
West Midlands
B72 1AJ
GB','hannahgallier@gmail.com','07740342022' ), 
 ('328','28.00','2.50','','D','','1299417209','Matt Gawn
Specsavers Opticians
57 West Street
Horsham

RH12 1PL
GB','gawnycartoons@yahoo.co.uk','01403 275115' ), 
 ('329','20.80','2.50','LINGERIEBLOG','D','','1299694638','Deborah  Padgett
10 Holystone Drive
Ingleby Barwick
Stockton-On-Tees
TS17 0PW
GB','deborahpadgett@hotmail.co.uk','07779017167' ), 
 ('330','22.00','2.50','','D','','1299867203','Amy Kempster
13
Orchard Grove
Diss
Norfolk
IP224LX
GB','amykempster@aol.com','01379644481' ); 
 INSERT INTO `orders` (`id`, `total`, `deliveryttl`, `voucher`, `status`, `notes`, `date`, `delivery`, `email`, `phone` ) VALUES ('331','16.00','2.50','','D','','1300053433','Angela Binns
25 Forester Grove
Carlton
Nottingham
Nottingham
NG4 1FR
GB','angbinns@hotmail.co.uk','07812729003' ), 
 ('332','22.00','2.50','','D','','1300097142','Lorraine Batchelor
66 Kenley Road
Merton Park
London 
Greater London
SW193JH
GB','Lebatchelor@hotmail.com','07967892826' ), 
 ('333','26.00','2.50','','D','','1300359328','Jeannette Sutor
The Woodlands, Tippings Hill
Hunt End
Redditch
Worcestershire
B97 5QJ
GB','jensutor@live.co.uk','01527 542568' ), 
 ('334','26.00','2.50','','D','','1300549965','Lucy Bell
4 Pear Tree Lane
Hempstead
Gillingham
Kent
ME7 3RJ
GB','davidtdownes@yahoo.co.uk','01634 389412' ), 
 ('335','24.00','2.50','','D','','1300731004','Angela Secq
24 The Lane
Mickleby
Saltburn-By-The-Sea
Cleveland
TS13 5LX
GB','angela_secq@hotmail.com','07592828356' ), 
 ('336','24.00','2.50','','D','','1301239688','Zoe Crouch
23 Tolvaddon Close
Woking
Surrey
GU21 3LS
GB','zoe_crouch@hotmail.com','01483566062' ), 
 ('337','26.00','2.50','','D','','1301263280','Mine Tahir
61 Stone Park Avenue
Beckenham
Kent
London
BR33LU
GB','minetahir@hotmail.com','07947812334' ), 
 ('338','22.00','2.50','','D','','1301732443','Nina Phillips
26 Cherry Road
Gorleston-on-sea
Great Yarmouth 
Norfolk
NR31 8EB
GB','ninajpp@gmail.com','07900555541' ), 
 ('339','28.00','2.50','','D','','1301783749','HAYLEY SCHOFIELD
61 Claudian Way
Chadwell St Mary
Grays
Essex
RM16 4QU
GB','hayleysemai1@hotmail.com','07905281648' ), 
 ('340','24.00','2.50','','D','','1301839945','Sylvia Horn
17 Fernlea Avenue
Ferndown
Dorset
BH22 8HG
GB','sylviahorn@yahoo.co.uk','01202 873477' ), 
 ('341','26.00','2.50','','D','','1302614715','Alison Kapma
C/- Louise Davies
Flat 10, 32 Keswick Road
Putney
London
SW15 2JT
GB','kapma.shop@gmail.com','07773854494' ), 
 ('342','26.00','2.50','','N','','1302699775','Kulbir  Morris
Rowan House 
Main Street
Leeds
West Yorkshire
LS25 3DA
GB','general@morrismorrisuk.com','07941199187' ), 
 ('343','26.00','2.50','','D','','1302700045','Kulbir  Morris
Rowan House 
Main Street
Leeds
West Yorkshire
LS25 3DA
GB','general@morrismorrisuk.com','07941199187' ), 
 ('344','26.00','2.50','','D','','1302949703','Rachael Lewis
16 Fife Street
Abercynon
Mountain Ash
Rhondda Cynon Taf
CF45 4TU
GB','rachielew84@yahoo.co.uk','07969834488' ), 
 ('345','26.00','2.50','','D','','1302981563','Colleen Osborne
1 Roche Drive
Goole
E Yorks
DN14 6NL
GB','osborne19@tiscali.co.uk','01405 764683' ), 
 ('346','26.00','2.50','','D','','1303056280','samar al-tahan
15 alverstone road
wembley
middlesex
ha9 9sd
GB','samartahan@hotmail.com','07884054109' ), 
 ('347','24.00','2.50','','D','','1303067798','Laura  Holian
Toshiba Information Systems, Toshiba Court
Weybridge Business Park, Addlestone Road
Weybridge
Surrey
KT15 2UL
GB','lauraholian@hotmail.com','07977075205' ), 
 ('348','26.00','2.50','','D','','1303115243','lindsay carruthers
9 elmcroft drive
Ashford
Middlesex
TW15 2PQ
GB','lindsayeliz@hotmail.co.uk','07787165499' ), 
 ('349','22.00','2.50','','D','','1303754429','Helen Lunn
12 Pagnell Avenue
Selby
North Yorkshire
YO8 8DG
GB','helenlunn2607@live.co.uk','07863902361' ), 
 ('350','24.00','2.50','','N','','1303839113','Amelia Field
90 Royal Parade
Eastbourne
East Sussex
BN22 7AE
GB','amelia-f@hotmail.com','07590765874' ), 
 ('351','24.00','2.50','','N','','1303839124','Amelia Field
90 Royal Parade
Eastbourne
East Sussex
BN22 7AE
GB','amelia-f@hotmail.com','07590765874' ), 
 ('352','22.00','2.50','','D','','1303840458','Elaine Denton
Apt 8, 1 Canalbridge Close
LOUGHBOROUGH
LEICESTERSHIRE
LE11 1SP
GB','elaine.denton@orange.net','07970474358' ), 
 ('353','24.00','2.50','','D','','1303929423','Amelia Field
90 Royal Parade
Eastbourne
East Sussex
BN22 7AE
GB','amelia-f@hotmail.com','07590765874' ), 
 ('354','26.00','2.50','','D','','1303933241','JON ILLINGWORTH
14 Dunsville Drive
COVENTRY
WEST MIDLANDS
CV22HS
GB','jon_illy@hotmail.com','07773634229' ), 
 ('355','22.00','2.50','','D','','1303978535','DIANA MENVILLE
13 WELBECK ROAD
EAST HAM
LONDON
E6 3ET
GB','dmenville@hotmail.co.uk','07984822898' ), 
 ('356','16.00','2.50','','N','','1304076263','Elle Tylee
11a bell common
Epping
Essex
CM16 4DY
GB','eleanor@devilfishcreative.com','07725840202' ), 
 ('357','16.00','2.50','','D','','1304077269','Elle Tylee
11a bell common
Epping
Essex
CM16 4DY
GB','eleanor@devilfishcreative.com','07725840202' ), 
 ('358','26.00','2.50','','D','','1304105455','Hayley Howard
341 Bishopsford Road
Morden
Surrey
SM4 6BW
GB','hayleylh@hotmail.co.uk','07887961790' ), 
 ('359','22.00','2.50','','D','','1304199845','Martina Rossi
120 Broomfield Avenue
Newton Mearns
Glasgow
East Renfrewshire
G77 5JR
GB','martinarossi@gmail.com','07736938195' ), 
 ('360','22.00','2.50','','D','','1304273729','sofina begum
3 the chandlers
the calls
leeds
west yorkshire
ls2 7ej
GB','sofi_miah@hotmail.co.uk','07889012686' ); 
 INSERT INTO `orders` (`id`, `total`, `deliveryttl`, `voucher`, `status`, `notes`, `date`, `delivery`, `email`, `phone` ) VALUES ('361','26.00','2.50','','D','','1304333012','Lynda Gilchrist
Flat 7, 21 Hyndland Road
Hyndland
Glasgow
Lanarkshire
G12 9UZ
GB','lynda_e_gilchrist@hotmail.com','01413399897' ), 
 ('362','22.00','2.50','','D','','1304334570','Kelly Steeples
2 Rachael Court 
Horbury
Wakeifeld
West Yorkshire
WF4 6GE
GB','k.steeples@southdale.wakefield.sch.uk','07709337898' ), 
 ('363','22.00','2.50','','D','','1304538338','Charlotte Hopkins
20 Gurnard Walk
Plymouth
Devon
PL3 6PE
GB','lottieh86@yahoo.co.uk','07875578790' ), 
 ('364','20.80','2.50','LINGERIEBLOG','D','','1304590420','Rachel  Sealey
42 Lynch road
Berkeley
Glos
Gloucestershire
GL13 9TA
GB','r_m_sealey@live.co.uk','07860468760' ), 
 ('365','22.00','2.50','','D','','1304719059','Claire Webster
19 Corhill Drive 
Aberdeen
Aberdeenshire
AB16 5 LY
GB','clarabella107@hotmail.com','01224480008' ), 
 ('366','22.00','2.50','','D','','1304939494','Lianne Conroy
24 Market Hill
Boroughbridge
North Yorkshire
YO51 9JU
GB','lianne.conroy@zyro.co.uk','01845 521706' ), 
 ('367','28.00','2.50','','C','','1305038674','Amy Neale
The Old Bank
1 High Street
Warboys
Cambridgeshire
PE282RH
GB','amyjaneneale@aol.com','01487823263' ), 
 ('368','26.00','2.50','','C','','1305392148','Shelagh Donnelly
5 Scotch Firs,
Fownhope
Hereford
Herefordshire
HR1 4NW
GB','pamelajean1011@aol.com','01432 860481' ), 
 ('369','26.00','2.50','','N','','1305496706','Danielle McMahon
175 Mealough Road
Belfast
Down
BT8 8LY
GB','danielle.mcmahon@qub.ac.uk','07738004750' ), 
 ('370','26.00','2.50','','D','','1305906335','Felicity Hazell
21 Portland Avenue
Sittingbourne
Kent
ME10 3QY
GB','flickhazel@hotmail.co.uk','07581354798' ), 
 ('371','24.00','2.50','','D','','1306271328','Claire  Muller
26 Balmoral Road
Kingston-upon-thames
Surrey
KT1 2TY
GB','clairemuller79@googlemail.com','07710425901' ), 
 ('372','26.00','2.50','','D','','1306510923','Dianne  Mann
29 Brechin Road
Kirriemuir

dd84da
GB','totallydianne@live.co.uk','01575572394' ), 
 ('373','22.00','2.50','','N','','1306747140','Sheila Macdonald
52 Josephs Road
Guildford
Surrey
GU1 1DW
GB','sheila.macdonald3@ntlworld.com','07763569148' ), 
 ('374','24.00','2.50','','D','','1306765339','Vicky Garrity
4 Thornhill Grove
Steeton
West Yorkshire
BD20 6ST
GB','vagarrity@hotmail.co.uk','07860 705207' ), 
 ('375','24.00','2.50','','D','','1307110934','Veronica  Mackie
204 South Village
Pumpherston
Livingston
West Lothian
EH53 0LS
GB','veronica.mackie@westlothian.gov.uk','01506 776539' ), 
 ('376','24.00','6.50','','D','','1307609539','Claire Hardman
10/3 Moruben Road
Mosman
Sydney
Australia
2088
AU','hardmanclaire_@hotmail.com','0426973335' ), 
 ('377','16.00','2.50','','D','','1307617146','hannah griffiths
1 Weetmans Drive
colchester
essex
co4 9ea
GB','hlg@hotmail.co.uk','07976756137' ), 
 ('378','16.00','2.50','','N','','1307876632','Julie Woodhouse
c/o Manor Pharmacy 
49 Brook Street
Sutton in Ashfield
Nottinghamshire 
NG17 1ES
GB','lisa.woodhouse@ntlworld.com','01623 400020' ), 
 ('379','16.00','2.50','','D','','1307887452','Julie Woodhouse
c/o Manor Pharmacy
49 Brook Street
Sutton in Ashfield
Nottinghamshire
NG17 1ES
GB','p.woodhouse@krcs.co.uk','01623 400020' ), 
 ('380','94.00','2.50','STPLATINUM','D','','1307989604','Sarah Redrup Platinum Wedding Collection
1 Tyburn Lane
Pulloxhill
Beds
MK45 5HG
GB','enquiries@platinumweddingcollection.com','01583 881382' ), 
 ('381','26.00','2.50','','D','','1308077655','John Williams
27 Clinton lane
Kenilworth
Warwickshire
Cv8 1as
GB','Williams-john47@sky.com','07766762009' ), 
 ('382','22.00','2.50','','D','','1308214071','VAILA Fenwick
Da Hoga
Park
Shetland
Shetland Islands
ZE2 9HP
GB','vaila_45@hotmail.com','01950431432' ), 
 ('383','26.00','2.50','','D','','1308215465','Claire  Pritchard
C/O Evesham Electronics Limited
Unit 2 Willow Park, Hinton Road, Childswickham
Nr Broadway
Worcs
WR12 7HY
GB','claireLpritchard@aol.com','07973690302' ), 
 ('384','19.20','2.50','LINGERIEBLOG','C','','1308385468','Vesna  Siljanovska
154 Effra Road
Wimbledon
London
SW19 8PR
GB','Ves1981@yahoo.com','07747 020 748' ), 
 ('385','165.50','2.50','STPLUMS','D','','1308644438','Plums Lingerie 
7 La Colomberie
St Helier
Jersey
JE2 $qb
GB','info@plumslingerie.com','01534 731302' ), 
 ('386','22.00','2.50','','D','','1308746236','Sheila Macdonald
52 Josephs Road
Guildford
Surrrey
GU1 1DW
GB','sheila.macdconald3@ntlworld.com','07763569148' ), 
 ('387','24.00','2.50','','D','','1308991701','Joanna Burns
Modern Homes
55 Oxford Street
Workington
Cumbria
CA14 2RU
GB','jo_burns@yahoo.com','07740400975' ), 
 ('388','24.00','2.50','','N','','1309168224','sylvia phipp
35 aldsworth ave
goring by sea
worthing
west sussex
bn12 4xg
GB','dave_phipp@btinternet.com','01903244099' ), 
 ('389','24.00','2.50','','D','','1309168478','sylvia phipp
35 aldsworth ave
goring by sea
worthing
west sussex
bn12 4xg
GB','dave_phipp@btinternet.com','01903244099' ), 
 ('390','20.80','2.50','LINGERIEBLOG','D','','1309375232','Laura  King 
25 Malham Drive
Whitefield
Manchester
Lancashire
M45 8SD 
GB','laura.king22@btinternet.com','07730431365' ); 
 INSERT INTO `orders` (`id`, `total`, `deliveryttl`, `voucher`, `status`, `notes`, `date`, `delivery`, `email`, `phone` ) VALUES ('391','20.80','2.50','LINGERIEBLOG','D','','1309456888','Chloe Dale
Experience House
5 Port Hill
Hertford
Hertfordshire
SG14 1PJ
GB','bernadette104@hotmail.com','07894500818' ), 
 ('392','26.00','2.50','','D','','1309599258','joanne collier
16 Boswell Road
Cowley
Oxford
Oxfordshire
OX4 3HN
GB','joanne.collier771@btinternet.com','07903355771' ), 
 ('393','24.00','2.50','','D','','1309780811','lisa mara
unit 14 whitworth court
manor park road
runcorn
cheshire
wa7 1wa
GB','lisa_jayne_mara@yahoo.co.uk','07940481143' ), 
 ('394','28.00','2.50','','D','','1310120057','Leanne Bell
51 
Loris Court
Cambridge
Cambridgeshire
CB19GF
GB','leanne.k.bell@googlemail.com','07846296561' ), 
 ('395','24.00','2.50','','D','','1310156533','Nicola Haslam
Echo Meadow House
Grounds Farm Lane
Kenilworth
Warwickshire
CV8 1PP
GB','nhcinderella@hotmail.com','07917831909' ), 
 ('396','23.40','2.50','WEDDING','D','','1310730577','Christina Maciver
4 Cragganmore Place
Dunsmuir Park
Kilmarnock
East Ayrshire
KA3 1NF
GB','tinamaciver@hotmail.com','07590203829' ), 
 ('397','22.00','2.50','','D','','1310765266','lynn gilvear
54 lindsay way
knightsridge
livingston
west lothian
eh54 8lq
GB','lgilvear@hotmail.com','07535687385' ), 
 ('398','22.00','2.50','','N','','1311080339','Mrs Ann Summers
1,Turnberry Lane
Collingtree Park
Northampton
Northamptonshire
NN4 0PA
GB','a.summers@ssesurf.co.uk','07909681199' ), 
 ('399','22.00','2.50','','D','','1311080510','Mrs Ann Summers
1,Turnberry Lane
Collingtree Park
Northampton
Northamptonshire
NN4 0PA
GB','a.summers@ssesurf.co.uk','07909681199' ), 
 ('400','16.00','2.50','','D','','1311330685','kate green
29a
high street
sandy
bedfordshire
sg19 1ag
GB','kategreen_5@hotmail.com','07970403290' ), 
 ('401','24.00','2.50','','D','','1311683634','Laura Downes Downes
11-13 Charterhouse Buildings
London
London
EC1M 7AP
GB','laurajcdownes@googlemail.com','07940706657' ), 
 ('402','22.00','2.50','','D','','1311931669','Catherine McKenzie
88 Sandringham Road
Redcar
Cleveland
TS10 1ES
GB','Catherinemck@hotmail.co.uk','07411977940' ), 
 ('403','24.00','2.50','','D','','1312061691','helen baty
53 Greenlee Drive .Benton
Newcastle upon tyne
Tyne and wear
ne77ga
GB','h.baty@sky.com','01912663453' ), 
 ('404','22.00','2.50','','N','','1312134694','suzanna wyatt
flat 6, 3 Holly court
Heaton Chapel
stockport
cheshire
SK4 5AB
GB','lilmissbloodthirsty@hotmail.com','07745624068' ), 
 ('405','22.00','2.50','','D','','1312203875','suzanna wyatt
flat 6, 3 Holly court
Heaton Chapel
stockport
cheshire
SK4 5AB
GB','lilmissbloodthirsty@hotmail.com','07745624068' ), 
 ('406','24.00','2.50','','D','','1312209382','kerry  nicola
212 high road 
woodford green
essex
ig8 9hh
GB','princesskn@hotmail.co.uk','07956511053' ), 
 ('407','28.00','2.50','','N','','1312388249','kirsty jones
bottom flat, 7 egerton rd
davenport
stockport
Greater Manchester
sk3 8sr
GB','k_m_jones@hotmail.co.uk','01614824222' ), 
 ('408','19.80','2.50','PERFECT','D','','1312490169','Laura Henworth
7th Floor, The Point
37 North Wharf Road
London
London
W2 1AF
GB','l.a.henworth@gmail.com','447944614604' ), 
 ('409','28.00','2.50','','D','','1312797234','Angela McGeoghegan
4 Laxdale Lane
Stornoway
Isle of Lewis
HS20DR
GB','angelamcg4@hotmail.com','01851705648' ), 
 ('410','22.00','2.50','','D','','1313102255','Ciara  Elliott
87 lettercreeve
Ballymena
Antrim
Bt42 2eu
GB','C.elliott2212@hotmail.co.uk','07725893750' ), 
 ('411','16.00','2.50','','D','','1313136599','Jo Perks
27 Wisteria Gardens
South Shields
Tyne and Wear
NE34 8EL
GB','jo_perks83@yahoo.co.uk','07976753141' ), 
 ('412','28.00','2.50','','D','','1313161076','Stacy Cole
47 kenilworth Crescent
Parkfield
Wolverhampton
West Midlands
WV4 6SU
GB','Stace.cole@googlemail.com','01902 343970' ), 
 ('413','22.00','2.50','','D','','1313310260','Emma Jones
33 Heath Road
Navenby
Lincoln
Lincolnshire
LN5 0TT
GB','billabong_bird1@hotmail.com','07854 829133' ), 
 ('414','24.00','2.50','','N','','1313482430','Ann Smith
118 The Greenway
OXTED
Surrey
RH8 0JD
GB','annsmithm@gmail.com','01883713269' ), 
 ('415','24.00','2.50','','D','','1313482933','Ann Smith
118 The Greenway
OXTED
Surrey
RH8 0JD
GB','annsmithm@gmail.com','01883713269' ), 
 ('416','24.00','2.50','','N','','1313486865','Margaret Nathan
The Anvil, Stortford Road, Clavering
Saffron Walden
Essex
CB11 4PE
GB','nathananvil@yahoo.co.uk','01799 550619' ), 
 ('417','24.00','2.50','','D','','1313486985','Margaret Nathan
The Anvil, Stortford Road, Clavering
Saffron Walden
Essex
CB11 4PE
GB','nathananvil@yahoo.co.uk','01799 550619' ), 
 ('418','26.00','2.50','','C','','1313663411','Richard Jessop
Zenith Provecta, Anglia House
Holly Park Mills
Calverley
Leeds
LS28 5QS
GB','richard.jessop@zenithprovecta.co.uk','07764476958' ), 
 ('419','22.00','6.50','','D','','1313873388','Dena Murphy
30 Asgard drive
Grange Manor
Waterford
Waterford
051
IE','denamurphy@live.ie','0863616707' ), 
 ('420','28.00','2.50','','D','','1314102154','dawn hirst
9 beech close
sherburn in elmet
leeds
north yorks
ls25 6ee
GB','dah65@hotmail.co.uk','07968054768' ); 
 INSERT INTO `orders` (`id`, `total`, `deliveryttl`, `voucher`, `status`, `notes`, `date`, `delivery`, `email`, `phone` ) VALUES ('421','22.00','2.50','','D','','1314218101','Simon Hampton
34 St Marys Road
Leatherhead
Surrey
KT22 8EY
GB','simon.hampton@kuoni.co.uk','07970 706138' ), 
 ('422','22.00','2.50','','D','','1314479564','Victoria Hales
212b Mare Street
London
London
E8 3RD
GB','vjh3hales@gmail.com','07799480515' ), 
 ('423','24.00','2.50','','N','','1314692852','Chris Rutledge
St Mary\'s Walk
Town
County
Postcode
GB','chris@loyaltymatters.co.uk','01423857900' ), 
 ('424','22.00','2.50','','D','','1314814967','Nicola Gallagher
3 Friars
Capel St Mary
Ipswich
Suffolk
IP9 2XS
GB','nikkigallagher@gmail.com','07944 119625' ), 
 ('425','24.00','2.50','','D','','1314865835','Ingrid mann
The Nave
St Andrew\'s Park
Norwich

NR7 0GG
GB','suki.mann@ntlworld.com','07990763187' ), 
 ('426','24.00','2.50','','D','','1315254427','bryonne paz
125 stanley green road
poole
dorset
bh15 3ae
GB','bryonnepaz@hotmail.co.uk','07921635375' ), 
 ('427','22.00','2.50','','N','','1315311360','Tashi Serdup
Risk Infrastructure and Control, 7C 083 , 7th Floor, Lloyds Bank Corporate Markets,
 33 Old Broad Street, London, 
London
London
EC2N 1HZ
GB','tashi.serdup@lloydsbanking.com','07921263266' ), 
 ('428','22.00','2.50','','D','','1315423433','Lorraine  Downes
1 alexander gate 
stevenage 
herts 
sg1 2lu 
GB','lorraine_39@hotmail.com','07908839979 ' ), 
 ('429','28.00','2.50','','D','','1315491412','Shannon Nursimulu
7 Halford Rd
London
United Kingdom
SW61JS
GB','shannon.nursimulu@gmail.com','07775429097' ), 
 ('430','24.00','2.50','','D','','1315636634','Caroline Dobbyn
41 Millharbour
Apt 97
London
London
E149NB
GB','cdobbyn6@hotmail.com','07797297408' ), 
 ('431','26.00','2.50','','D','','1315832019','Fran Valentine
5, Pinecroft Road
Wokingham
Berkshire
RG41 4AL
GB','franvalentine@tesco.net','01189783913' ), 
 ('432','20.00','2.50','STBANTAMS','D','','1315837545','Emma  Lloyd
The Bridal Room
6 Keil Close - High St 
Broadway
Great Britain (UK)
WR12 7EJ
GB','emmalloyd@hotmail.co.uk','01386859070' ), 
 ('433','26.00','2.50','','D','','1316251165','Gwen Rahardja
5 Frognal Court
Finchley Road
London
Greater London
NW3 5HL
GB','gw2n@hotmail.com','07909585866' ), 
 ('434','16.00','2.50','','D','','1316539463','margaret  henderson
15 claymore avenue
portlethen
aberdeen
scotland
ab12 4rf
GB','mnmhenderson@btinternet.com','07877872349' ), 
 ('435','22.00','2.50','','N','','1316718849','Simon Worrall
51 Verdin Street
Northwich
Cheshire
CW97BX
GB','wogger01@aol.com','' ), 
 ('436','22.00','2.50','','D','','1316719493','Simon Worrall
51 Verdin Street
Northwich
Cheshire
CW97BX
GB','wogger01@aol.com','' ), 
 ('437','24.00','2.50','','D','','1316873733','Kate Webber
Whetcombe Cottage
Trusham
Newton Abbot
Devon
TQ13 0NT
GB','kr_webber@hotmail.co.uk','07976236607' ), 
 ('438','24.00','2.50','','D','','1318323150','Alison Talbot
Mayfair, lynn road shouldham
kings lynn
norfolk
Pe33 0bt
GB','alison_talbot@hotmail.co.uk','01366 348078' ), 
 ('439','22.00','2.50','','D','','1318694906','Victoria  Bailey
21 Emu Road
Battersea
London
London
SW8 3PS
GB','vickybailey@hotmail.com','07989743815' ), 
 ('440','22.00','2.50','','D','','1319725387','Jayne Hayes
12 Birchfield Avenue
Beacon Park
Plymouth
Devon
PL2 3LA
GB','jayneallen@live.co.uk','01752316083' ), 
 ('441','24.00','2.50','','D','','1319985427','Karen Smith
1349 Neath Road
Hafod
Swansea
W.Glam
SA1 2HN
GB','karen_helen_123@hotmail.co.uk','07746222119' ), 
 ('442','26.00','2.50','','D','','1320251530','Victoria Walker
1 Aylwin Close
Basingstoke
Hampshire
RG21 3PD
GB','victoria.walker@shoosmiths.co.uk','07880 794866' ), 
 ('443','6.00','2.50','STPLATINUM','D','','1320678407','Amanda  Murray
110 Nokes Court
Common Wealth Drive
Crawley
West Sussex
RH10 1AN
GB','enquiries@platinumweddingcollection.com','07515546344' ), 
 ('444','24.00','2.50','','D','','1321218597','Monika Kuzelova
Reed Smith, The Broadgate Tower
20 Primrose Street
London
London
EC2A 2RS
GB','m_kuzela@yahoo.co.uk','07826894747' ), 
 ('445','26.00','2.50','','D','','1321885728','Rosemary Cole
9 Greenways
Sandhurst
Berkshire
GU47 8PJ
GB','rmcole@sky.com','07760 258131' ), 
 ('446','26.00','2.50','','D','','1322211634','Michelle Glover
29 Key Court
Haughton Green, Denton
Manchester

M34 7GE
GB','m.glover@cestrian.co.uk','07921438310' ), 
 ('447','24.00','2.50','','D','','1322591109','Lesley Smith
32 Trafalgar Drive
Flitwick
Bedfordshire
MK45 1EF
GB','savvas33@btinternet.com','07920 440273' ), 
 ('448','17.60','2.50','TWITTER','D','','1322645641','Anna  Adams
The Paddocks
St Mary\'s Church
Cowbridge
Vale of Glamorgan
CF71 7LT
GB','eirlys_bellin@hotmail.com','07968 389405' ), 
 ('449','17.60','2.50','TWITTER','D','','1322645956','Eirlys Bellin
Flat 1
144 Knights Hill
London
London
SE27 0SR
GB','eirlys_bellin@hotmail.com','07968 389405' ), 
 ('450','26.00','2.50','','N','','1323018981','isobell neil
2 torridon drive flat 0/3
dean park
renfrewshire
pa4 0us
GB','ineil@btinternet.com','97908767524' ); 
 INSERT INTO `orders` (`id`, `total`, `deliveryttl`, `voucher`, `status`, `notes`, `date`, `delivery`, `email`, `phone` ) VALUES ('451','19.80','2.50','PERFECT','D','','1325265365','Maria Smith
3 Sunningdale Close
York
Yorkshire
YO26 5PD
GB','x_xmariax_x@hotmail.com','07857754472' ), 
 ('452','24.00','2.50','','D','','1325764841','Susan  Kelleher
East Stand Manchester United
Sir Matt Busby Way
Manchester
Lancashire
M16 0RA 
GB','sue.kelleher@manutd.co.uk','07762522252' ), 
 ('453','22.00','2.50','','D','','1327152042','Karen Devereux
89 Longford Road
Neath Abbey
Neath

SA10 7HF
GB','karendevereux@yahoo.co.uk','07813590444' ), 
 ('454','22.00','2.50','','D','','1328478812','Sarah Broder
5 Cobham Close
Sidcup
Kent
DA15 9PH
GB','ksbroder@btinternet.com','07507639051' ), 
 ('455','16.00','2.50','','D','','1328533675','Barbara Sowden
65, Moorland avenue 
Gidersome
Leeds
West Yorkhire
LS27 7BY
GB','babs61@ntlworld.com','01132190296' ), 
 ('456','28.00','2.50','','D','','1329047386','Karen Speirs
25 Piper Crescent
Burntisland
Fife
KY3 0JS
GB','karenspeirs63@gmail.com','07812736068' ), 
 ('457','24.00','2.50','','D','','1329155743','katie smith
225 curzon road
ashton-under-lyne
lanashire
ol6 9nb
GB','katie.smith25@btinternet.com','07879771746' ), 
 ('458','24.00','2.50','','D','','1329395457','Patricia Bendall
70 Main Street.
Kinoulton
Nottingham
Nottinghamshire
NG12 3EN
GB','m.j.bendall@talk21.com','01949823968' ), 
 ('459','22.00','2.50','STBANTAMS','D','','1329479544','Emma Lloyd
The Bridal Room
6 Keil Close - High St
Broadway
Worcestershire
WR12 7DP
GB','emma@thebridalroombroadway.co.uk','01386859070' ), 
 ('460','19.20','2.50','TWITTER','D','','1329572138','Helen Walton
Kionslieu
Patrick Road
St Johns
Isle of Man
IM4 3BN
GB','helen.l.walton@gmail.com','07624226600' ), 
 ('461','19.20','2.50','TWITTER','D','','1329572238','Helen Walton
Kionslieu
Patrick Road
St Johns
Isle of Man
IM4 3BN
GB','helen.l.walton@gmail.com','07624226600' ), 
 ('462','22.00','2.50','','D','','1329820215','Julie Barrett
21 Brae House
Manse Brae, Rhu
HELENSBURGH
Argyll and Bute
G84 8RE
GB','jcashman1@hotmail.com','07912758030' ), 
 ('463','24.00','2.50','','D','','1330613072','sheena Bayliss
14 sherringham close
Fawley
Southampton
Hampshire
SO45 1SQ
GB','esoxlucius20lb@sky.com','442380894384' ), 
 ('464','24.00','2.50','','D','','1331894921','Christine Stark
36 Blue Waters Drive
Broadsands
Paignton
Devon
TQ4 6JE
GB','david@davidstark.wanadoo.co.uk','01803843503' ), 
 ('465','22.00','2.50','','N','','1332230604','eleanor james
23 orlestone view
hamstreet
ashford
kent
tn262lb
GB','eleanorjames10@gmail.co.uk','07585442952' ), 
 ('466','22.00','2.50','','N','','1332230770','eleanor james
23 orlestone view
hamstreet
ashford
kent
tn262lb
GB','eleanorjames10@gmail.co.uk','07585442952' ), 
 ('467','22.00','2.50','','N','','1332504651','Kirsty  Kelly
Flat 2/1 22 Holmhead Place
Cathcart
Glasgow
Glasgow
G44 4HB
GB','kirsty.kely@hotmail.co.uk','07969446166' ), 
 ('468','22.00','2.50','','N','','1332511801','eleanor james
23 orlestone view
Hamstreet
ashford
kent
tn26 2lb
GB','eleanorjames10@gmail.com','07585442952' ), 
 ('469','24.00','2.50','','C','','1332519615','Laura George
50 Harlow Crescsent
Harrogate
North Yorkshire
HG2 0AJ
GB','laurageorge1@hotmail.co.uk','07810505248' ), 
 ('470','18.00','2.50','','D','','1332525852','eleanor james
23 orlestone view
hamstreet
ashford
kent
tn26 2lb
GB','eleanorjames10@gmail.com','07585442952' ), 
 ('471','22.00','2.50','','D','','1332602144','Olivia Crellin
2 Forge Cottages
Leith hill lane
Holmbury St Mary
Surrey
RH5 6LY
GB','o.g.crellin@live.co.uk','07761912153' ), 
 ('472','18.00','2.50','','D','','1332774513','Jenni Hastie
2 Regency Chambers
Bury
Lancashire
BL9 0JW
GB','jennifer_hastie@yahoo.com','07875156049' ), 
 ('473','14.40','2.50','PERFECT','D','','1333908769','Katherine Newman
9 Thompson close
dane bank, Denton
manchester
Lancashire
m34 2pq
GB','katnewman@o2email.co.uk','07725317306' ), 
 ('474','18.00','2.50','','D','','1333967952','Kathryn Eccleston
Mill Stream Cottage
Cusgarne
Truro
Cornwall
TR4 8RW
GB','kjeccleston@hotmail.com','07763817011' ), 
 ('475','18.00','2.50','','D','','1334054816','Sarah Cole
Sawmill Cottage
Low Nibthwaite
Nr Ulverston
Cumbria
LA12 8DE
GB','misssarahcole@googlemail.com','07590206327' ), 
 ('476','28.00','2.50','','D','','1334161823','Zara  Kazaryan
46-48 Grosvenor Gardens
London
London
wc1b 5ae
GB','zarakazaryan@googlemail.com','07703632797' ), 
 ('477','18.00','2.50','','N','','1334964198','Rosalind Duncan
1 Skythorn Way
Falkirk
Falkirk District
FK1 5NR
GB','Thistle_wiz_ere@hotmail.com','07900538204' ), 
 ('478','18.00','2.50','','D','','1334964200','Rosalind Duncan
1 Skythorn Way
Falkirk
Falkirk District
FK1 5NR
GB','Thistle_wiz_ere@hotmail.com','07900538204' ), 
 ('479','19.20','2.50','TWITTER','D','','1336131159','Laura Coulcher
5 Loxley Court
Cranemead
Ware
Hertfordshire
SG12 9Ff
GB','Laura.coulcher@hotmail.com','07990514714' ), 
 ('480','24.00','2.50','','D','','1336209604','Amanda Friend
125 Wesleyan Road
Peterborough
Cambs
PE1 3RX
GB','afriend75@hotmail.co.uk','07593174579' ); 
 INSERT INTO `orders` (`id`, `total`, `deliveryttl`, `voucher`, `status`, `notes`, `date`, `delivery`, `email`, `phone` ) VALUES ('481','22.00','2.50','','D','','1336414850','Jennifer Neill
45 Hoizer Street
Coatbridge
North Lanarkshire
ML5 4BA
GB','Jenniferneill25@aol.com','07730588087' ), 
 ('482','16.00','2.50','','D','','1336987031','Rosalyn Norbury-mccann
4 belgravia gardens
Manchester
Lancs
M21 9jj
GB','Roxyroz1982@yahoo.co.uk','07983617234' ), 
 ('483','22.00','2.50','','N','','1337083059','Rosalyn Barr
1 Glenmanor avenue
Moodiesburn
North Lanarkshire
G69 0BW
GB','rosdalbeck@yahoo.co.uk','07738544678' ), 
 ('484','22.00','2.50','','N','','1337083750','Rosalyn Barr
1 Glenmanor avenue
Moodiesburn
North Lanarkshire
G69 0BW
GB','rosdalbeck@yahoo.co.uk','07738544678' ), 
 ('485','22.00','2.50','','D','','1337083820','Rosalyn Barr
1 Glenmanor avenue
Moodiesburn
North Lanarkshire
G69 0BW
GB','rosdalbeck@yahoo.co.uk','07738544678' ), 
 ('486','24.00','2.50','','N','','1337947092','Mary Dyson
21 KNOWLES VIEW
Lakeside Court
SWADLINCOTE
South Derbyshire
DE11 8JR
GB','dyson-mary81@hotmail.co.uk','01283 221205' ), 
 ('487','60.00','2.50','STBRIDEZILLAS','D','','1337950488','Amanda Davis-Harrison Bridezillas Ltd
18 High Street
Keynsham
Bristol
Avon
BS31 1DQ
GB','amanda@bridezillas.biz','0117 9043039' ), 
 ('488','22.00','2.50','','D','','1338074773','michelle bushell
41 eastern ave
reading
berks
RG1 5RX
GB','michellebushell3@hotmail.com','07763634843' ), 
 ('489','46.00','6.50','','N','','1338210358','Valia Pischou
41, Ipsilantou str
Loutraki
Loutraki
20300
GR','valpischos@gmail.com','00306979215482' ), 
 ('490','24.00','2.50','','D','','1338479520','Pat  Connell
6 Thorncliffe Dr
Darwen
Lancs
BB3 3QA
GB','patricia_connell@hotmail.com','01254 772406' ), 
 ('491','26.00','2.50','','D','','1338628690','Abby Alford
46 Cae Bracla
Bridgend
Bridgend
CF31 2HF
GB','abby.alford@googlemail.com','01656 750199' ), 
 ('492','22.00','6.50','','N','','1339153919','Shannon Moe
120 Westlake Avenue North #215
Seattle
Washington
98109
US','shannonmoe82@gmail.com','07971235890' ), 
 ('493','22.00','2.50','','D','','1339515793','Laura Smith
39 Sweet Bay Crescent
Godinton Park
Ashford
Kent
TN233pq
GB','lauzsmith1990@hotmail.com','07805217391' ), 
 ('494','50.00','6.50','','N','','1339577045','Valia Pischou
41, Ipsilantou str
Loutraki
Loutraki
20300
GR','valpischos@gmail.com','00306979215482' ), 
 ('495','46.00','6.50','','D','','1339582911','Valia Pischou
41, Ipsilantou str
Loutraki
Loutraki
20300
GR','valiapischou@gmail.com','00306979215482' ), 
 ('496','120.38','2.50','STPLUMS','N','','1339689813','plums lingerie
7 colomberie
st helier

je2 4qb
GB','info@plumslingerie.com','01534731302' ), 
 ('497','120.38','2.50','STPLUMS','N','','1339689892','plums lingerie
7 colomberie
st helier

je2 4qb
GB','info@plumslingerie.com','01534731302' ), 
 ('498','120.38','2.50','STPLUMS','N','','1339689947','plums lingerie
7 colomberie
st helier

je2 4qb
GB','info@plumslingerie.com','01534731302' ), 
 ('499','24.00','2.50','','D','','1339704126','Stephanie Jagger
20 Primrose Way
Wembley
Middlesex
HA0 1DR
GB','stephaniemjagger@aol.co.uk','07429654875' ), 
 ('500','24.00','2.50','','D','','1339759065','Linda Marshall
62 Sycamore Close
Ipswich
Suffolk
IP8 3RL
GB','lindamae212@gmail.com','07879050531' ), 
 ('501','152.50','2.50','STPLUMS','D','','1339763234','plums lingerie
7 colomberie
st helier
jerset
je24qb
GB','info@plumslingerie.com','01534731302' ), 
 ('502','28.00','2.50','','D','','1339765121','julie  wood
329 Manchester Road
Droylsden
Manchester 
Lancs 
M43  6HF 
GB','Juliewood5@aol.com','0161 301 3008 ' ), 
 ('503','24.00','2.50','','D','','1341318928','Amelia  Crawshaw
10b Severus Road
London
London
SW111PL
GB','amycrawshaw@yahoo.com','07758311655' ), 
 ('504','22.00','2.50','STBANTAMS','D','','1341354243','Emma Lloyd
The Bridal Room
6 Keil Close - High St
Broadway
Worcestershire
WR12 7DP
GB','emmalloyd@hotmail.co.uk','441386852806' ), 
 ('505','22.00','2.50','','D','','1341403270','Hannah  Currie
4-6 
Bank Street
Kirriemuir
Angus
DD8 4BG
GB','hannahfc3@hotmail.com','07957 927 140' ), 
 ('506','41.00','2.50','STBRIDEZILLAS','D','','1341999845','Amanda  Davis-Harrison
Bridezillas Ltd
18 High Streeet
Keynsham 
Bristol
BS31 1DQ
GB','amanda@bridezillas.biz','0117 9043039' ), 
 ('507','22.00','2.50','','N','','1342527874','Stacey Seaman
81 Westhill Road
Kings Norton
Birmingham
West Midlands
B38 8TG
GB','moon0569@hotmail.co.uk','07547244237' ), 
 ('508','22.00','2.50','','D','','1342528388','Stacey Seaman
81 Westhill Road
Kings Norton
Birmingham
West Midlands
B38 8TG
GB','moon0569@hotmail.co.uk','07547244237' ), 
 ('509','24.00','2.50','','D','','1342612819','Lucy   Gale 
Lantern Lodge 
St Mary\'s Close
Brede
East Sussex
TN31 6HD
GB','lucilastic2@hotmail.com','01424 882663' ), 
 ('510','6.50','2.50','STPLUMS','D','','1342688452','plums lingerie
7 colomberie
st helier
jersey
je2 4qb
GB','info@plumslingerie.com','01534731302' ); 
 INSERT INTO `orders` (`id`, `total`, `deliveryttl`, `voucher`, `status`, `notes`, `date`, `delivery`, `email`, `phone` ) VALUES ('511','19.00','2.50','STBANTAMS','D','','1343059431','Emma Lloyd
The Bridal Room - Broadway  - 
6 Keil Close, High St
Broadway
Worcestershire
WR12 7DP
GB','emma@thebridalroombroadway.co.uk','01386 859070' ), 
 ('512','26.00','2.50','','D','','1343305952','amy fitzgerald
3 geary road
london
greater london
nw101he
GB','amyx1981x@hotmail.co.uk','02084508617' ), 
 ('513','22.00','2.50','','D','','1343653634','Donna McPherson
Kilchurn
Golf Course Road
Blairgowrie
Perthshire
PH10 6LF
GB','donnamcp@btconnect.com','01250 876689' ), 
 ('514','28.00','2.50','','D','','1343677232','Kimberley Jeakings
Apartment 10 18 High Street
Kidlington
Oxfordshire
OX5 2FW
GB','Kjeakings@hotmail.com','07787188753' ), 
 ('515','24.00','2.50','','D','','1343758172','Natalie De gouveia
77 Dedworth rd
Windsor chiropractic clinic
Windsor
Berkhire
Sl4 5bb
GB','nataliedgv@gmail.com','07587150054' ), 
 ('516','22.00','2.50','','D','','1343764900','Kate Hudson
31 Harpenden road
St Albans
Hertfordshire
Al3 6bj
GB','Kateh1982@hotmail.com','07764859089' ), 
 ('517','24.00','2.50','','D','','1343938201','kirsty smith
flat 15 wenderholme court 68 south park hill road
southcroydon 
england
cr2 7dw
GB','sxckirst@hotmail.com','07984858500' ), 
 ('518','24.00','2.50','','D','','1344619101','Evelyn  Burn
23 thistle road 
Thorny close 
Sunderland 
Tyne and wear
Sr34ln
GB','Mandystephenson@hotmail.com','07975688926' ), 
 ('519','22.00','2.50','','D','','1344772440','Emma  Papworth
10 kinross close
Holcombe Brook
Bury
LANCASHIRE
BL0 9WD
GB','emmapaps@yahoo.co.uk','01204 880683' ), 
 ('520','22.00','2.50','','N','','1345113961','chloe kenway
Flat 2, 105 Portsmouth Road
Southampton
Hampshire
SO19 9BE
GB','chloe.kenway@hotmail.co.uk','02380437642' ), 
 ('521','19.20','2.50','LINGERIEBLOG','D','','1345122293','Petra Hourd
Marco Polo Travel Publishing, Pinewood, Chineham Business Park
Crockford Lane
Basingstoke
Hampshire
RG24 8AL
GB','petra.hourd@hotmail.co.uk','07879658721' ), 
 ('522','28.00','2.50','','D','','1345543796','Egle Juogadalvyte
162 Glastonbury Rd.
Morden
Surrey
SM4 6PQ
GB','alex.fleming@hotmail.co.uk','07807564149' ), 
 ('523','16.00','2.50','','N','','1345924809','elizabeth wheeler
24 maria street
darwen
lancashire
bb3 2jn
GB','elizabethwheeler@hotmail.co.uk','01254 760053' ), 
 ('524','16.00','2.50','','D','','1345924814','elizabeth wheeler
24 maria street
darwen
lancashire
bb3 2jn
GB','elizabethwheeler@hotmail.co.uk','01254 760053' ), 
 ('525','24.00','2.50','','D','','1346510212','Abi Hallett
Eon UK, Sherwood Park, Newstead Court
Little Oak Drive
Annesley
Nottinghamshire
NG150DR
GB','abihau@me.com','07825001778' ), 
 ('526','28.00','2.50','','D','','1346672350','Louise Turner
TTA, Resource House
20 Denmark Street
Wokingham
Berkshire
RG402BB
GB','louise@tta-int.co.uk','01189369100' ), 
 ('527','24.00','2.50','','N','','1346928931','Sofia Buttarazzi
Teapigs ltd
1 the old pumping station, pump alley 
Brentford
Middlesex
TW80AP
GB','sofia@teapigs.co.uk','07956858775' ), 
 ('528','24.00','2.50','','C','','1346930310','Laura  George
50 Harlow Crecent
Harrogate
North Yorkshire
HG2 0AJ
GB','laura@laurageorge.co.uk','07810505248' ), 
 ('529','24.00','2.50','','D','','1346931858','Sofia Buttarazzi
Teapigs
1 the old puming station, pump alley
Brentford
Middlesex
Tw90ap
GB','Sofia@teapigs.co.uk','07956858775' ), 
 ('530','26.00','2.50','','D','','1347195035','Sandra Cobo
111 Ralph Court
Queensway
London

W2 5HU
GB','sanderolicobo@hotmail.com','' ), 
 ('531','28.00','2.50','','N','','1348304230','Mary Boyle
36 castle road
Bridge of weir
Renfrewshire
Pa11 3qg
GB','maryboyle@me.com','01505 690162' ), 
 ('532','24.00','2.50','','D','','1348304366','TANIA Antoniou
40 ridge road
Winchmore hill
London
N21 3ea
GB','Taniachristou@hotmail.com','07957405396' ), 
 ('533','24.00','2.50','','N','','1348388958','Emma Tyrrell
Upgrade options ltd
Clock tower, green hills enterprise centre
Tilford
Surrey
Gu10 2DZ
GB','Emmat@upgrade.co.uk','07967957779' ), 
 ('534','24.00','2.50','','D','','1348388961','Emma Tyrrell
Upgrade options ltd
Clock tower, green hills enterprise centre
Tilford
Surrey
Gu10 2DZ
GB','Emmat@upgrade.co.uk','07967957779' ), 
 ('535','35.00','2.50','','N','','1348400299','angela mcnamara
49
norden toad
maidenhead
berkshire
sl64az
GB','lloydmc1@hotmail.com','01628777493' ), 
 ('536','25.75','2.50','STBANTAMS','D','','1349091369','Emma  Lloyd
The Bridal Room
6 Keil Close - High St
Broadway
Worcestershire
WR12 7DP
GB','emma@thebridalroombroadway.co.uk','441386859070' ), 
 ('537','22.00','2.50','','D','','1349277317','Chantel Swarts
23 East Kent Avenue
Northfeel
Kent
DA11 9HU
GB','chanie.swarts@dnv.com','07525260845' ), 
 ('538','19.20','2.50','TWITTER','D','','1349286427','Emily Bateman
27 Woodsyre
Sydenham Hill
London

SE266SS
GB','emily_mail1234@hotmail.com','07513053374' ), 
 ('539','24.00','2.50','','D','','1349377131','Susanna Coulson
5 Wheeler Court
Southdowns Park
Haywards Heath
West Sussex
RH16 4SS
GB','susannacoulson@hotmail.co.uk','07540065632' ), 
 ('540','22.00','2.50','','D','','1349620682','laura wilbraham
26 campbell street
pemberton
wigan
england
wn59ht
GB','laurawilbraham@hotmail.co.uk','07871051356' ); 
 INSERT INTO `orders` (`id`, `total`, `deliveryttl`, `voucher`, `status`, `notes`, `date`, `delivery`, `email`, `phone` ) VALUES ('541','78.00','2.50','','D','','1349707792','Lesley Higginbotham
4 Meadowsweet Drive
Priorslee
Telford
Shropshire
TF2 9QX
GB','lesley@johnsilverfinearts.com','01952 275666' ), 
 ('542','22.00','2.50','','D','','1350202217','Tracy Black
63 bent spur road
Kearsley
Bolton 
Lancashire
Bl4 8pd
GB','Peretblack@btinternet.com','07811122140' ), 
 ('543','24.00','2.50','','N','','1350479098','Clare Bateson
Barn Cottage, Campden Road
Shipston On Stour
Warwickshire
CV36 4PZ
GB','changjiclare@yahoo.com','07414582508' ), 
 ('544','28.00','2.50','','D','','1350676237','Sarah Schmitt
82 Sketty Road
Swansea
West Glamorgan
SA2 0JZ
GB','sarahschmitt87@gmail.com','01792 511648' ), 
 ('545','24.00','2.50','','D','','1350679228','Sharon  Foy
6 Croft Avenue
Tamworth
Staffordshire
B79 8AY
GB','sharonfoy@hotmail.co.uk','01827 69363' ), 
 ('546','28.00','2.50','','D','','1350902870','Elizabeth Harvey
207 Cotefield Drive
Leighton Buzzard
Bedfordshire
LU73DT
GB','lharvey1983@yahoo.co.uk','07889395680' ), 
 ('547','28.00','2.50','','D','','1351026794','Jill Hanson
Brook House
Nackington Road
Canterbury
Kent
CT4 7AX
GB','jhanson125@aol.com','07890 945953' ), 
 ('548','22.00','2.50','','D','','1351446680','Gary Rafferty
23, ledi avenue 
Tullibody
Clackmannanshire
Fk10 2rz
GB','tmckenzie86@o2.co.uk','07809883529' ), 
 ('549','105.50','2.50','STBRIDEZILLAS','N','','1351769776','AMANDA Davis-Harrisojn
Bridezillas Ltd
18 High Street
Keynsham  Bristolo
Somerset
BS31 1DQ
GB','amanda@bridezillas.biz','0117 9043039' ), 
 ('550','105.50','2.50','STBRIDEZILLAS','D','','1351769825','AMANDA Davis-Harrison
Bridezillas Ltd
18 High Street
Keynsham  Bristolo
Somerset
BS31 1DQ
GB','amanda@bridezillas.biz','0117 9043039' ), 
 ('551','24.00','2.50','','D','','1352829009','Hannah  Quig
c/o Shepherd and Wedderburn LLP
1 Exchange Crescent, Conference Square
Edinburgh
Midlothian
EH3 8UL
GB','hannah.quig@shepwedd.co.uk','07807596353' ), 
 ('552','24.00','2.50','','D','','1353931412','Sharda Dean
Paulls House
Aldenham School, Aldenham Road,
Elstree
Hertfordshire
WD6 3AJ
GB','shardadean@btinternet.com','01923 839317' ), 
 ('553','24.00','2.50','','D','','1353950000','Nicola Green
8 Winsbury Way
Bradley Stoke
Bristol
South Gloucestershire
BS32 9BE
GB','nic-green@hotmail.co.uk','07773097834' ), 
 ('554','24.00','2.50','','N','','1354522470','Heather Boobyer
The Old Manse 
Petworth Road
Haslemere
Surrey
GU27 2HX
GB','Heatherboobyer@hotmail.com','01428652963' ), 
 ('555','24.00','2.50','','D','','1354522558','Heather Boobyer
The Old Manse 
36 Petworth Road
Haslemere
Surrey
GU27 2HX
GB','Heatherboobyer@hotmail.com','01428652963' ), 
 ('556','22.00','2.50','','N','','1354624814','Sarah Tait
46 Burnbrae Terrace
EDINBURGH
MidLothian
EH19 3DB
GB','Sarah1984_33@hotmail.com','07854317915' ), 
 ('557','22.00','2.50','','D','','1354624942','Sarah Tait
46 Burnbrae Terrace
EDINBURGH
MidLothian
EH19 3DB
GB','Sarah1984_33@hotmail.com','07854317915' ), 
 ('558','24.00','2.50','','D','','1354700625','Hannah Manson
The Ashmolean Museum, Beaumont Street
Oxford
Oxfordshire
OX1 2PH
GB','hannah.manson@ashmus.ox.ac.uk','01865 288197' ), 
 ('559','22.00','2.50','','D','','1354737893','Joanne  Innes
27 mavisbank
Loanhead
Midlothian
Eh20 9dd
GB','joanne8@hotmail.co.uk','07793682529' ), 
 ('560','28.00','2.50','','D','','1354838959','AGNIESZKA MARCZAK
WILDWOOD , 1 WEST COMMON
GERRARDS CROSS

SL9 7QN
GB','agamarczak@yahoo.co.uk','07916121739' ), 
 ('561','22.00','2.50','','D','','1354964838','Julia  Leech
12 The Street
Blundeston
Lowestoft
Suffolk
NR32 5AQ
GB','julialytton@live.co.uk','07785241241' ), 
 ('562','24.00','2.50','','D','','1354967826','Margaret  Marshall
37 St. James Road
Sevenoaks
Kent
TN13 3NG
GB','marshallmargaret34@gmail.com','07779 725594' ), 
 ('563','24.00','2.50','','D','','1355040656','Joanna Coral
16 The Squirrels
24a The Avenue
Poole
Dorset
BH13 6AF
GB','jojo_coral@hotmail.com','01202 766530' ), 
 ('564','48.00','2.50','','D','','1355150273','JULIE EDMUNDS
UTTINGS iNSURANCE BROKERS
16 THE FAIRLAND
HINGHAM
NORFOLK
NR9 4HN
GB','julie.edmunds@uttingsinsurance.co.uk','01953 850459' ), 
 ('565','26.00','2.50','','D','','1355245596','eliza baird baird
inverden
glenkindie  
alford
aberdeenshire
ab33 8sx
GB','lallah.baird@freeuk.com','01975641212' ), 
 ('566','22.00','2.50','','N','','1355258757','Lorna Turner
Apartment 17
12 Lakeside Rise
Blackley
Manchester
M9 8QD
GB','lornat74@hotmail.com','07845813344' ), 
 ('567','26.00','2.50','','D','','1355483176','Lee Tucker
9 Towergate Industrial Park
Colebrook Way
Andover
Hampshire
SP10 3BB
GB','lee@tuckerjoinery.co.uk','07775610801' ), 
 ('568','28.00','2.50','','D','','1355750112','Nikki Yarnall
10 Bishops Way
Whitwood
Castleford
West Yorkshire
WF10 5GJ
GB','nikki.yarnall@nmu.co.uk','07709353447' ), 
 ('569','22.00','2.50','','D','','1356605601','Leah  Jones
31 Melville Court
Goldhawk Road
London
London
W129NY
GB','leah.jones05@gmail.com','07792601143' ), 
 ('570','24.00','2.50','','D','','1357070897','Gemma Sanger
Angel cottage
3 angel lane
Ferndown
Dorset
Bh22 9dz
GB','Slinky145@hotmail.com','07788890227' ); 
 INSERT INTO `orders` (`id`, `total`, `deliveryttl`, `voucher`, `status`, `notes`, `date`, `delivery`, `email`, `phone` ) VALUES ('571','22.00','2.50','','D','','1357220670','Eleanor Eleanor
116 Reliance Way
Oxford
Oxfordshire
OX4 2FQ
GB','eleanor.calver@gmail.com','07879866773' ), 
 ('572','22.00','2.50','','D','','1357743986','Katherine Wholey
Tiss LTD 
330 Lytham road
Blackpool
Lancashire
FY4 1DW
GB','kath.boucher@tissltd.com','07983451243' ), 
 ('573','26.00','2.50','','D','','1358727010','Sarah Kirwin
17 Albert Road
Leeds
West Yorkshire 
LS27 8JX
GB','Sarah_kirwin@hotmail.com','07796162581' ), 
 ('574','22.00','2.50','','N','','1358882959','Lucy Cook
40 woodleigh
Thornbury
Bristol
Bs352jt
GB','Cookerlucy@hotmail.com','07738909136' ), 
 ('575','22.00','2.50','','D','','1358882961','Lucy Cook
40 woodleigh
Thornbury
Bristol
Bs352jt
GB','Cookerlucy@hotmail.com','07738909136' ), 
 ('576','84.00','2.50','','D','','1359139234','Holly O\'Brien
417 Le Capelain House
Castle Quay
St Helier
Jersey
JE2 3EA
GB','obrienholly@hotmail.co.uk','07797769444' ), 
 ('577','48.00','2.50','','N','','1359800517','Nicola Richardson
75/3 Montgomery St
Edinburgh
Mid Lothian
EH7 5HZ
GB','nico1010@hotmail.co.uk','07966220153' ), 
 ('578','48.00','2.50','','D','','1359800520','Nicola Richardson
75/3 Montgomery St
Edinburgh
Mid Lothian
EH7 5HZ
GB','nico1010@hotmail.co.uk','07966220153' ), 
 ('579','19.20','6.50','LINGERIEBLOG','D','','1360662259','Amanda Boteler
99 Robertson Quay
#05-11 Rivergate
Singapore
Singapore
238258
SG','amanda.boteler@gmail.com','006596693261' ), 
 ('580','44.00','2.50','STBRIDEZILLAS','D','','1360761783','Amanda Davis-Harrison
Bridezillas ltd
18 High Street, keynsham
Bristol
BANES
BS31 1DQ
GB','amanda@bridezillas.biz','01179043039' ), 
 ('581','26.00','2.50','','N','','1361315287','Irina Parker
Flat 22 Stockleigh Hall
51 Prince Albert Road
London
London
NW8 7LA
GB','ipbparker@gmail.com','07584014025' ), 
 ('582','26.00','2.50','','D','','1361315332','Irina Parker
Flat 22 Stockleigh Hall
51 Prince Albert Road
London
London
NW8 7LA
GB','ipbparker@gmail.com','07584014025' ), 
 ('583','24.00','2.50','','D','','1362567091','David Hpa
7 Mount Pleasant
South Ruislip
London
Middlesex
HA4 9HF
GB','David.hpa@ladbrokes.co.uk','07908816553' ), 
 ('584','28.00','2.50','','D','','1362738716','Ellen Lake
11 Longmead
Woolmer Green
Knebworth
Herts
SG3 6JH
GB','ellen.lake@btinternet.com','07412119188' ), 
 ('585','24.00','2.50','','N','','1363357229','Nicola Erswell
61 Northlea ave 
Bradford

Bd108lj
GB','Nicolaerswell@hotmail.com','07775535096' ), 
 ('586','24.00','2.50','','C','','1363357231','Nicola Erswell
61 Northlea ave 
Bradford

Bd108lj
GB','Nicolaerswell@hotmail.com','07775535096' ), 
 ('587','32.00','2.50','','D','','1363622827','Amanda  Bigault
St. Margarets
Bromley Green Road
Ashford
Kent
TN262EF
GB','amandajanefrances@hotmail.co.uk','07772280346' ), 
 ('588','136.00','2.50','','D','','1363770662','Amanda Watkins
26 highfield road
Caerleon 
Newport
Gwent
Np18 3du
GB','Dunnamanda01@gmail.com','01633430611' ), 
 ('589','28.80','2.50','PERFECT','D','','1363799489','THEOTHOULLA KOMODROMOU
46 ELMFIELD ROAD
POTTERS BAR
HERTS 
EN6 2JJ
GB','thea_ele@hotmail.com','07932528286' ), 
 ('590','34.00','2.50','','D','','1364304695','Isla Kelman
105 Kings Gate
Aberdeen

AB15 4EN
GB','Islak@hotmail.co.uk','07501009105' ), 
 ('591','26.00','2.50','','D','','1364932386','Anna Moore
Atkins
71 Old Channel Road
Belfast
Antrim
BT3 9DE
GB','Annabannanamoore@gmail.com','07958127853' ), 
 ('592','15.00','2.50','','D','','1365523506','Geraldine sheppard 
7 glen mill
rathfriland 
down
bt34 5fb
GB','gsheppard84@hotmail.com','07707948299' ), 
 ('593','26.00','2.50','','D','','1365885758','Emma Crosskey
12A Seaview Road
Woodingdean
Brighton
East Sussex
BN2 6DF
GB','crosskey_e@hotmail.com','07554664153' ), 
 ('594','34.00','2.50','','D','','1366097298','Virginia Green
53 Cecil Aldin Drive
Tilehurst
Reading
Berkshire
RG31 6YP
GB','sg53cadr@btinternet.com','447952242084' ), 
 ('595','15.00','2.50','','D','','1366102337','Ann Tuffy
8 Tavistock Road 
Watford
Hertfordshire 
WD24 4HL
GB','Ann.Tuffy@hotmail.co.uk','07853 164477' ), 
 ('596','32.00','2.50','','D','','1366111192','Jill Smith
30 Morecambe Road
Brighton
East Sussex
BN18TL
GB','jillsm30@gmail.com','01273884174' ), 
 ('597','30.00','2.50','','D','','1367095646','Sandra Harding
Parc Wain
Carloggas Farm Cottages. St,Mawgan
Newquay
Cornwall
TR8 4EQ
GB','sandrapharding@btinternet.com','91637 860416' ), 
 ('598','53.00','2.50','','D','','1367482734','Michele Guy
20 Burrell Road
Compton
Newbury
Berks
RG206NS
GB','shell_125@fsmail.net','01635578828' ), 
 ('599','32.00','2.50','','D','','1368204991','NICOLA WRIGHT
CARE OF 30A, MERERID MIN Y MOR
VICTORIA PARADE
PWLLHELI
GWYNEDD
LL535AN
GB','return2nicola@googlemail.com','13459254226' ), 
 ('600','12.00','2.50','TWITTER','D','','1369598746','Eimear McElroy
Ubiquitous Chip
8-12 Ashton Lane
Glasgow
Scotland
G12 8SJ
GB','eimearmcelroy@hotmail.co.uk','07766122924' ); 
 INSERT INTO `orders` (`id`, `total`, `deliveryttl`, `voucher`, `status`, `notes`, `date`, `delivery`, `email`, `phone` ) VALUES ('601','15.00','2.50','','N','','1369668113','Aoife Dooley
72 Old Broad Street
London
London
EC2M 1QT
GB','aoife.dooley@gmail.com','07540978804' ), 
 ('602','15.00','2.50','','D','','1369668631','Aoife Dooley
72 Old Broad Street
London
London
EC2M 1QT
GB','aoife.dooley@gmail.com','07540978804' ), 
 ('603','26.00','2.50','','D','','1369922976','Siobhan Cullen
132 St Marys Road
Tonbridge
Kent
TN9 2NN
GB','cullen_siobhan@hotmail.com','07852911208' ), 
 ('604','15.00','2.50','','D','','1371299752','Ulla Vitting Madsen
103 St George\'s Drive
Flat 5
London
Storbritannien
SW1V 4DA
GB','ullavitting@hotmail.com','07811360498' ), 
 ('605','26.00','2.50','','N','','1371732355','Katie Kean
41 Constantine Way
Hatch Warren 
Basingstoke
Hampshire
RG22 4UR
GB','katie.kean24@gmail.com','07808132648' ), 
 ('606','32.00','2.50','','D','','1371751166','Eleanor Saxon
14 North Pathway
Harborne
Birmingham
West Midlands
B17 9EJ
GB','Eas364@doctors.org.uk','07894442999' ), 
 ('607','32.00','2.50','','D','','1372162870','Lara Wright
Shirefields
Heddington
Calne
Wiltshire
SN11 0PQ
GB','lara.92@hotmail.co.uk','01380859224' ), 
 ('608','15.00','2.50','','D','','1372761627','Lindsey  Hendry
Telefonica
SkyPark 8 Elliott Place 
Glasgow

G3 8ep
GB','lindsey.hendry@o2.com','07916277047' ), 
 ('609','35.00','2.50','','D','','1373006222','Brooke Nightingale
57
Great Harry Drive
Eltham
London
Se9 3dd
GB','Brookenightingale@live.com','07525202692' ), 
 ('610','34.00','2.50','','D','','1373291369','Rachael Lobb
Flat 2 Temple House
6 Temple Avenue
London
London
EC4Y 0DF
GB','rlobb@deloitte.co.uk','07500221214' ), 
 ('611','25.00','2.50','','D','','1373465037','sally bayliss
21 Hawkside
Wilnecote
Tamworth
Staffordshire
B77 4HW
GB','sally.bayliss@nextiraone.co.uk','07722 666730' ), 
 ('612','26.00','2.50','','D','','1374133469','sophie Evans
C/O Caroline Cook
UK Mission enterprise LTD
15 17 Harriet Walk
London
SW1X 9JQ
GB','sophieevans2001@yahoo.co.uk','00971561330124' ), 
 ('613','34.00','2.50','','D','','1374484946','Sue  Wright
17 Orchard Rise
Fivehead
Taunton
Somerset
TA3 6PB
GB','suewright77@btinternet.com','01460 281437' ), 
 ('614','56.00','2.50','','N','','1374571611','nichola Hollands
20 cobbetts Mead 
Haywards Heath
West Sissex
RH16 3TQ
GB','nichola@motivate.ae','01444 455257' ), 
 ('615','25.00','2.50','','N','','1374621176','Nicci Kilbride
1/1 49 Rannoch street
Cathcart
Glasgow
G44 4DD
GB','Niccikilbride@hotmail.co.uk','07738553878' ), 
 ('616','25.00','2.50','','N','','1374622813','Nicci Kilbride
1/1 49 Rannoch street
Cathcart
Glasgow
G44 4DD
GB','Niccikilbride@hotmail.co.uk','07738553878' ), 
 ('617','25.00','2.50','','D','','1374695682','Nicci Kilbride
1/1 49 Rannoch Street
Cathcart
Glasgow
G44 4DD
GB','Niccikilbride@hotmail.co.uk','07738553878' ), 
 ('618','45.00','2.50','','N','','1374700076','Katrina Coules
61 Patrick coman house
Skinner st
London
London
Ec1v4ny
GB','Katrinacoules@hotmail.co.uk','07943144203' ), 
 ('619','15.00','2.50','','D','','1374940362','Johanna Graham
30 School Road
Manchester
Lancashire
M32 8DH
GB','johannawroe@hotmail.com','07733331441' ), 
 ('620','34.00','2.50','','D','','1374996042','Nicci Kilbride
1/1 49 Rannoch Street
Cathcart
Glasgow
G44 4DD
GB','Niccikilbride@hotmail.co.uk','07738553878' ), 
 ('621','34.00','2.50','','D','','1375274409','Dawn  Lee
59 Wenvoe Terrace
Barry
SOUTH GLAMORGAN
CF62 7ET
GB','dawn.lee@live.co.uk','447572451574' ), 
 ('622','15.00','2.50','','D','','1375305193','Leanne Jones
170 Tranmere Road
London
United Kingdom
SW18 3QU
GB','leejones07@gmail.com','447825928144' ), 
 ('623','95.76','2.50','STJENNY','D','','1375355924','Jenny Marie
28 Bridge Road
East Molesey
Surrey
KT8 9HA
GB','jennifer@jennymarie.co.uk','07817705824' ), 
 ('624','32.00','2.50','','D','','1375786515','Matt Dykes
40 Swan Street
Kingsclere
Berkshire
RG20 5PL
GB','maxdykes@gmail.com','07876575570' ), 
 ('625','56.00','2.50','','D','','1376063170','Kerry Attenborrow
24 linwood close
Hinckley
Leics
Le10 0xg
GB','kerryattenborrow@hotmail.co.uk','07969461528' ), 
 ('626','32.00','6.50','','D','','1376104648','Dena Causey
18 Adams Ave
Berlin
Camden
08009
US','dcausey1@comcast.net','856-723-4284' ), 
 ('627','26.00','2.50','','D','','1376209634','Debera Clark
The Lookout,
95 Satchell Lane
Hamble-le-Rice
Hampshire
SO31 4HL
GB','debera.kett@hotmail.co.uk','07956866681' ), 
 ('628','34.00','2.50','','D','','1376247488','June  Young
23 Brighton Road
Aldershot
Hants
GU12 4HG
GB','angela.myles@ntlworld.com','01252316479' ), 
 ('629','15.00','2.50','','D','','1376296083','Robyn  Wilkin
Loomis Sayles Investments Ltd
25 St. James\'s Street, The Economist Plaza
London
London
 SW1A 1HG
GB','robynwilkin@yahoo.co.uk','07590750826' ), 
 ('630','12.00','2.50','TWITTER','D','','1376940680','Aleesha Jones
16 briardale 
Wimbledon park road
London
London 
Sw196pf
GB','aleesha.jones@bensonradiology.com.au','07778854809' ); 
 INSERT INTO `orders` (`id`, `total`, `deliveryttl`, `voucher`, `status`, `notes`, `date`, `delivery`, `email`, `phone` ) VALUES ('631','38.00','2.50','','D','','1377368491','Jennifer Wright
71a Alexandra Avenue
Mansfield
Notts
NG185AD
GB','jennyshaynes@googlemail.com','07813774028' ), 
 ('632','35.00','2.50','','D','','1377640086','Katrina  Waters
Apartment 193 Metro Central Heights 
119 Newington Causeway
London
London
SE1 6BW
GB','triniwat@hotmail.com','07950821293' ), 
 ('633','15.00','2.50','','D','','1378209111','Peter  Huntly
9, Gilbert Close
Rayleigh
Essex
SS6 8QR
GB','pa.huntly@btinternet.com','01268 777398' ), 
 ('634','15.00','2.50','','N','','1378661353','Gavin Wall
30 fenchurch street
Ascot underwriting
London 
London
Ec3m 3bd
GB','Gavin.wall@ascotuw.com','07815025292' ), 
 ('635','38.00','2.50','','D','','1378992373','Laura Dixon-Burt
24 Rowleys Mill
Uttoxeter New Road
Derby
Derbyshire
DE223TJ
GB','lauradixon8@hotmail.co.uk','07769348501' ), 
 ('636','34.00','2.50','','D','','1379515231','Angie Smith
6 Carr view
South kirkby
Pontefract
West yorkshire
Wf93bx
GB','Iancampbellsmith59@yahoo.co.uk','01977609172' ), 
 ('637','32.00','2.50','','N','','1379849135','Alison Howarth
1 Derwent Drive
Adel
Leeds
Yorkshire
LS16 8JD
GB','ahowarth@hotmail.com','01132612569' ), 
 ('638','15.00','2.50','','N','','1379866867','Christine Kennedy
31 Kirkview
Glasgow
North Lanarkshire
G67 4EH
GB','christinekennedy18@gmail.com','01236 614533' ), 
 ('639','15.00','2.50','','N','','1379866871','Christine Kennedy
31 Kirkview
Glasgow
North Lanarkshire
G67 4EH
GB','christinekennedy18@gmail.com','01236 614533' ), 
 ('640','15.00','2.50','','N','','1379866871','Christine Kennedy
31 Kirkview
Glasgow
North Lanarkshire
G67 4EH
GB','christinekennedy18@gmail.com','01236 614533' ), 
 ('641','32.00','2.50','','D','','1379947168','Rosemary Hilton
Red House Farm
Baston Fen
Peterborough
Cambs
PE6 9PT
GB','Rosie54@outlook.com','07876 190000' ), 
 ('642','15.00','2.50','','D','','1380551868','Michelle Quinn
5 Sharket Head Close
Queensbury
Bradford
West Yorkshire
BD13 1PD
GB','shellyquinn6@hotmail.com','07428716242' ), 
 ('643','26.00','2.50','','D','','1380629052','Abigail Thomas
12
Princethorpe Road
Ipswich
Suffolk
IP38NY
GB','abst5@hotmail.com','07779617530' ), 
 ('644','92.00','2.50','','D','','1380787816','Lesley Sharon Wood
Kitten Hollow 3 Vine Cottages
Gwaelod Y Garth
Cardiff
Caerdydd
CF15 9HQ
GB','Lesleyswood@gmail.com','07775575805' ), 
 ('645','32.00','2.50','','N','','1380874530','maria smith
the old station house station road 
tolleshunt darcy
essex
essex
cm98tj
GB','mariageordie@gmail.com','01621869129' ), 
 ('646','86.64','2.50','STVICKI','N','','1380897994','Vicki Hancox
26 Hunnington Crescent
Halesowen
West Midlands
B63 3DJ
GB','vicki_hancox@hotmail.com','07980 974409' ), 
 ('647','86.64','2.50','STVICKI','D','','1380898164','Vicki Hancox
26 Hunnington Crescent
Halesowen
West Midlands
B63 3DJ
GB','vicki_hancox@hotmail.com','07980 974409' ), 
 ('648','25.00','2.50','','N','','1381864953','Simon  Joyce 
Jewson Ltd 
79 RIngwood Road 
Parkstone 
Dorset 
BH14 0RA 
GB','simonjoyce2002@yahoo.co.uk','07976870669' ), 
 ('649','25.00','2.50','','D','','1381864968','Simon  Joyce 
Jewson Ltd 
79 RIngwood Road 
Parkstone 
Dorset 
BH14 0RA 
GB','simonjoyce2002@yahoo.co.uk','07976870669' ), 
 ('650','15.00','2.50','','D','','1382685049','Judith Air
25 Tindale Avenue
Cramlington
Northumberland
Ne23 2BP
GB','judithgeejudith@gmail.com','07889090814' ), 
 ('651','15.00','2.50','','N','','1383410376','Emily Christie
91 Raemoir Avenue
Banchory
Aberdeenshire
AB315UE
GB','emily.christie@hotmail.co.uk','01330820683' ), 
 ('652','25.00','2.50','','N','','1383596739','Luciana Custodio NOOK Services
London Business School (House 42 - Business Incubator)
 Sussex Pl, Regents Park
London
London
NW1 4SA
GB','luciana.custodio@gmail.com','0034626850869' ), 
 ('653','32.00','2.50','','D','','1383940447','William Woodhead
c/o BST Transport & Warehousing Ltd, 
Unit 4, 6 John Brannan Way, Darrows Industrial Estate, 
Bellshill
North Lanarkshire
ML4 3HD
GB','planet_shed@mac.com','447785512717' ), 
 ('654','110.00','6.50','','D','','1385632900','Luciana Custodio
Start2bee (local amarillo)
Carrer de l\'Escorial 180
Barcelona

08024
ES','luciana.custodio@gmail.com','+34626850869' ), 
 ('655','25.00','2.50','','D','','1385923547','Jacquie Cooper
192  The Hill
Glapwell
Chesterfield
Derbys
S44 5NB
GB','Supercooperz@aol.com','01623 810434' ), 
 ('656','26.00','2.50','','D','','1386014267','Naomi Tucker
63 Bartlett Street
Caerphilly
Caerphilly
CF83 1JT
GB','naomitucker1987@hotmail.co.uk','07539834976' ), 
 ('657','15.00','2.50','','D','','1386018158','Luciana Custodio
60 London Rd‎
Milton Keynes
Buckinghamshire
MK5 8AQ
GB','luciana.custodio@gmail.com','+447523638860/+34626850869' ), 
 ('658','30.00','2.50','','N','','1386770745','Nicki Cavanagh
Greenhurst
Bracknell Lane
Hartley Wintney, Hook
Hampshire
RG27 8QQ
GB','nicki.cavanagh@hotmail.co.uk','447926603987' ), 
 ('659','30.00','2.50','','D','','1386772971','Nicki Cavanagh
Greenhurst
Bracknell Lane
Hartley Wintney, Hook
Hampshire
RG27 8QQ
GB','nicki.cavanagh@hotmail.co.uk','447926603987' ), 
 ('660','26.00','2.50','','D','','1387189092','Gill Gillespie
1 Low Wood Court
Utley
Keighley
West Yorkshire
BD20 6DG
GB','gill.gillespie.saunders@gmail.com','gill.gillespie.saunders@gmail.' ); 
 INSERT INTO `orders` (`id`, `total`, `deliveryttl`, `voucher`, `status`, `notes`, `date`, `delivery`, `email`, `phone` ) VALUES ('661','20.00','2.50','TWITTER','D','','1387554885','Rebecca Entwistle
4 Baronscourt Road
Edinburgh

EH8 7ET
GB','bjentwistle@hotmail.com','07899083141' ), 
 ('662','25.00','2.50','','D','','1388587137','Steve Garner
5 Hartland Drive
Sothall
Sheffield
South Yorkshire
S20 2QD
GB','steve.garner@yahoo.co.uk','07800500414' ), 
 ('663','100.00','2.50','','D','','1388672124','Kelly Zaremba
9 Cooperative Terrace
Coxhoe
Durham
Co. Durham
DH6 4DQ
GB','kellyzaremba87@googlemail.com','441915979096' ), 
 ('664','38.00','2.50','','D','','1389205710','Yaalini Arumuham
38D Yonge Park
Finsbury Park
London
Greater London
N4 3NT
GB','catchup@talk21.com','07583 968471' ), 
 ('665','15.00','2.50','','D','','1390397090','Lana Ojjeh Lette
Flat 20
10-14 Old Church Street
London
London
SW3 5DQ
GB','lanaojjeh@gmail.com','07736070091' ), 
 ('666','38.00','2.50','','D','','1390419474','Nicholas Hughes
5 Haycroft Close
Mansfield Woodhouse
Mansfield 
Notts
NG199SJ
GB','nh2575@googlemail.com','07794991485' ), 
 ('667','32.00','2.50','','N','','1391077829','Charlotte Hawkins
3A Ockendon Road
upminster
essex
rm14 2dn
GB','charlotte.hawkins@live.co.uk','07854401482' ), 
 ('668','15.00','2.50','','D','','1392471812','samantha goldby
13c wilton gardens
west molesey
surrey
kt8 1qp
GB','samanthagoldby@hotmail.com','07788665714' ), 
 ('669','45.00','2.50','','N','','1392896364','asta saltyte
preshaven sands 
prestatyn 

ll199tt
GB','drekute@one.lt','07788243039' ), 
 ('670','38.00','2.50','','D','','1393061684','denise oatridge
25 the nurseries
coven
wolverhampton
west midlands
wv9 5bz
GB','daveoutrage@aol.com','07904658950' ), 
 ('671','20.00','2.50','PERFECT','D','','1393248768','David  Simpson
6 back lane 
Henley in arden 
Warwickshire
B95 5SS 
GB','Djpeach1984@googlemail.com','07733686465' ), 
 ('672','26.00','2.50','','N','','1393252562','GLYNIS LYTTLE
11 ASHGREEN
ANTRIM
ANTRIM
BT41 1HL
GB','glynis@cmcair.co.uk','07971614907' ), 
 ('673','26.00','2.50','','N','','1393254644','GLYNIS LYTTLE
11 ASHGREEN
ANTRIM
ANTRIM
BT41 1HL
GB','glynis@cmcair.co.uk','07971614907' ), 
 ('674','26.00','2.50','','N','','1393275339','Glynis Lyttle
11ashgreen
Antrim
Antrim
Bt411hl
GB','Glynis@cmcair.co.uk','07971614907' ), 
 ('675','26.00','2.50','','N','','1393275435','Zara Lyttle
11ashgreen
Antrim
Antrim
Bt411hl
GB','Zara_lyttle@hotmail.com','07814894493' ), 
 ('676','25.00','2.50','','D','','1394727925','Barbara Coleman
32 New Road
Langley
Slough
Berksire
SL3 8JJ
GB','amanda.coleman5@outlook.com','01753 340039' ), 
 ('677','25.00','2.50','','D','','1395918342','Daniel Cole
249 Fawcett Road
Southsea
PORTSMOUTH
HAMPSHIRE
PO4 0LB
GB','sonic.dan@hotmail.co.uk','07949462306' ), 
 ('678','15.00','2.50','','D','','1396004721','Kate Jones
c/o 33 Curtis Wood Park Road
Herne
Herne Bay
Kent
CT6 7TY
GB','kate.baybutt@hotmail.co.uk','07834571206' ), 
 ('679','25.00','2.50','','D','','1396381695','Kay Townsend
159 Loughborough Road
West Bridgford
Nottingham
Nottinghamshire
NG2 7JS
GB','kay-townsend@sky.com','447779011226' ), 
 ('680','25.00','2.50','','D','','1396527147','Julie Wilkinson
27 Fen Street
Nayland
Colchester
Essex
CO64HT
GB','julie.wilkinson36@btinternet.com','01206262678' ), 
 ('681','28.00','2.50','','D','','1396610042','James Thurtle
epyx Limited, Heath Farm, Hampton Lane, 
Meriden
Coventry
CV7 7LL
GB','jamesthurtle@hotmail.co.uk','07595057155' ), 
 ('682','32.00','2.50','','D','','1397119832','Sian Rees
4 rock wood rd
Plymouth
England
Pl6 7se
GB','Sianrees@fsmail.net','07709949688' ), 
 ('683','25.00','2.50','','D','','1397208513','DEBBIE HODGETTS
176 TESSALL LANE
NORTHFIELD
BIRMINGHAM
WEST MIDLANDS
B31 5EB
GB','debbiejo123@hotmail.com','0121 477 7551' ), 
 ('684','26.00','2.50','','D','','1397244263','Jacquie Cooper
192The Hill
Glapwell
Chesterfield 
Derbys
S44 5NB
GB','supercooperz@aol.com','01623810434' ), 
 ('685','15.00','2.50','','D','','1398693341','Kelly Buckley
35 Clos Hector
Cardiff
Not Applicable
CF24 2HL
GB','kellybuckley82@hotmail.co.uk','07500068476' ), 
 ('686','25.60','2.50','TWITTER','D','','1399288400','Nicola Lewington
77 Cedar Crescent 
North Baddsley
Southampton
Hanpshire
SO52 9Fx
GB','nlewington@hotmail.com','07912431421' ), 
 ('687','40.00','2.50','','D','','1399930999','Janine Dominy
3 Lawns Close
Wimborne
Dorset
BH21 2JR
GB','email@janinedominy.fsnet.co.uk','07788538588' ), 
 ('688','34.00','2.50','','N','','1400062082','Lena  Emery
29 Corsham Street
London 

N1 6DR 
GB','emerylena@gmail.com','07879927408' ), 
 ('689','40.00','2.50','','D','','1400169064','melissa banks
19 magdalene st
glastonbury
somerset
ba6 9ew
GB','bocabar@googlemail.com','07745857057' ), 
 ('690','34.00','2.50','','N','','1400240662','Naomi  Mwasambili
32 Horsely Court
4 Candle Street
Lodnon
London
E1 4RX
GB','namwasam@hotmail.co.uk','07411128245' ); 
 INSERT INTO `orders` (`id`, `total`, `deliveryttl`, `voucher`, `status`, `notes`, `date`, `delivery`, `email`, `phone` ) VALUES ('691','34.00','2.50','','D','','1400316308','Mo  Waters
16 Kingfisher Drive
Boulters Meadow
Maidenhead
Berkshire
WL6 8FH
GB','mowaters25@gmail.com','07795 956281' ), 
 ('692','30.40','2.50','STBRIDALROOM','D','','1400669873','Emma  Lloyd 
The Bridal Room Broadway
6 Kelil Close - High St 
Broadway
worcs
WR12 7DP
GB','info@thebridalroombroadway.co.uk','441386859070' ), 
 ('693','34.00','2.50','','D','','1400759998','Sharon Colley
4,Oakfield,
Plaistow
West Suusex
RH14 OQD
GB','dianavenison@aol.com','01403871288' ), 
 ('694','40.00','2.50','','N','','1401271247','Claire Dunn
1 Iona Close 
Hexham Road
Morden
Surrey
SM4 6HR
GB','clairedunn07@hotmail.co.uk','07788130209' ), 
 ('695','40.00','2.50','','D','','1401352774','RACHEL HUNTER
MEAD HOUSE MEAD ROAD
STOKE GIFFORD
BRISTOL
BRISTOL
BS34 8PS
GB','rachelhunter82@gmail.com','447967076119' ), 
 ('696','25.00','2.50','','D','','1401553198','Blanche Soulsby
72 cranham gardens
Upminster
Essex
Rm141jq
GB','blanchesoulsby@gmail.com','07944143866' ), 
 ('697','110.20','2.50','STSTATELY','D','','1401876269','Leanne Whysall
1 Leighton Way 
Belper
Derbyshire 
DE56 1SX
GB','Enquiries@statelybrides.co.uk','01773826126' ), 
 ('698','34.00','2.50','','D','','1402542405','Naomi Mwasambili
32 Horsley Court
4 Candle Street
London
london
E1 4RX
GB','namwasam@hotmail.co.uk','07411128245' ), 
 ('699','28.00','2.50','','N','','1402579634','Kim  Pickard
17 Lyric Road
Barnes
London

sw13 9qa
GB','eloisepickard@gmail.com','+61447025933' ), 
 ('700','34.00','2.50','','D','','1402740066','Mrs Amanda Cherry
2 Ash Drive 
Wolfreton Lane 
Hull
East Yourshire
HU10 6PR
GB','Mandy@chezz.karoo.co.uk','01482 652228' ), 
 ('701','18.00','2.50','','D','','1403019268','Rebecca Willcocks
58 Old Compton Street
London
London
W1D 4UF
GB','rebecca.willcocks@primefocusworld.com','07740683279' ), 
 ('702','18.00','2.50','','D','','1403019273','Rebecca Willcocks
58 Old Compton Street
London
London
W1D 4UF
GB','rebecca.willcocks@primefocusworld.com','07740683279' ), 
 ('703','34.00','2.50','','D','','1404576088','Barbara Hall
3 Oast View 
Horsmonden
Tonbridge
Kent
TN12 8LE
GB','Halltribe@aol.com','01892 723821' ), 
 ('704','25.00','2.50','','D','','1404730894','Andrew Lyons
58 The Hollows
Gilford road
Lurgan
Armagh
BT667FF
GB','arjlyons@gmail.com','07909768838' ), 
 ('705','34.00','2.50','','N','','1404899453','Emma  Goodbier
8 Longsight avenue
Clitheroe 
Lancashire
Bb7 2an
GB','Egoodbier@outlook.com','07590121803' ), 
 ('706','34.00','2.50','','C','','1404899456','Emma  Goodbier
8 Longsight avenue
Clitheroe 
Lancashire
Bb7 2an
GB','Egoodbier@outlook.com','07590121803' ), 
 ('707','40.00','2.50','','D','','1405334477','Jillian Barnes
Brunthill Farm
Kilmarnock
East ayrshire
KA3 6HX
GB','jillianfbarnes@hotmail.com','07585977859' ), 
 ('708','25.00','2.50','','D','','1405423055','Steven Galvin
79 Wilton Rd
Upper Shirley
southampton
Hampshire
SO15 5JJ
GB','glenveaghelectrical@googlemail.com','447900068846' ), 
 ('709','20.00','2.50','PERFECT','D','','1405773766','kerry Lowe
28 Harriett Street
Stapleford

Ng98fg
GB','kerry.lowe@me.com','07907020652' ), 
 ('710','40.00','2.50','','D','','1405793432','Gayle  Jobson
18 Birch Avenue
Heworth
Gateshead
Tyne & Wear
NE10 8UX
GB','gaylejobson8@hotmail.com','07958394041' ), 
 ('711','40.00','2.50','','N','','1406113571','alexis snowdon
44, Pole Hill Road
Chingford
London

E4 7LZ
GB','alexis.snowdon@edfenergy.com','0781 412 8284' ), 
 ('712','28.00','2.50','','N','','1406127123','Lauren McConville
19/7 Comely Bank Road
Edinburgh
Edinburgh
EH4 1DS
GB','l.mcconville@hotmail.co.uk','07946638319' ), 
 ('713','28.00','2.50','','D','','1406128630','Lauren McConville
19/7 Comely Bank Road
Edinburgh
Edinburgh
EH4 1DS
GB','l.mcconville@hotmail.co.uk','07946638319' ), 
 ('714','40.00','2.50','','D','','1406281448','alexis snowdon
44, Pole Hill Road
chingford
London

E4 7LZ
GB','alexis.snowdon@edfenergy.com','0208 524 3802' ), 
 ('715','34.00','2.50','','D','','1406550978','Catherine Sheridan
116 Pitfold Road
Lee
London
SE12 9Hy
GB','catherine.sheridan2@ntlworld.com','07940464979' ), 
 ('716','25.00','2.50','','D','','1407504798','Patricia Pratas
Sprüth Magers Ltd
7a Grafton Street
London

W1S 4EJ
GB','pp@spruethmagers.com','00 44 20 7408 1613' ), 
 ('717','18.00','2.50','','D','','1407612326','Ellie Burns
56 Blundell Avenue
Hightown
Merseyside
L38 9EY
GB','elliecarole93@outlook.com','07580516770' ), 
 ('718','76.76','2.50','STBRIDEZILLAS','N','','1407951416','AMANDA DAVIS-HARRISON
BRIDEZILLAS
18 HIGH STREET
KEYNSHAM BRISTOL
BANES
BS31 1DQ
GB','AMANDA@BRIDEZILLAS.BIZ','0117 9043039' ), 
 ('719','65.00','2.50','','D','','1408528095','Dopreen Smith
The Exchange, 240 Wishaw Road, Waterloo, Wishaw
ML2 8EZ
Wishaw
Lanarkshire
ML2 8EZ
GB','doreensmith21@gmail.com','07971615144' ), 
 ('720','22.40','2.50','TWITTER','D','','1408707273','georgina Fearns
51 Lincoln Road
London
london
N2 9DJ
GB','georgiefearns@hotmail.co.uk','07711587626' ); 
 INSERT INTO `orders` (`id`, `total`, `deliveryttl`, `voucher`, `status`, `notes`, `date`, `delivery`, `email`, `phone` ) VALUES ('721','18.00','2.50','','D','','1410114971','judith mcclelland
80 Cascum Road
Banbridge

BT32 4LE
GB','jude.eleanor@live.co.uk','447834873601' ), 
 ('722','125.00','2.50','','N','','1410540525','Dianne Pearson
38 Mayfield Road, 
Sutton
Surrey
SM2 5DT
GB','diannepearson@blueyonder.co.uk','02086438680' ), 
 ('723','88.00','2.50','','D','','1410787216','Dianne Pearson
38 Mayfield Road,
Sutton
Surrey
SM2 5DT
GB','diannepearson@blueyonder.co.uk','02086438680' ), 
 ('724','40.00','2.50','','D','','1410865044','Christopher  Davies
Mere Dentistry
Dutchy Manor, Springfield Road
Mere
Wiltshire
BA12 6EW
GB','ccmdavies@hotmail.com','07792073312' ), 
 ('725','25.00','2.50','','N','','1411144437','Julie Bacon
39a Ventnor villas
Hove
East Sussex
Bn3 3da
GB','Juliepbacon@hotmail.co.uk','07500082117' ), 
 ('726','25.00','2.50','','D','','1411144438','Julie Bacon
39a Ventnor villas
Hove
East Sussex
Bn3 3da
GB','Juliepbacon@hotmail.co.uk','07500082117' ), 
 ('727','22.80','2.50','STBRIDEZILLAS','D','','1412614018','AMANDA DAVIS-HARRISON
BRIDEZILLAS
10 SANDY PARK ROAD, BRISLINGTON
BRISTOL

BS4 3PE
GB','AMANDA@BRIDEZILLAS.BIZ','0117 9043039' ), 
 ('728','28.00','2.50','','D','','1412786824','Nicola  Pollitt
109 Mill Fold Road
Middleton
Greater Manchester
M24 1DF
GB','nicpol1979@aol.com','07929311738' ), 
 ('729','28.00','2.50','','D','','1413473425','SHIRLEY MOORE
STRATHEARN
90, CULDUTHEL PARK
INVERNESS
HIGHLANDS
IV2 4RZ
GB','shirley.moore000@btinternet.com','01463 225884' ), 
 ('730','28.00','2.50','','D','','1413473436','SHIRLEY MOORE
STRATHEARN
90, CULDUTHEL PARK
INVERNESS
HIGHLANDS
IV2 4RZ
GB','shirley.moore000@btinternet.com','01463 225884' ), 
 ('731','18.00','2.50','','D','','1413637682','Teri Hardman
25
Green Lane
Doncaster
South Yorkshire
DN6 7NG
GB','terihardman@googlemail.com','07999014341' ), 
 ('732','18.00','2.50','','D','','1413994629','Tanya Newcombe
4 Argyll Way
Stamford
Lincs
PE92XQ
GB','alanwalters@live.com','07540816134' ), 
 ('733','18.00','2.50','','D','','1413994636','Tanya Newcombe
4 Argyll Way
Stamford
Lincs
PE92XQ
GB','alanwalters@live.com','07540816134' ); 
 -- Struktur Tabel page_blocks -------------------- 
CREATE TABLE IF NOT EXISTS `page_blocks` ( 
 `id` int(11) NOT NULL auto_increment, 
`p_id` int(11) NOT NULL , 
`title` varchar(200) NOT NULL , 
`content` text NOT NULL , 
`viewable` set('Y','N') NOT NULL  , 
PRIMARY KEY (`id`), 
KEY `viewable` (`viewable`), 
KEY `p_id` (`p_id`) 
 ); 
-- Data Tabel page_blocks -------------------- 
INSERT INTO `page_blocks` (`id`, `p_id`, `title`, `content`, `viewable` ) VALUES ('74','11','The Village Boutique','83 High Street, Kinver, West Midlands, DY7 6HD<div><br></div><div>01384 877122</div><div><br></div><div><a href=\"http://contact@thevillageboutiqueltd.com\" title=\"\" target=\"_blank\">contact@thevillageboutiqueltd.com</a></div><div><br></div><div><a href=\"http://www.thevillageboutiqueltd.com\" title=\"\" target=\"_blank\">www.thevillageboutiqueltd.com</a></div>','Y' ), 
 ('14','5','Ivory Bride','<P>A fantastic combination of lace and silk makes for the most luxurious wedding lingerie!</P>','Y' ), 
 ('77','5','Amour Des Fleurs','<img src=\"http://i.imgur.com/u2qZfy4.jpg\" width=\"825\"><br>','Y' ), 
 ('78','5','Art Deco','I love Art Deco engagement rings. This one was the inspiration behind our new design Lana.<div><img src=\"http://i.imgur.com/Dv539w1.jpg\" width=\"825\"><br></div>','Y' ), 
 ('22','11','The Harrogate Wedding Lounge','<P>38 Forest Head Lane, Harrogate, North Yorkshire, HG2 7TF</P>
<P>01423 888242</P>
<P><A href=\"mailto:info@theharrogateweddinglounge.com\">info@theharrogateweddinglounge.com</A></P>
<P><A href=\"http://www.theharrogateweddinglounge.com\">www.theharrogateweddinglounge.com</A> <BR></P>','Y' ), 
 ('15','4','Wedding & Lifestyle - 16th July 2009','<P style=\"FONT-WEIGHT: bold\">Brilliant Bridal Garters</P>
<P>A stylish bridal garter service for brides looking for something different is exactly what Laura George has recently established. Laura’s designs are elegant, modern and glamorous and are crafted using silks, tulles and antique-style laces. They’re also embellished with satin and organza bows, diamantes, pearls, beading and swarovski crystals.<BR><BR>These luxurious garters are a wonderful compliment to a bride’s wedding gown and lingerie and rather than a one-size-fits-all approach Laura offers a full range of sizes. To coincide with her launch, Laura George is including a second garter for the traditional garter toss, absolutely free. The original garter is beautifully packaged and can be kept as a memorable keepsake.</P>
<P>To view this article please go to <A href=\"http://www.weddingandlifestyle.co.uk/guide/\">www.weddingandlifestyle.co.uk/guide/</A></P>','Y' ), 
 ('31','11','Veils of Berkeley','<P>2a High Street, Berkeley, Gloucestershire, GL13 9BJ</P>
<P>01453 810070</P>
<P><A href=\"mailto:veilsofberkeley@btconnect.com\">veilsofberkeley@btconnect.com</A></P>
<P><A href=\"http://www.veilsofberkeley.co.uk\">www.veilsofberkeley.co.uk</A><BR></P>','Y' ), 
 ('23','4','lingerieblog - 25th November 2009','<H1>Luxury Garters from Laura George</H1>
<DIV class=date>
<P>November 25, 2009 by <A title=\"Posts by Jon\" href=\"http://www.lingerieblog.co.uk/?author=2\">Jon</A>&nbsp;</P></DIV>
<P>There’s new name in luxury garters.</P>
<P>Laura George is a designer who believes in creating exquisite bridal garters for every brides wedding day and night. Her designs are unique and only use the finest silks, soft tulles and antique style laces.&nbsp; </P>
<P>Laura told Lingereblog.co.uk, “I hand make all my designs from only the finest quality of fabrics such as silks soft tulles and antique style laces. Garters are then embellished using Swarovski crystals and pearls, hand tied bows and beading. ”</P>
<P>As well as her collection of bridal pieces, Laura offers a bespoke service for those wanting something different and endeavours to help brides achieve their dreams. </P>
<P>Garters retail from £22 – £28</P>
<P>To view this article please go to <A href=\"http://www.lingerieblog.co.uk\">www.lingerieblog.co.uk</A></P>','Y' ), 
 ('26','4','Breasttalk - 5th December 2009','<P><SPAN class=normaltxt>Laura George is a designer who believes in creating exquisite bridal garters for every brides wedding day and night. She understands how important it is for every detail to be perfect on your big day. Her designs are unique, elegant and modern with a touch of glamour.<BR><BR>Products are manufactured using the highest quality materials and only the finest silks, soft tulles and antique style laces are used. Each garter is individually hand made in the UK. </SPAN></P>
<P><SPAN class=normaltxt>To view this article please go to <A href=\"http://www.breasttalk.co.uk/products/laura_george.aspx\">www.breasttalk.co.uk/products/laura_george.aspx</A><BR><BR></SPAN><BR></P>','Y' ), 
 ('27','4','lingerieblog - 18th December 2009','<H1>Laura George Garters – Reviewed</H1>
<DIV class=date>
<P>December 18, 2009 by <A title=\"Posts by Carol\" href=\"http://www.lingerieblog.co.uk/?author=1\"><FONT color=#2255aa>Carol</FONT></A>&nbsp;</P></DIV>
<P>Whether its a summer or winter wedding or you want a garter for a hen night or party, <A href=\"http://www.laurageorge.co.uk/\" target=_blank><FONT color=#2255aa>Laura George Garters</FONT></A> are perfect! </P>
<P>You’ll find lots of colours and styles to choose from and they even come in different sizes so you won’t have to keep adjusting one that’s too tight or uncomfortable. Laura’s Garters are available in sizes XS – XL.</P>
<P>Laura told Lingerieblog, “All my designs are handmade using only the finest quality of fabrics such as silks, soft tulles and antique style laces. Embellishing designs with Swarovski crystals and pears, hand tied bows and beading”.&nbsp; </P>
<P><A href=\"http://www.laurageorge.co.uk/product.html?id=6\" target=_blank><FONT color=#2255aa>Coco</FONT></A> is a very traditional style bridal garter and has the whole ‘something blue’ trim to it, and of course its white and frilly and looks absolutely perfect to wear with your wedding stockings under your dress!</P>
<P>The Lace and other fabrics are extremely soft and feminine and perfect for a traditional White wedding or a more modern wedding. Traditions are something to keep going so try to stick to the ‘something new’ tradition etc. </P>
<P>If it’s something for the bedroom, hen night or honeymoon you want then why not check out the wide selection of colours and styles by <A href=\"http://www.laurageorge.co.uk/\" target=_blank><FONT color=#2255aa>Laura George</FONT></A>.&nbsp; </P>
<P>All Laura George garters have plush backed elastic and come packaged in a delicate organza bag<BR></P>','Y' ), 
 ('28','4','bridalbuyer.com - 28th January 2010','<BR>
<DIV class=standfirst>
<P>Laura George is a designer who creates exquisite bridal garters where every detail is important and the end result is heirloom-worthy</P></DIV>
<P>
<P>Laura George’s work is that perfect mix of elegance, modernity and glamour. Her garters are of the highest quality and in the finest silks, soft tulles and antique-style laces, embellished with Swarovksi crystals and pearls, hand-tied bows and beading. All products use plush-backed elastic for added comfort and are individually hand made in the UK, and packaged in beautiful organza bags. Retail prices are between £22 and £28. Laura has a degree in Contour Fashion and over five years of industry experience designing lingerie for many high street retailers and International brands.&nbsp;After successfully working her way to the top of her career as Senior Lingerie Designer, she decided it was time to set up her eponymous brand. She also offers a bespoke service for those wanting something different.</P><SPAN lang=EN>
<P></SPAN><A href=\"http://www.bridalbuyer.com/story.aspx?storycode=6508283\"><U><FONT color=#0000ff size=2><FONT color=#0000ff size=2><SPAN lang=EN>http://www.bridalbuyer.com/story.aspx?storycode=6508283</U></FONT></FONT></SPAN></A></P>','Y' ), 
 ('56','6','Andrew and Loraine','Congratulations to Andrew and Loraine who married June 2010. Loraine chose to wear Lucy Limited Edition garter design, she was really pleased with the bridal gift.<BR>','Y' ), 
 ('57','6','Susannah and Husband','Susannah married her husband in a gorgeous ceremony at the end of September 2010. She was really pleased with her Coco garter which was a gift from her best friend.<BR>','Y' ), 
 ('32','11','Plums Lingerie','<P>7 Colomberie, St Helier, Jersey, Channel Islands, JE2 4QB</P>
<P>01534 731302</P>
<P><A href=\"mailto:info@plumslingerie.com\">info@plumslingerie.com</A></P>
<P><A href=\"http://www.plumslingerie.co.uk\">www.plumslingerie.co.uk</A><BR></P>','Y' ), 
 ('33','4','Wedding Ideas Magazine - March 2010','<P>Find my theme - 1920\'s</P>
<P>Grace was featured in Wedding Ideas Magazine as inspiration for 1920\'s theme.<BR></P>','Y' ), 
 ('34','4','Wedding Ideas Magazine - April 2010','<P>101 Pastels.</P>
<P>Kitty garter in pink was chosen for 101 things pastel to feature in Wedding Ideas Magazine.&nbsp; Kitty is made from 100% silk and also comes in white, ivory and pale blue.<BR></P>','Y' ), 
 ('35','4','Perfect Wedding Magazine - May 2010','<P>Sasha was chosen to be a part of a lingerie special in Perfect Wedding Magazine.&nbsp; </P>
<P>Sasha was the editors favourite.<BR></P>','Y' ), 
 ('36','4','Wedding Ideas Magazine - June 2010','<P>101 things for under £101.</P>
<P>Darcy in pink was chosen to a be a part of the under £101 feature in Wedding Ideas Magazine.</P>
<P>Darcy is made from soft tulle and embellished with Swarovski crystals and large hand tied bow.&nbsp; Also comes in white, ivory and pale blue.<BR></P>','Y' ), 
 ('37','6','Warren & Claire','<P>Recently married at The Old Swan Hotel in Harrogate. Claire had a bespoke garter made by Laura George&nbsp;which was ivory with a&nbsp;blue bow and Swarovksi heart crystal. Then embellished with Swarovski pearls. This garter tied in perfectly&nbsp;with Claires ivory gown and and its beaded embellishments.</P>
<P>Claire said she loved her&nbsp;special garter and was only to happy to show it off on her big day!</P>
<P>Congratulations to Warren and Claire.<BR></P>','Y' ), 
 ('41','11','Ann Bridal - St Albans','<p>2 Clarence Road, St Albans, Hertfordshire, AL1 4NE</p>
<p>01727 838426</p>
<p><a href=\"http://www.annbridal.co.uk\">www.annbridal.co.uk</a><br></p>','Y' ), 
 ('42','11','Ann Bridal - Stevenage','<p>111/113 High Street, Old Town, Stevenage, Hertfordshire, SG1 3HS</p>
<p>01438 318216</p>
<p><a href=\"http://www.annbridal.co.uk\">www.annbridal.co.uk</a><br></p>','Y' ), 
 ('43','11','Ellie Sanderson - Oxford','<P>26 Little Clarendon Street, Oxford, OX1 2 HU</P>
<P>01865 558444</P>
<P><A href=\"mailto:oxford@elliesanderson.com\">oxford@elliesanderson.com</A></P>
<P><A href=\"http://www.elliesanderson.co.uk\">www.elliesanderson.co.uk</A></P>
<P><BR>&nbsp;</P>','Y' ), 
 ('44','11','Ellie Sanderson - Buckinghamshire','<P>1 London End, Beaconsfield Old Town, Buckinghamshire, HP9 2HN</P>
<P>01494 674440</P>
<P><A href=\"mailto:ellie@elliesanderson.com\">ellie@elliesanderson.com</A></P>
<P><A href=\"http://www.elliesanderson.co.uk\">www.elliesanderson.co.uk</A><BR></P>','Y' ), 
 ('45','11','Curvy Bridal','<P>33 Fishergate, Boroughbridge, North Yorkshire, YO51 9AL</P>
<P>01423 325247</P>
<P><A href=\"mailto:info@curvybridal.co.uk\">info@curvybridal.co.uk</A></P>
<P><A href=\"http://www.curvybridal.co.uk\">www.curvybridal.co.uk</A><BR></P>','Y' ), 
 ('48','11','Ivory and Lace Bridal','<p>Milton Barn, Herne Manor Farm, Park Road, Toddington, Bedfordshire, LU5 6HH</p>
<p>01525 838141</p>
<p><a href=\"mailto:info@ivoryandlace-bridal.co.uk\">info@ivoryandlace-bridal.co.uk</a></p>
<p><a href=\"http://www.ivoryandlace-bridal.co.uk\">www.ivoryandlace-bridal.co.uk</a></p>','Y' ), 
 ('61','4','Bridal Buyer - August 2012','Today we have been featured on the Bridal Buyer eblast ahead of the British Bridal&nbsp;Exhibition. Featuring our updated&nbsp;directory&nbsp;and fantastic new Lookbook.&nbsp;','Y' ), 
 ('75','11','The Bridal Room Atherstone','65 Station Street, Atherstone, Warwickshire, CV9 1DB<div><br></div><div>01827 767080</div><div><br></div><div><a href=\"http://info@thebridalroomatherstone.co.uk\" title=\"\" target=\"\">info@thebridalroomatherstone.co.uk</a></div><div><br></div><div><a href=\"http://www.thebridalroomatherstone.co.uk\" title=\"\" target=\"_blank\">www.thebridalroomatherstone.co.uk</a></div><div><br></div><div><br></div><div><br></div>','Y' ), 
 ('73','11','JENNY MARIE','28 Bridge Road, Hampton Court, Surrey, KT8 9HA<div><br></div><div>020 8941 8544</div><div><br></div><div><font color=\"#993300\"><u><a href=\"mailto:info@jennymarie.co.uk\" title=\"\" target=\"\">info@jennymarie.co.uk</a></u></font></div><div><font color=\"#993300\"><br></font></div><div><font color=\"#993300\"><u><a href=\"http://www.jennymarie.co.uk\" title=\"\" target=\"_blank\">www.jennymarie.co.uk</a></u></font></div><div><font color=\"#993300\"><u><br></u></font></div><div><font color=\"#993300\"><u><br></u></font></div>','Y' ), 
 ('52','11','The Bridal Room','<P>Keil Close, High Street, Broadway, Worcestershire, WR12 7DP</P>
<P>01386 859070</P>
<P><A href=\"mailto:emma@thebridalroombroadway.co.uk\">emma@thebridalroombroadway.co.uk</A></P>
<P><A href=\"http://www.thebridalroombroadway.co.uk\">www.thebridalroombroadway.co.uk</A><BR></P>','Y' ), 
 ('76','11','Stately Brides','65b King Street, Belper, Derbyshire, DE56 1QA<div><br></div><div>01773 826 126</div><div><br></div><div><a href=\"http://enquiries@statelybrides.co.uk\" title=\"\" target=\"_blank\">enquiries@statelybrides.co.uk</a></div><div><br></div><div><a href=\"http://www.statelybrides.co.uk\" title=\"\" target=\"_blank\">www.statelybrides.co.uk</a></div>','Y' ); 
 INSERT INTO `page_blocks` (`id`, `p_id`, `title`, `content`, `viewable` ) VALUES ('59','11','Mrs Jones Bridal Boutique','<P>4 Weavers Walk, Northbrook Street, Newbury, Berkshire, RG14 1AL</P>
<P>01635 35113</P>
<P><A href=\"mailto:laura@mrsjonesbridal.co.uk\">laura@mrsjonesbridal.co.uk</A></P>
<P><A href=\"http://www.mrsjonesbridal.co.uk\">www.mrsjonesbridal.co.uk</A><BR></P>','Y' ), 
 ('60','11','Bridezillas','<p>10 Sandy Park Road, Brislington, Bristol, BS4 3PE</p>
<p>0117 904 3039</p>
<p><a href=\"mailto:amanda@bridezillas.biz\">amanda@bridezillas.biz</a></p>
<p><a href=\"http://www.bridezillas.biz\">www.bridezillas.biz</a><br></p>','Y' ), 
 ('62','11','Ellie Sanderson - The Dressing Room','11 London End, Beaconsfield, Buckinghamshire, HP9 2HN<div><br></div><div>01494 678157</div><div><br></div><div><a href=\"mailto:thedressingroom@elliesanderson.com\" title=\"\" target=\"_blank\">thedressingroom@elliesanderson.com</a></div><div><br></div><div><a href=\"http://www.elliesanderson.co.uk\" title=\"\" target=\"_blank\">www.elliesanderson.co.uk</a></div>','Y' ), 
 ('64','5','Lace Detail','<br>','Y' ), 
 ('66','4','Pure Weddings Magazine - November 2012','Our stunning Wedding Garters - Kitty and Olivia where picked to be featured in the November issue of Pure Weddings. A beautiful magazine that can help you plan your perfect wedding.','Y' ), 
 ('67','4','Leicestershire Wedding Planner - January 2013','One of our best selling garters - Coco has been chosen to go in this years Wedding Planner for the Leicestershire area!','Y' ), 
 ('68','4','Absolute Bridal Magazine - January 2013','We where really pleased when some of our stunning new lingerie range was chosen to be featured in Absolute Bridal Magazine. Amour des Fleurs silk wedding kimono and garter along with Darcy Wedding garter are to be featured.&nbsp;','Y' ), 
 ('69','4','Live and Voice Magazines - January 2013','Four of our gorgeous wedding garters have been chosen to feature in two of the best Welsh magazines - Live and Voice. Amelia, Bea, Olivia and Coco will feature.','Y' ), 
 ('72','4','Pure Weddings Magazine - Spring/ Summer 2013','We are where really pleased to be featured in the latest edition of Pure Weddings Magazine, as part on \'Brides on the Buses\'. The lovely images where taken by Lissa Alexandra photography on some beautiful wedding buses available from the Yorkshire Heritage Bus Company. Wedding garters to be featured where Cosette garter, Amelia garter and Kitty garter in red.<img src=\"http://i.imgur.com/yu675Fg.jpg\" width=\"809\">&nbsp;','Y' ); 
 -- Struktur Tabel pages -------------------- 
CREATE TABLE IF NOT EXISTS `pages` ( 
 `id` int(11) NOT NULL auto_increment, 
`title` varchar(200) NOT NULL , 
`content` text NOT NULL , 
`viewable` set('Y','N') NOT NULL  , 
PRIMARY KEY (`id`), 
KEY `viewable` (`viewable`) 
 ); 
-- Data Tabel pages -------------------- 
INSERT INTO `pages` (`id`, `title`, `content`, `viewable` ) VALUES ('2','About us - Laura George','Laura George is a designer who believes in creating exquisite&nbsp;wedding lingerie for every brides wedding day and night. She understands how important it is for every detail to be perfect on your big day. 
<p>Her lingerie is unique, elegant and modern with a touch of glamour. Products are of the highest quality and only use the finest silks, soft tulles and antique style laces. Wedding lingerie and garters are embellished with swarovksi crystals and pearls, hand tied bows and beading. All products use plush backed elastic for added comfort. Each garter and lingerie piece is individually hand made in the UK to give a luxurious finish that will compliment any brides gown and lingerie.</p>
<p>Laura George has a degree in Contour Fashion and over ten&nbsp;years industry experience designing lingerie for many high street retailers and International brands. After successfully working her way to the top of her career as Senior Lingerie Designer, Laura George decided it was time to set up her own brand.</p>
<p>As well as a truly stunning collection of modern and sophisticated wedding garters and lingerie, Laura George Couture offers a completely bespoke service for those wanting something different and endeavours to help brides achieve their dreams. Please email the designer direct for this service. All details can be found in the <a title=\"\" href=\"http://www.laurageorge.co.uk/contact.html\" target=\"\">Contact Us</a> section of this website.</p>
<p>Discover a Laura George bridal garter today for your perfect wedding day and a beautiful keepsake for a lifetime.</p>','Y' ), 
 ('3','Privacy and Security','<H2>Your information, your privacy</H2>
<P>WE DO NOT STORE ANY FINANCIAL OR PERSONAL INFORMATION ON THIS SITE</P>
<P>The Laura George shop currently uses paypal pro technology to facilitate the payment process and ensure that transactions are 100% secure. Paypal protects your financial information with industry leading security and fraud prevention systems. When you use paypal, your financial information is not shared with Laura George (the merchant). For more information on Paypal please visit their website at <A href=\"http://www.paypal.com/\">www.paypal.com</A>.</P>
<P>Once your payment is complete you will be emailed a receipt for you transaction. We require your email and postal address when purchasing from our site, this is kept securely and remains private. All information is kept securely in accordance with the data protection act. We do not share information with any third parties.</P>','Y' ), 
 ('7','Information','<p>We endeavour to deliver your order as quickly as possible, we have all garters in stock which can be sent out the same or following day, dependent on time of order. However as all our lingerie is individually handmade please allow <strong>between 7-10 working days for delivery</strong>,
from date of order confirmation. Couture orders may take longer but
this will be outlined when placing the order. We will email you once
your order has been dispatched. </p>
	<p><strong>UK delivery costs £2.50 and International delivery £6.</strong>
When ordering please click the required button for UK or International
delivery. Please note that all orders will require a signature upon
arrival.</p>
	<p>If you are not completely satisfied with your
purchase we will happily offer you an exchange or refund within 7 days
of your receipt, provided the items are unworn, unwashed and in perfect
condition. To discuss any returns or exchanges please give us a call on 07534 65 22 82 or email us at <a href=\"http://laura@laurageorge.co.uk\" title=\"\" target=\"\">laura@laurageorge.co.uk</a>. We pride ourselves on giving fantastic customer service so will do anything we can to make sure you have your perfect product.&nbsp; Please package securely and return to Laura George, 50 Harlow Crescent,
Harrogate, North Yorkshire, HG2 0AJ, UK. <strong>Please note we DO NOT accept returns or cancellations on Couture items.</strong></p>
	<p>We
do no accept responsibility for goods lost in transit, therefore we
strongly recommend that you send them by registered post and retain
proof of posting. Please allow 2-3 days for your package to reach us
(longer if outside the UK) and 7-14 working days upon our receipt of
you package for your exchange or refund to be issued.</p>
	<p>All
refunds will be issued via paypal for security. Please note that we do
not issue refunds to a third party and we do not refund postage.</p>
	<p>In the event you wish to cancel an order please ‘<a target=\"\" title=\"\" href=\"http://www.laurageorge.co.uk/contact.html\">contact us</a>’ as soon as possible via email.</p>
	<p>Please note this does not affect your statutory rights as a consumer. </p>','Y' ), 
 ('8','Fitting & Care','<p>Our garments are made to flatter, we wouldn’t want them to dig in or mishape so it is best the correct size is worn to give the right amount of ruching and emphasis on details.</p><p><br>Measure the leg where you would like the garter to be worn. Using the below size chart choose the size which the leg measurement fits into.</p>
<p><strong>Size chart</strong></p>
<table cellpadding=\"0\" cellspacing=\"0\">
<tbody>
<tr>
<td width=\"232\"><strong>Leg measurement in cms</strong></td>
<td width=\"80\"><strong>40-48</strong></td>
<td width=\"80\"><strong>48-53</strong></td>
<td width=\"80\"><strong>53-58</strong></td>
<td width=\"80\"><strong>58-63</strong></td>
<td width=\"80\"><strong>63-68</strong></td></tr>
<tr>
<td><strong><br>Size of garter to purchase</strong></td>
<td>XS</td>
<td>S</td>
<td>M </td>
<td>L</td>
<td>XL</td></tr></tbody></table>
<p><br>If your measurements do not appear on the chart please <a target=\"\" title=\"\" href=\"http://www.laurageorge.co.uk/contact.html\">‘Contact Us’</a> and we will be able to make your size for no extra cost.</p>
<h1>Care</h1>
<p>Please handwash all garters in cool water with gentle handwash liquid. Wash dark, light and coloured fabrics separatley.</p>
<p>Allow to drip dry. We DO NOT replace or refund machine washed products.</p>','Y' ), 
 ('9','History & Tradition','<P>Traditionally it has been considered lucky to gain a part of the brides clothing. Guests would grab at her dress and try to tear off pieces. Although they were not going to wear the dress again brides did not want it destroyed. Looking for a better alternative they began the custom of throwing the brides wedding&nbsp;garter to the guests which could then be divided amongst them. Today the tradition is very similar to that of the bride tossing the bouquet to all the single female guests. The groom throws the wedding&nbsp;garter to all the single men. It is believed that whichever groomsmen catches the wedding&nbsp;garter will be the next to marry. If you do not wish to take part in the garter toss, maybe give the bridal garter to the next couple you know who are going to be married. This will in turn bring them luck on their wedding day and future life together. </P>
<P>Another wedding tradition that we have all probably heard is that a bride should have ‘something old. something new, something borrowed, something blue…’ A&nbsp;wedding garter from Laura George could be part of this tradition which brings the couple infinite luck, joy and happiness.</P>','Y' ), 
 ('10','Couture','<P>Laura George Couture offers a fully bespoke service for anyone looking for&nbsp;a different garter. You may want one of the existing&nbsp;garters&nbsp;in a different colour, size or with alternative bow or embellishment. You may even have an idea of your own that you want making into reality. Using your ideas we will source the perfect fabrics to make an entirely unique&nbsp;garter just for you. </P>
<P>This service is not just for brides but for anyone who wants a special garter making for any occasion be it their prom, evening dance, burlesque event or just a night in.‘<A title=\"\" href=\"http://www.laurageorge.co.uk/contact.html\" target=\"\">Contact Us</A>’ either by email or phone so we can start creating your perfect garter today.</P>','Y' ), 
 ('11','Stockists','<h1><br></h1>','Y' ), 
 ('12','Terms & Conditions','<P>The use of this website and the purchase of any goods is governed by these terms and conditions. Please read them carefully and print and keep a copy for your reference. We reserve the right to modify these terms and conditions at anytime without notice to you. Please do not assume the same terms apply in the future. Access to this website may be suspended, restricted or terminated at any time.<BR></P>
<P>This website is owned by Laura George for all contact details go to the ‘Contact Us’ page.Throughout <A style=\"TEXT-DECORATION: underline\" href=\"http://www.laurageorge.co.uk/\">www.laurageorge.co.uk</A> references to ‘we’, ‘us’ or ‘our’ are all to Laura George.<BR></P>
<P>Making a purchase with us could not be easier, just browse around the website until you find the product you wish to buy. Choose the size, colour and quantity and add to the shopping basket. Click buy and enter all details required to satisfy the order. All prices shown are in GBP and are valid at the time of order placement, we reserve the right to change these prices at anytime. We accept all major credit and debit cards through paypal via our website. When you place an order with us you are making an offer to buy goods. We will send you an email to confirm that we accept your order and that a contract has been made between us.<BR></P>
<P>We accept no responsibility for other websites, information, practices or products of any other parties linked to the Laura George website. We are not liable for any loss or damage resulting from dealings with other websites.</P>
<P>All copyright, trademarks and all other intellectual property rights in all material shall remain at all times invested in us. You are only permitted to use material when expressly authorised by us. Reproduction of any items is a violation of applicable copyright laws.</P>','Y' ), 
 ('4','Press','<p>Please find below some of the wonderful Press release featuring Laura George Wedding Garters and Lingerie.</p>','Y' ), 
 ('5','Trends','<p>Look out for what is new in bridalwear, lingerie and accessories for 2013. Be the most stunning bride possible!</p>','Y' ), 
 ('6','Testimonials','<P>Please find brides testimonials below.&nbsp; If you would like to leave a testimonial please email us with a picture of wedding day.<BR></P>','Y' ); 
 -- Struktur Tabel products -------------------- 
CREATE TABLE IF NOT EXISTS `products` ( 
 `id` int(11) NOT NULL auto_increment, 
`c_id` int(11) NOT NULL , 
`name` varchar(100) NOT NULL , 
`description` text NOT NULL , 
`price` decimal(10,2) NOT NULL , 
`colours` varchar(50) NOT NULL , 
`sizes` varchar(50) NOT NULL  , 
PRIMARY KEY (`id`), 
KEY `c_id` (`c_id`), 
KEY `price` (`price`) 
 ); 
-- Data Tabel products -------------------- 
INSERT INTO `products` (`id`, `c_id`, `name`, `description`, `price`, `colours`, `sizes` ) VALUES ('1','6','Amelia','A gorgeous garter design created from antique style lace and decorated with a bouquet of satin roses in a striking colour combination.

A great garter design for any bride.

Laura George is a British luxury brand. Each one of our stunning wedding garters is inspired, designed and handmade in the UK. We start all our designs with our luxurious signature plush backed elastic. This makes our wedding garters unique from any others. It is super soft to touch, feels amazing when worn and gives a special finish to our fabulous products.  Every element is thought about. We only use the highest quality of fabrics and trims and use a beautiful branded woven label to finish each of our garters. 


A Laura George wedding garter will always look stunning when worn. This is because we believe in perfect fit and understand how important it is to a bride on her wedding day. Every detail must be perfect and this is why our gorgeous garters are available in five sizes. 

All of our wedding garters come packaged in a beautiful branded gift box making them a perfect keepsake.
','28.00','1,2','1,2,3,4,5' ), 
 ('3','6','Natalia - Swarovski crytals and pendant','An extravagant garter design perfect for your wedding night.

Created from beautiful soft tulle, embellished with Swarovski crystals and highlighted with stunning Swarovski crystal drop and satin bow.

The ultimate wedding night garter.

Laura George is a British luxury brand. Each one of our stunning wedding garters is inspired, designed and handmade in the UK. We start all our designs with our luxurious signature plush backed elastic. This makes our wedding garters unique from any others. It is super soft to touch, feels amazing when worn and gives a special finish to our fabulous products.  Every element is thought about. We only use the highest quality of fabrics and trims and use a beautiful branded woven label to finish each of our garters. 


A Laura George wedding garter will always look stunning when worn. This is because we believe in perfect fit and understand how important it is to a bride on her wedding day. Every detail must be perfect and this is why our gorgeous garters are available in five sizes. 

All of our wedding garters come packaged in a beautiful branded gift box making them a perfect keepsake.
','40.00','5','1,2,3,4,5' ), 
 ('2','6','Sasha - Limited edition','A garter created from a seductive layer of soft black tulle, complimented with large hand tied satin bow and metal heart trinket.

This garter is the perfect fusion for your wedding night.

This is a Limited Edition garter with less than 50 of each design made.

Laura George is a British luxury brand. Each one of our stunning wedding garters is inspired, designed and handmade in the UK. We start all our designs with our luxurious signature plush backed elastic. This makes our wedding garters unique from any others. It is super soft to touch, feels amazing when worn and gives a special finish to our fabulous products.  Every element is thought about. We only use the highest quality of fabrics and trims and use a beautiful branded woven label to finish each of our garters. 


A Laura George wedding garter will always look stunning when worn. This is because we believe in perfect fit and understand how important it is to a bride on her wedding day. Every detail must be perfect and this is why our gorgeous garters are available in five sizes. 

All of our wedding garters come packaged in a beautiful branded gift box making them a perfect keepsake.
','28.00','3,4','1,2,3,4,5' ), 
 ('4','6','Kitty - 100% Pure Silk','A truly luxurious garter design made from 100% pure silk, with hand tied satin bow.

A simple, yet elegant garter which gives a sensual finish to any brides lingerie.

Laura George is a British luxury brand. Each one of our stunning wedding garters is inspired, designed and handmade in the UK. We start all our designs with our luxurious signature plush backed elastic. This makes our wedding garters unique from any others. It is super soft to touch, feels amazing when worn and gives a special finish to our fabulous products.  Every element is thought about. We only use the highest quality of fabrics and trims and use a beautiful branded woven label to finish each of our garters. 


A Laura George wedding garter will always look stunning when worn. This is because we believe in perfect fit and understand how important it is to a bride on her wedding day. Every detail must be perfect and this is why our gorgeous garters are available in five sizes. 

All of our wedding garters come packaged in a beautiful branded gift box making them a perfect keepsake.
','34.00','1,5,7,8','1,2,3,4,5' ), 
 ('5','6','Dixie','This garter is a stunning combination of beautiful lace embellished with a striking contrast of satin bows.

This garter is a cheeky alternative to traditional pieces.

Laura George is a British luxury brand. Each one of our stunning wedding garters is inspired, designed and handmade in the UK. We start all our designs with our luxurious signature plush backed elastic. This makes our wedding garters unique from any others. It is super soft to touch, feels amazing when worn and gives a special finish to our fabulous products.  Every element is thought about. We only use the highest quality of fabrics and trims and use a beautiful branded woven label to finish each of our garters. 


A Laura George wedding garter will always look stunning when worn. This is because we believe in perfect fit and understand how important it is to a bride on her wedding day. Every detail must be perfect and this is why our gorgeous garters are available in five sizes. 

All of our wedding garters come packaged in a beautiful branded gift box making them a perfect keepsake.
','28.00','1,6,7','1,2,3,4,5' ), 
 ('6','1','Coco','An exquisite wedding garter that combines beautiful soft tulle with antique style lace.

This garter is highlighted with a hand tied satin bow in baby blue, perfect for that something blue tradition.

Laura George is a British luxury brand. Each one of our stunning wedding garters is inspired, designed and handmade in the UK. We start all our designs with our luxurious signature plush backed elastic. This makes our wedding garters unique from any others. It is super soft to touch, feels amazing when worn and gives a special finish to our fabulous products.  Every element is thought about. We only use the highest quality of fabrics and trims and use a beautiful branded woven label to finish each of our garters. 


A Laura George wedding garter will always look stunning when worn. This is because we believe in perfect fit and understand how important it is to a bride on her wedding day. Every detail must be perfect and this is why our gorgeous garters are available in five sizes. 

All of our wedding garters come packaged in a beautiful branded gift box making them a perfect keepsake.
','34.00','9,10','1,2,3,4,5' ), 
 ('7','6','Chloe - Swarovski Crystals','This garter is a beautiful contrast of blacks and pinks Chloe combines beautiful soft tulle with large hand tied satin bow, finished to perfection with either Swarovski crystals.

Laura George is a British luxury brand. Each one of our stunning wedding garters is inspired, designed and handmade in the UK. We start all our designs with our luxurious signature plush backed elastic. This makes our wedding garters unique from any others. It is super soft to touch, feels amazing when worn and gives a special finish to our fabulous products.  Every element is thought about. We only use the highest quality of fabrics and trims and use a beautiful branded woven label to finish each of our garters. 


A Laura George wedding garter will always look stunning when worn. This is because we believe in perfect fit and understand how important it is to a bride on her wedding day. Every detail must be perfect and this is why our gorgeous garters are available in five sizes. 

All of our wedding garters come packaged in a beautiful branded gift box making them a perfect keepsake.
','34.00','7','1,2,3,4,5' ), 
 ('8','1','Maisie','A beautiful wedding garter made from a sumptuous lace and decorated with a row of gorgeous satin bows.

This garter really is a flattering addition to your big day

Laura George is a British luxury brand. Each one of our stunning wedding garters is inspired, designed and handmade in the UK. We start all our designs with our luxurious signature plush backed elastic. This makes our wedding garters unique from any others. It is super soft to touch, feels amazing when worn and gives a special finish to our fabulous products.  Every element is thought about. We only use the highest quality of fabrics and trims and use a beautiful branded woven label to finish each of our garters. 


A Laura George wedding garter will always look stunning when worn. This is because we believe in perfect fit and understand how important it is to a bride on her wedding day. Every detail must be perfect and this is why our gorgeous garters are available in five sizes. 

All of our wedding garters come packaged in a beautiful branded gift box making them a perfect keepsake.

','28.00','9,10','1,2,3,4,5' ), 
 ('9','1','Kitty - 100% Pure Silk','A truly luxurious wedding garter design made from 100% pure silk, with a hand tied satin bow. 

A simple yet elegant garter that will compliment any brides gown.

Laura George is a British luxury brand. Each one of our stunning wedding garters is inspired, designed and handmade in the UK. We start all our designs with our luxurious signature plush backed elastic. This makes our wedding garters unique from any others. It is super soft to touch, feels amazing when worn and gives a special finish to our fabulous products.  Every element is thought about. We only use the highest quality of fabrics and trims and use a beautiful branded woven label to finish each of our garters. 


A Laura George wedding garter will always look stunning when worn. This is because we believe in perfect fit and understand how important it is to a bride on her wedding day. Every detail must be perfect and this is why our gorgeous garters are available in five sizes. 

All of our wedding garters come packaged in a beautiful branded gift box making them a perfect keepsake.

','34.00','6,9,10,11','1,2,3,4,5' ), 
 ('11','1','Olivia - Swarovski crystal heart','A stunning heart shaped Swarovski crystal is the perfect highlight on this delicate wedding garter design of soft tulle and satin bow.

Laura George is a British luxury brand. Each one of our stunning wedding garters is inspired, designed and handmade in the UK. We start all our designs with our luxurious signature plush backed elastic. This makes our wedding garters unique from any others. It is super soft to touch, feels amazing when worn and gives a special finish to our fabulous products.  Every element is thought about. We only use the highest quality of fabrics and trims and use a beautiful branded woven label to finish each of our garters. 


A Laura George wedding garter will always look stunning when worn. This is because we believe in perfect fit and understand how important it is to a bride on her wedding day. Every detail must be perfect and this is why our gorgeous garters are available in five sizes. 

All of our wedding garters come packaged in a beautiful branded gift box making them a perfect keepsake.
','25.00','6,9,10,11','1,2,3,4,5' ), 
 ('99','1','Madison - Swarovski Baroque crystal pendant and pearls','The creme de la creme of wedding garters. Handmade from beautiful soft Italian tulle and super soft plush backed elastic. This wedding garter is embellished all over with Swarovski pearls, large hand tied satin bow with small blue bow over. From this stunning bow combination hangs a Swarovski  crystal baroque pendant. The Swarovski crystal pendant is clear AB which has the highest quality of cuts giving the most sparkly finish. 

Laura George is a British luxury brand. Each one of our stunning wedding garters is inspired, designed and handmade in the UK. We start all our designs with our luxurious signature plush backed elastic. This makes our wedding garters unique from any others. It is super soft to touch, feels amazing when worn and gives a special finish to our fabulous products.  Every element is thought about. We only use the highest quality of fabrics and trims and use a beautiful branded woven label to finish each of our garters. 


A Laura George wedding garter will always look stunning when worn. This is because we believe in perfect fit and understand how important it is to a bride on her wedding day. Every detail must be perfect and this is why our gorgeous garters are available in five sizes. 

All of our wedding garters come packaged in a beautiful branded gift box making them a perfect keepsake.
','40.00','9,10','1,2,3,4,5' ), 
 ('13','1','Bea','A gorgeous wedding garter design created from antique style lace and large satin roses. 

Bea is the perfect garter for brides who are looking for a great blend of traditional and modern.

Laura George is a British luxury brand. Each one of our stunning wedding garters is inspired, designed and handmade in the UK. We start all our designs with our luxurious signature plush backed elastic. This makes our wedding garters unique from any others. It is super soft to touch, feels amazing when worn and gives a special finish to our fabulous products.  Every element is thought about. We only use the highest quality of fabrics and trims and use a beautiful branded woven label to finish each of our garters. 


A Laura George wedding garter will always look stunning when worn. This is because we believe in perfect fit and understand how important it is to a bride on her wedding day. Every detail must be perfect and this is why our gorgeous garters are available in five sizes. 

All of our wedding garters come packaged in a beautiful branded gift box making them a perfect keepsake.
','28.00','9,10','1,2,3,4,5' ), 
 ('14','1','Ava - Swarovski Pearls','This wedding garter is a stunning layered combination of soft tulle, lace and satin ribbon. 

Embellished with Swarovski pearls, satin bow and pearl drop. A perfect garter design for any wedding day.

Laura George is a British luxury brand. Each one of our stunning wedding garters is inspired, designed and handmade in the UK. We start all our designs with our luxurious signature plush backed elastic. This makes our wedding garters unique from any others. It is super soft to touch, feels amazing when worn and gives a special finish to our fabulous products.  Every element is thought about. We only use the highest quality of fabrics and trims and use a beautiful branded woven label to finish each of our garters. 


A Laura George wedding garter will always look stunning when worn. This is because we believe in perfect fit and understand how important it is to a bride on her wedding day. Every detail must be perfect and this is why our gorgeous garters are available in five sizes. 

All of our wedding garters come packaged in a beautiful branded gift box making them a perfect keepsake.

','40.00','9,10','1,2,3,4,5' ), 
 ('15','1','Grace - Swarovski Pearls','An elegant wedding garter design made from beautifully soft tulle and highlighted with a hand tied satin bow.

Perfectly detailed with Swarovski pearls. This garter is a sophisticated bridal piece.

Laura George is a British luxury brand. Each one of our stunning wedding garters is inspired, designed and handmade in the UK. We start all our designs with our luxurious signature plush backed elastic. This makes our wedding garters unique from any others. It is super soft to touch, feels amazing when worn and gives a special finish to our fabulous products.  Every element is thought about. We only use the highest quality of fabrics and trims and use a beautiful branded woven label to finish each of our garters. 


A Laura George wedding garter will always look stunning when worn. This is because we believe in perfect fit and understand how important it is to a bride on her wedding day. Every detail must be perfect and this is why our gorgeous garters are available in five sizes. 

All of our wedding garters come packaged in a beautiful branded gift box making them a perfect keepsake.

','34.00','9,10','1,2,3,4,5' ), 
 ('16','1','Darcy - Swarovski Crystals','Perfect in any colour Darcy garter combines beautiful soft tulle with a large hand tied satin bow and finished to perfection with Swarovski crystals. 

A contemporary wedding garter for any bride.

Laura George is a British luxury brand. Each one of our stunning wedding garters is inspired, designed and handmade in the UK. We start all our designs with our luxurious signature plush backed elastic. This makes our wedding garters unique from any others. It is super soft to touch, feels amazing when worn and gives a special finish to our fabulous products.  Every element is thought about. We only use the highest quality of fabrics and trims and use a beautiful branded woven label to finish each of our garters. 


A Laura George wedding garter will always look stunning when worn. This is because we believe in perfect fit and understand how important it is to a bride on her wedding day. Every detail must be perfect and this is why our gorgeous garters are available in five sizes. 

All of our wedding garters come packaged in a beautiful branded gift box making them a perfect keepsake.

','34.00','6,9,10,11','1,2,3,4,5' ), 
 ('55','5','Mrs A Silk Brief','Why not treat yourself to this gorgeous silk wedding brief? Our silk has a slight stretch to ensure a perfect fit and comfort on your wedding day. The mesh back is embellished with a beautiful silk heart and real Swarovski Crystals to give a truly luxurious finish. To the centre front is a small hand tied satin bow and Swarovski crystal. Each brief comes packaged in a delicate organza bag. Complete your wedding lingerie with one of our stunning wedding garters.
                                               
                                                    Comments - \'I was so excited about wearing this personalised brief on my wedding day. The silk is so soft and the Swarovski crystals so sparkly\'
\'This was the perfect finishing touch and a lovely surprise for my partner\'','60.00','10','7,8,9,10,11' ), 
 ('51','5','Mrs G Silk Brief','Why not treat yourself to this gorgeous silk wedding brief? Our silk has a slight stretch to ensure a perfect fit and comfort on your wedding day. The mesh back is embellished with a beautiful silk heart and real Swarovski Crystals to give a truly luxurious finish. To the centre front is a small hand tied satin bow and Swarovski crystal. Each brief comes packaged in a delicate organza bag. Complete your wedding lingerie with one of our stunning wedding garters.
                                               
                                                    Comments - \'I was so excited about wearing this personalised brief on my wedding day. The silk is so soft and the Swarovski crystals so sparkly\'
\'This was the perfect finishing touch and a lovely surprise for my partner\'','60.00','10','9' ), 
 ('28','1','Dolly - Swarovski Crystal','A beautiful garter made from soft tulle and embellished with five satin bows, highlighted with blue Swarovski crystals. Perfect for that something blue with sparkle!

Laura George is a British luxury brand. Each one of our stunning wedding garters is inspired, designed and handmade in the UK. We start all our designs with our luxurious signature plush backed elastic. This makes our wedding garters unique from any others. It is super soft to touch, feels amazing when worn and gives a special finish to our fabulous products.  Every element is thought about. We only use the highest quality of fabrics and trims and use a beautiful branded woven label to finish each of our garters. 


A Laura George wedding garter will always look stunning when worn. This is because we believe in perfect fit and understand how important it is to a bride on her wedding day. Every detail must be perfect and this is why our gorgeous garters are available in five sizes. 

All of our wedding garters come packaged in a beautiful branded gift box making them a perfect keepsake.
','34.00','9,10,11','1,2,3,4,5' ), 
 ('29','1','Cosette - Diamante flower','A beautiful vintage design handmade from soft tulle. Embellished with an exquisite embroided flower. Highlighted with silver thread, sequins and sead beads with a large diamante to the centre. A truly luxurious garter for any bride.

Laura George is a British luxury brand. Each one of our stunning wedding garters is inspired, designed and handmade in the UK. We start all our designs with our luxurious signature plush backed elastic. This makes our wedding garters unique from any others. It is super soft to touch, feels amazing when worn and gives a special finish to our fabulous products.  Every element is thought about. We only use the highest quality of fabrics and trims and use a beautiful branded woven label to finish each of our garters. 


A Laura George wedding garter will always look stunning when worn. This is because we believe in perfect fit and understand how important it is to a bride on her wedding day. Every detail must be perfect and this is why our gorgeous garters are available in five sizes. 

All of our wedding garters come packaged in a beautiful branded gift box making them a perfect keepsake.
','40.00','9,10','1,2,3,4,5' ), 
 ('126','2','Brief','This gorgeous brief is made from silk satin which has a slight stretch to give a fantastic fit. The back is embellished with a stunning floral applique. The flowers are laser cut with laser cut detail. Embroidered to the applique with silver and ivory threads. The applique is then decorated with mirror sequins, clear beads, straw beads and pearls. Making this a truly special piece to have as part of your wedding lingerie.','65.00','10','7,8,9,10,11' ), 
 ('150','1','Oriana','A beautiful lace garter embellished with double bow made from satin and organza ribbons. Highlighted with a lovely heart shaped trinket.

Laura George is a British luxury brand. Each one of our stunning wedding garters is inspired, designed and handmade in the UK. We start all our designs with our luxurious signature plush backed elastic. This makes our wedding garters unique from any others. It is super soft to touch, feels amazing when worn and gives a special finish to our fabulous products. Every element is thought about. We only use the highest quality of fabrics and trims and use a beautiful branded woven label to finish each of our garters. 


A Laura George wedding garter will always look stunning when worn. This is because we believe in perfect fit and understand how important it is to a bride on her wedding day. Every detail must be perfect and this is why our gorgeous garters are available in five sizes. 

All of our wedding garters come packaged in a beautiful branded gift box making them a perfect keepsake.
','28.00','9,10','1,2,3,4' ), 
 ('56','5','Mrs B Silk Brief','Why not treat yourself to this gorgeous silk wedding brief? Our silk has a slight stretch to ensure a perfect fit and comfort on your wedding day. The mesh back is embellished with a beautiful silk heart and real Swarovski Crystals to give a truly luxurious finish. To the centre front is a small hand tied satin bow and Swarovski crystal. Each brief comes packaged in a delicate organza bag. Complete your wedding lingerie with one of our stunning wedding garters.
                                               
                                                    Comments - \'I was so excited about wearing this personalised brief on my wedding day. The silk is so soft and the Swarovski crystals so sparkly\'
\'This was the perfect finishing touch and a lovely surprise for my partner\'','60.00','10','7,8,9,10,11' ), 
 ('57','5','Mrs C silk brief','Why not treat yourself to this gorgeous silk wedding brief? Our silk has a slight stretch to ensure a perfect fit and comfort on your wedding day. The mesh back is embellished with a beautiful silk heart and real Swarovski Crystals to give a truly luxurious finish. To the centre front is a small hand tied satin bow and Swarovski crystal. Each brief comes packaged in a delicate organza bag. Complete your wedding lingerie with one of our stunning wedding garters.
                                               
                                                    Comments - \'I was so excited about wearing this personalised brief on my wedding day. The silk is so soft and the Swarovski crystals so sparkly\'
\'This was the perfect finishing touch and a lovely surprise for my partner\'','60.00','10','7,8,9,10,11' ), 
 ('58','5','Mrs D silk brief','Why not treat yourself to this gorgeous silk wedding brief? Our silk has a slight stretch to ensure a perfect fit and comfort on your wedding day. The mesh back is embellished with a beautiful silk heart and real Swarovski Crystals to give a truly luxurious finish. To the centre front is a small hand tied satin bow and Swarovski crystal. Each brief comes packaged in a delicate organza bag. Complete your wedding lingerie with one of our stunning wedding garters.
                                               
                                                    Comments - \'I was so excited about wearing this personalised brief on my wedding day. The silk is so soft and the Swarovski crystals so sparkly\'
\'This was the perfect finishing touch and a lovely surprise for my partner\'','60.00','10','7,8,9,10,11' ), 
 ('59','5','Mrs E silk brief','Why not treat yourself to this gorgeous silk wedding brief? Our silk has a slight stretch to ensure a perfect fit and comfort on your wedding day. The mesh back is embellished with a beautiful silk heart and real Swarovski Crystals to give a truly luxurious finish. To the centre front is a small hand tied satin bow and Swarovski crystal. Each brief comes packaged in a delicate organza bag. Complete your wedding lingerie with one of our stunning wedding garters.
                                               
                                                    Comments - \'I was so excited about wearing this personalised brief on my wedding day. The silk is so soft and the Swarovski crystals so sparkly\'
\'This was the perfect finishing touch and a lovely surprise for my partner\'','60.00','10','7,8,9,10,11' ), 
 ('60','5','Mrs F silk brief','Why not treat yourself to this gorgeous silk wedding brief? Our silk has a slight stretch to ensure a perfect fit and comfort on your wedding day. The mesh back is embellished with a beautiful silk heart and real Swarovski Crystals to give a truly luxurious finish. To the centre front is a small hand tied satin bow and Swarovski crystal. Each brief comes packaged in a delicate organza bag. Complete your wedding lingerie with one of our stunning wedding garters.
                                               
                                                    Comments - \'I was so excited about wearing this personalised brief on my wedding day. The silk is so soft and the Swarovski crystals so sparkly\'
\'This was the perfect finishing touch and a lovely surprise for my partner\'','60.00','10','7,8,9,10,11' ), 
 ('61','5','Mrs H Silk brief','Why not treat yourself to this gorgeous silk wedding brief? Our silk has a slight stretch to ensure a perfect fit and comfort on your wedding day. The mesh back is embellished with a beautiful silk heart and real Swarovski Crystals to give a truly luxurious finish. To the centre front is a small hand tied satin bow and Swarovski crystal. Each brief comes packaged in a delicate organza bag. Complete your wedding lingerie with one of our stunning wedding garters.
                                               
                                                    Comments - \'I was so excited about wearing this personalised brief on my wedding day. The silk is so soft and the Swarovski crystals so sparkly\'
\'This was the perfect finishing touch and a lovely surprise for my partner\'','60.00','10','7,8,9,10,11' ), 
 ('62','5','Mrs I silk brief','Why not treat yourself to this gorgeous silk wedding brief? Our silk has a slight stretch to ensure a perfect fit and comfort on your wedding day. The mesh back is embellished with a beautiful silk heart and real Swarovski Crystals to give a truly luxurious finish. To the centre front is a small hand tied satin bow and Swarovski crystal. Each brief comes packaged in a delicate organza bag. Complete your wedding lingerie with one of our stunning wedding garters.
                                               
                                                    Comments - \'I was so excited about wearing this personalised brief on my wedding day. The silk is so soft and the Swarovski crystals so sparkly\'
\'This was the perfect finishing touch and a lovely surprise for my partner\'','60.00','10','7,8,9,10,11' ), 
 ('63','5','Mrs J silk brief','Why not treat yourself to this gorgeous silk wedding brief? Our silk has a slight stretch to ensure a perfect fit and comfort on your wedding day. The mesh back is embellished with a beautiful silk heart and real Swarovski Crystals to give a truly luxurious finish. To the centre front is a small hand tied satin bow and Swarovski crystal. Each brief comes packaged in a delicate organza bag. Complete your wedding lingerie with one of our stunning wedding garters.
                                               
                                                    Comments - \'I was so excited about wearing this personalised brief on my wedding day. The silk is so soft and the Swarovski crystals so sparkly\'
\'This was the perfect finishing touch and a lovely surprise for my partner\'','60.00','10','7,8,9,10,11' ), 
 ('64','5','Mrs K silk brief','Why not treat yourself to this gorgeous silk wedding brief? Our silk has a slight stretch to ensure a perfect fit and comfort on your wedding day. The mesh back is embellished with a beautiful silk heart and real Swarovski Crystals to give a truly luxurious finish. To the centre front is a small hand tied satin bow and Swarovski crystal. Each brief comes packaged in a delicate organza bag. Complete your wedding lingerie with one of our stunning wedding garters.
                                               
                                                    Comments - \'I was so excited about wearing this personalised brief on my wedding day. The silk is so soft and the Swarovski crystals so sparkly\'
\'This was the perfect finishing touch and a lovely surprise for my partner\'','60.00','10','7,8,9,10,11' ); 
 INSERT INTO `products` (`id`, `c_id`, `name`, `description`, `price`, `colours`, `sizes` ) VALUES ('65','5','Mrs L silk brief','Why not treat yourself to this gorgeous silk wedding brief? Our silk has a slight stretch to ensure a perfect fit and comfort on your wedding day. The mesh back is embellished with a beautiful silk heart and real Swarovski Crystals to give a truly luxurious finish. To the centre front is a small hand tied satin bow and Swarovski crystal. Each brief comes packaged in a delicate organza bag. Complete your wedding lingerie with one of our stunning wedding garters.
                                               
                                                    Comments - \'I was so excited about wearing this personalised brief on my wedding day. The silk is so soft and the Swarovski crystals so sparkly\'
\'This was the perfect finishing touch and a lovely surprise for my partner\'','60.00','10','7,8,9,10,11' ), 
 ('66','5','Mrs M Silk brief','Why not treat yourself to this gorgeous silk wedding brief? Our silk has a slight stretch to ensure a perfect fit and comfort on your wedding day. The mesh back is embellished with a beautiful silk heart and real Swarovski Crystals to give a truly luxurious finish. To the centre front is a small hand tied satin bow and Swarovski crystal. Each brief comes packaged in a delicate organza bag. Complete your wedding lingerie with one of our stunning wedding garters.
                                               
                                                    Comments - \'I was so excited about wearing this personalised brief on my wedding day. The silk is so soft and the Swarovski crystals so sparkly\'
\'This was the perfect finishing touch and a lovely surprise for my partner\'','60.00','10','7,8,9,10,11' ), 
 ('67','5','Mrs N Silk brief','Why not treat yourself to this gorgeous silk wedding brief? Our silk has a slight stretch to ensure a perfect fit and comfort on your wedding day. The mesh back is embellished with a beautiful silk heart and real Swarovski Crystals to give a truly luxurious finish. To the centre front is a small hand tied satin bow and Swarovski crystal. Each brief comes packaged in a delicate organza bag. Complete your wedding lingerie with one of our stunning wedding garters.
                                               
                                                    Comments - \'I was so excited about wearing this personalised brief on my wedding day. The silk is so soft and the Swarovski crystals so sparkly\'
\'This was the perfect finishing touch and a lovely surprise for my partner\'','60.00','10','7,8,9,10,11' ), 
 ('68','5','Mrs O Silk brief','Why not treat yourself to this gorgeous silk wedding brief? Our silk has a slight stretch to ensure a perfect fit and comfort on your wedding day. The mesh back is embellished with a beautiful silk heart and real Swarovski Crystals to give a truly luxurious finish. To the centre front is a small hand tied satin bow and Swarovski crystal. Each brief comes packaged in a delicate organza bag. Complete your wedding lingerie with one of our stunning wedding garters.
                                               
                                                    Comments - \'I was so excited about wearing this personalised brief on my wedding day. The silk is so soft and the Swarovski crystals so sparkly\'
\'This was the perfect finishing touch and a lovely surprise for my partner\'','60.00','10','7,8,9,10,11' ), 
 ('69','5','Mrs P silk brief','Why not treat yourself to this gorgeous silk wedding brief? Our silk has a slight stretch to ensure a perfect fit and comfort on your wedding day. The mesh back is embellished with a beautiful silk heart and real Swarovski Crystals to give a truly luxurious finish. To the centre front is a small hand tied satin bow and Swarovski crystal. Each brief comes packaged in a delicate organza bag. Complete your wedding lingerie with one of our stunning wedding garters.
                                               
                                                    Comments - \'I was so excited about wearing this personalised brief on my wedding day. The silk is so soft and the Swarovski crystals so sparkly\'
\'This was the perfect finishing touch and a lovely surprise for my partner\'','60.00','10','7,8,9,10,11' ), 
 ('70','5','Mrs Q silk brief','Why not treat yourself to this gorgeous silk wedding brief? Our silk has a slight stretch to ensure a perfect fit and comfort on your wedding day. The mesh back is embellished with a beautiful silk heart and real Swarovski Crystals to give a truly luxurious finish. To the centre front is a small hand tied satin bow and Swarovski crystal. Each brief comes packaged in a delicate organza bag. Complete your wedding lingerie with one of our stunning wedding garters.
                                               
                                                    Comments - \'I was so excited about wearing this personalised brief on my wedding day. The silk is so soft and the Swarovski crystals so sparkly\'
\'This was the perfect finishing touch and a lovely surprise for my partner\'','60.00','10','7,8,9,10,11' ), 
 ('71','5','Mrs U silk brief','Why not treat yourself to this gorgeous silk wedding brief? Our silk has a slight stretch to ensure a perfect fit and comfort on your wedding day. The mesh back is embellished with a beautiful silk heart and real Swarovski Crystals to give a truly luxurious finish. To the centre front is a small hand tied satin bow and Swarovski crystal. Each brief comes packaged in a delicate organza bag. Complete your wedding lingerie with one of our stunning wedding garters.
                                               
                                                    Comments - \'I was so excited about wearing this personalised brief on my wedding day. The silk is so soft and the Swarovski crystals so sparkly\'
\'This was the perfect finishing touch and a lovely surprise for my partner\'','60.00','10','7,8,9,10,11' ), 
 ('72','5','Mrs V silk brief','Why not treat yourself to this gorgeous silk wedding brief? Our silk has a slight stretch to ensure a perfect fit and comfort on your wedding day. The mesh back is embellished with a beautiful silk heart and real Swarovski Crystals to give a truly luxurious finish. To the centre front is a small hand tied satin bow and Swarovski crystal. Each brief comes packaged in a delicate organza bag. Complete your wedding lingerie with one of our stunning wedding garters.
                                               
                                                    Comments - \'I was so excited about wearing this personalised brief on my wedding day. The silk is so soft and the Swarovski crystals so sparkly\'
\'This was the perfect finishing touch and a lovely surprise for my partner\'','60.00','10','7,8,9,10,11' ), 
 ('73','5','Mrs W silk brief','Why not treat yourself to this gorgeous silk wedding brief? Our silk has a slight stretch to ensure a perfect fit and comfort on your wedding day. The mesh back is embellished with a beautiful silk heart and real Swarovski Crystals to give a truly luxurious finish. To the centre front is a small hand tied satin bow and Swarovski crystal. Each brief comes packaged in a delicate organza bag. Complete your wedding lingerie with one of our stunning wedding garters.
                                               
                                                    Comments - \'I was so excited about wearing this personalised brief on my wedding day. The silk is so soft and the Swarovski crystals so sparkly\'
\'This was the perfect finishing touch and a lovely surprise for my partner\'','60.00','10','7,8,9,10,11' ), 
 ('74','5','Mrs X silk brief','Why not treat yourself to this gorgeous silk wedding brief? Our silk has a slight stretch to ensure a perfect fit and comfort on your wedding day. The mesh back is embellished with a beautiful silk heart and real Swarovski Crystals to give a truly luxurious finish. To the centre front is a small hand tied satin bow and Swarovski crystal. Each brief comes packaged in a delicate organza bag. Complete your wedding lingerie with one of our stunning wedding garters.
                                               
                                                    Comments - \'I was so excited about wearing this personalised brief on my wedding day. The silk is so soft and the Swarovski crystals so sparkly\'
\'This was the perfect finishing touch and a lovely surprise for my partner\'','60.00','10','7,8,9,10,11' ), 
 ('75','5','Mrs Y silk brief','Why not treat yourself to this gorgeous silk wedding brief? Our silk has a slight stretch to ensure a perfect fit and comfort on your wedding day. The mesh back is embellished with a beautiful silk heart and real Swarovski Crystals to give a truly luxurious finish. To the centre front is a small hand tied satin bow and Swarovski crystal. Each brief comes packaged in a delicate organza bag. Complete your wedding lingerie with one of our stunning wedding garters.
                                               
                                                    Comments - \'I was so excited about wearing this personalised brief on my wedding day. The silk is so soft and the Swarovski crystals so sparkly\'
\'This was the perfect finishing touch and a lovely surprise for my partner\'','60.00','10','' ), 
 ('76','5','Mrs Z silk brief','Why not treat yourself to this gorgeous silk wedding brief? Our silk has a slight stretch to ensure a perfect fit and comfort on your wedding day. The mesh back is embellished with a beautiful silk heart and real Swarovski Crystals to give a truly luxurious finish. To the centre front is a small hand tied satin bow and Swarovski crystal. Each brief comes packaged in a delicate organza bag. Complete your wedding lingerie with one of our stunning wedding garters.
                                               
                                                    Comments - \'I was so excited about wearing this personalised brief on my wedding day. The silk is so soft and the Swarovski crystals so sparkly\'
\'This was the perfect finishing touch and a lovely surprise for my partner\'','60.00','10','7,8,9,10,11' ), 
 ('77','5','Mrs R silk brief','Why not treat yourself to this gorgeous silk wedding brief? Our silk has a slight stretch to ensure a perfect fit and comfort on your wedding day. The mesh back is embellished with a beautiful silk heart and real Swarovski Crystals to give a truly luxurious finish. To the centre front is a small hand tied satin bow and Swarovski crystal. Each brief comes packaged in a delicate organza bag. Complete your wedding lingerie with one of our stunning wedding garters.
                                               
                                                    Comments - \'I was so excited about wearing this personalised brief on my wedding day. The silk is so soft and the Swarovski crystals so sparkly\'
\'This was the perfect finishing touch and a lovely surprise for my partner\'','60.00','10','7,8,9,10,11' ), 
 ('78','5','Mrs S silk brief','Why not treat yourself to this gorgeous silk wedding brief? Our silk has a slight stretch to ensure a perfect fit and comfort on your wedding day. The mesh back is embellished with a beautiful silk heart and real Swarovski Crystals to give a truly luxurious finish. To the centre front is a small hand tied satin bow and Swarovski crystal. Each brief comes packaged in a delicate organza bag. Complete your wedding lingerie with one of our stunning wedding garters.
                                               
                                                    Comments - \'I was so excited about wearing this personalised brief on my wedding day. The silk is so soft and the Swarovski crystals so sparkly\'
\'This was the perfect finishing touch and a lovely surprise for my partner\'','60.00','10','7,8,9,10,11' ), 
 ('80','5','Mrs T silk brief','Why not treat yourself to this gorgeous silk wedding brief? Our silk has a slight stretch to ensure a perfect fit and comfort on your wedding day. The mesh back is embellished with a beautiful silk heart and real Swarovski Crystals to give a truly luxurious finish. To the centre front is a small hand tied satin bow and Swarovski crystal. Each brief comes packaged in a delicate organza bag. Complete your wedding lingerie with one of our stunning wedding garters.
                                               
                                                    Comments - \'I was so excited about wearing this personalised brief on my wedding day. The silk is so soft and the Swarovski crystals so sparkly\'
\'This was the perfect finishing touch and a lovely surprise for my partner\'','60.00','10','7,8,9,10,11' ), 
 ('121','5','The design your own','If you would like to design your own silk brief then just purchase this option and send us a quick email with whatever you would like in your heart! This is a great option if you are going to have a double barrel surname or not even changing your name at all! You could have both your initials or just one word such as Bride or Love. These wedding briefs are made from gorgeous soft silk with a slight stretch and embellished with real Swarovski crystals. Each pair comes packaged in a delicate organza bag. Why not complete your wedding lingerie with one of our stunning wedding garters. Comments \' I just loved these pants for my wedding, it meant that my lingerie was totally unique and at no extra cost, fantastic!\'','60.00','10','7,8,9,10,11' ), 
 ('82','5','The perfect garter','Why not complete your wedding lingerie with this stunning wedding garter. Made from soft tulle and plush backed elastic. This beautiful design is embellished with a large hand tied satin bow and Swarovski crystals.
Laura George is a British luxury brand. Each one of our stunning wedding garters is inspired, designed and handmade in the UK. We start all our designs with our luxurious signature plush backed elastic. This makes our wedding garters unique from any others. It is super soft to touch, feels amazing when worn and gives a special finish to our fabulous products.  Every element is thought about. We only use the highest quality of fabrics and trims and use a beautiful branded woven label to finish each of our garters. 


A Laura George wedding garter will always look stunning when worn. This is because we believe in perfect fit and understand how important it is to a bride on her wedding day. Every detail must be perfect and this is why our gorgeous garters are available in five sizes. 

All of our wedding garters come packaged in a beautiful branded gift box making them a perfect keepsake.

','34.00','6,9,10,11','1,2,3,4,5' ), 
 ('125','2','Amour Strapless Cami','This gorgeous strapless cami is made from 100% silk chiffon. The chiffon has a slight crinkle which gives a fantastic look to this garment. The side front is embellished with a stunning floral applique. The flowers are laser cut with laser cut detail. Embroidered to the applique with silver and ivory threads. The applique is then decorated with mirror sequins, clear beads, straw beads and pearls. Making this a truly special piece to have as part of your wedding lingerie. It is perfect to wear when getting ready as it wont cause any straplines. The alluring quality of the fabric also makes it the perfect lingerie to wear on the wedding night and honeymoon.','95.00','10','1,2,3,4,5' ), 
 ('100','2','Head band','A stunning head band to match your wedding lingerie collection. Handmade from super soft tulle and push backed elastic. Embellished with laser cut flower, sequins, pearls and ivory and silver embroidery.','65.00','10','63' ), 
 ('89','8','Fairytale Kimono','This beautiful silk Kimono is an ideal piece as part of your wedding lingerie collection. Made from a luxurious crinkle chiffon and delicate ivory lace. With matching full lace tie, hand tied satin bows and Swarovski crystals. This wedding Kimono is perfect for slipping on whilst getting ready for your big day or lounging around on your honeymoon.','135.00','10','63' ), 
 ('90','8','Garter','This gorgeous garter is the perfect compliment to the Fairytale Kimono. Made from the most delicate of ivory laces with plush backed elastic. This wedding garter comes packaged in a beautiful organza bag and can be embellished with either an ivory or pale blue and tied satin bow and crystal.

Laura George is a British luxury brand. Each one of our stunning wedding garters is inspired, designed and handmade in the UK. We start all our designs with our luxurious signature plush backed elastic. This makes our wedding garters unique from any others. It is super soft to touch, feels amazing when worn and gives a special finish to our fabulous products.  Every element is thought about. We only use the highest quality of fabrics and trims and use a beautiful branded woven label to finish each of our garters. 


A Laura George wedding garter will always look stunning when worn. This is because we believe in perfect fit and understand how important it is to a bride on her wedding day. Every detail must be perfect and this is why our gorgeous garters are available in five sizes. 

All of our wedding garters come packaged in a beautiful branded gift box making them a perfect keepsake.
','28.00','10,11','1,2,3,4,5' ), 
 ('106','3','Silk French Knicker','This gorgeous french knicker is made from 100% silk. With a slight stretch and satin finish this is one of our most luxurious of fabrics. It can be worn with our super soft camisole or hand knitted lounge wrap. These beautiful knickers are finished with hand tied satin ivory bows and real Swarovski crystals. A simple yet stunning design.','85.00','10','9' ), 
 ('104','9','Silk and Chantilly Lace Kimono','This beautiful wedding kimono is truly stunning. Handmade in the UK from crinkle silk chiffon and Chantilly lace. This is the creme de la creme piece for your wedding day. Can be left open or hand tied with our silk satin sash.','365.00','10','63' ), 
 ('105','9','Silk and Chantilly Lace Long Kimono','This is our statement wedding piece. Our lingerie is handmade, high end and aspirational. This long kimono is perfect for any designer bride. This stunning piece combines a beautiful French Chantilly lace with crinkle silk chiffon. This elegant kimono has a stunning train edged with our gorgeous Chantilly lace. Waist tied with satin silk tie that gives a beautiful contrast of texture to the chiffon.  Choose this piece as part of your wedding lingerie and make it a day/ night to remember.','625.00','10','63' ), 
 ('103','9','Silk and Chantilly Lace Teddy','This is the creme de la creme of our wedding lingerie range - beautiful silk chiffon combined Chantilly lace create this beautiful playsuit. This is the perfect piece for your wedding night or honeymoon. Front fastening with alluring peep holes and embellished with tiny rows of hand tied satin bows.','275.00','10','9' ), 
 ('127','2','Garter','This stunning wedding garter is perfect on its own or as part of the Amour des Fluers wedding lingerie collection. Lovingly handmade from super soft tulle and plush backed elastic. This beautiful wedding garter is embellished with a gorgeous applique. Laser cut flowers are decorated mirror sequins, clear beads, straw beads and pearls. The applique is then delicately embroidered with ivory and silver threads. This garter comes packaged in a beautiful organza bag.','55.00','10','1,2,3,4,5' ), 
 ('101','2','Kimono','A truly stunning kimono. Handmade in the UK from a beautiful crinkle silk chiffon. With beautiful back detail, this kimono is embellished with a row of floral appliques that sweep down from the shoulder to the hem. Each applique is embroidered with silver and ivory threads, laser cut flowers, mirror sequins, clear beads, silver beads and pearls. The perfect piece to wear when getting ready for your big day or to sweep in on your wedding night!','195.00','10','63' ), 
 ('102','2','The Trousseau','Why not buy every piece of this stunning lingerie collection for your wedding Trousseau. Includes strapless camisole, brief, garter, headband and kimono.','390.00','10','1,2,3,4,5' ), 
 ('128','3','Camisole','This super soft camisole is made from a beautiful microfibre that you will just want to touch. The straps are silk satin and match perfectly to our french knicker and lounge pant. The front of this gorgeous camisole is embellished with stunning floral embroidery applique, which is highlighted with Swarovski pearls. A truly luxurious piece to have for your wedding day and honeymoon.','85.00','10','7,8,9,10,11' ), 
 ('107','3','Silk Lounge Pant','You will feel \'a million dollars\' in these stunning silk lounge pants. Wear them when preparing for your big day or lounging around on your honeymoon. They are handmade from silk to give a truly luxurious finish. Embellished with hand tied satin bows and Swarovksi crystals - a simple yet stunning design.','195.00','10','9' ); 
 INSERT INTO `products` (`id`, `c_id`, `name`, `description`, `price`, `colours`, `sizes` ) VALUES ('108','3','Hand Knitted Wool Wrap - Online Exclusive','This gorgeous wrap is the perfect addition to your lounge wear collection. Hand knitted in the UK from 100% Merino wool. This wool is super soft and ultra fine to give any hand knitted garment a lace like finish. This piece will look stunning with the silk lounge pant or knicker. Perfect for snuggling up on your honeymoon. This pattern was designed exclusively for Laura George.','365.00','10','7,8,9,10,11' ), 
 ('109','4','Camisole','This exquisite camisole is made from a stunning combination of silk and lace. The lace is light with a floral design and given a depth with a luxurious cording, the galloon edge of the lace finishes the edge of the camisole. Highlighted with hand tied satin ivory bows and Swarovski crystals.','165.00','10','9' ), 
 ('110','4','Lace Wrap','This beautiful lace wrap is a stunning yet elegant piece to have as part of your wedding lingerie. The lace is light with a floral design that has added depth with cording and galloon edge. This truly luxurious piece can be hand tied at the waist with a silk tie, making you feel truly special.','265.00','10','7,8,9,10,11' ), 
 ('111','3','Hand Knitted Wool Cape - Online Exclusive','You will feel gorgeous in this hand knitted wool cape. Made from Superkid Mohair, Silk and Metalized fibre this cape is dark ivory with flecks of gold.  It is a beautiful and versatile piece - you could wear on your wedding day with your dress as beautiful shrug, to lounge around in on your honeymoon or as a traditional bed jacket. If you would like a different colour just let us know as we hand knit to order.','295.00','4','63' ), 
 ('112','4','Silk French Knicker','This gorgeous french knicker is made from 100% silk. With a slight stretch and satin finish this is one of our most luxurious of fabrics. Can be worn with Romance camisole or lace wrap and also any of our loungewear items to create a stunning capsule collection of wedding lingerie. These beautiful knickers are finished with hand tied satin ivory bows and real Swarovski crystals. A simple yet stunning design.','85.00','10','9' ), 
 ('113','4','Simple Brief','This simple brief is the perfect partner for our gorgeous lace camisole and wrap. Handmade from silk and mesh to give a luxurious finish this brief is embellished with one hand tied satin bow and Swarovski crystal.','50.00','10','7,8,9,10,11' ), 
 ('114','4','Silk Lounge Pant','You will feel \'a million dollars\' in these stunning silk lounge pants. Wear them when preparing for your big day or lounging around on your honeymoon. They are handmade from silk to give a truly luxurious finish. Embellished with hand tied satin bows and Swarovksi crystals - a simple yet stunning design.','195.00','10','7,8,9,10,11' ), 
 ('115','9','Chantilly Lace Garter','This stunning wedding garter is lovingly handmade from Chantilly lace. With plush backed elastic for comfort and luxury this beautiful garter is embellished with large hand tied satin bow.','95.00','10','1,2,3,4,5' ), 
 ('116','9','Silk Wedding Brief','This beautiful wedding brief is handmade from silk and mesh, with large hand tied satin bow. With a smooth finish it is perfect to wear under any gown. Why not wear it with our gorgeous Chantilly lace garter.','50.00','10','7,8,9,10,11' ), 
 ('117','10','Romance Silk PJ\'s','This is a truly gorgeous set of silk and lace PJ\'s. The camisole has silk cups and corded lace body with wider straps. The PJ pant is 100% silk with a slight stretch for great comfort and perfect fit. This set is embellished with hand tied satin bows and Swarovski crystals.','285.00','10','9' ), 
 ('118','10','Romance Silk Sleep Set','This set is perfect for your wedding night or destination honeymoon. These gorgeous silk knickers are ideal for sleeping in - light and floaty yet super sexy. Teamed with our gorgeous silk and lace camisole this makes the perfect set. Highlighted with hand tied satin bows and Swarovski crystals.','195.00','10','7,8,9,10,11' ), 
 ('119','10','Sleep Set','This gorgeous sleep set combines super soft microfibre with silk, floral applique and Swarovski pearls. The beautiful short is silk with a slight stretch for luxury and comfort. The applique top is made from a beautiful soft microfibre that you will just want to touch again and again.','135.00','10','7,8,9,11' ), 
 ('120','10','Silk PJ\'S','This gorgeous set of PJ\'s is made up of our beautiful silk PJ pant and super soft microfibre lounge camisole. This is our best selling PJ set and we think this is due to the great combination of luxurious fabrics that give a great feel and comfort along with the beautiful and subtle embellishments of floral applique and Swarovski pearls.','245.00','10','7,8,9,10,11' ), 
 ('129','1','Peony - Great for garter toss','A gorgeous lace garter - great for your garter toss garter. Handmade from a narrow lace and embellished with a beautiful hand tied satin bow and Signature Swarovski crystal.

Laura George is a British luxury brand. Each one of our stunning wedding garters is inspired, designed and handmade in the UK. We start all our designs with our luxurious signature plush backed elastic. This makes our wedding garters unique from any others. It is super soft to touch, feels amazing when worn and gives a special finish to our fabulous products.  Every element is thought about. We only use the highest quality of fabrics and trims and use a beautiful branded woven label to finish each of our garters. 


A Laura George wedding garter will always look stunning when worn. This is because we believe in perfect fit and understand how important it is to a bride on her wedding day. Every detail must be perfect and this is why our gorgeous garters are available in five sizes. 

All of our wedding garters come packaged in a beautiful branded gift box making them a perfect keepsake.
','18.00','9,10','1,2,3,4,5' ), 
 ('130','1','Poppy - Great for Garter toss','A beautiful soft tulle garter - perfect for your garter toss garter. Handmade in the UK from super soft tulle and embellished with hand tied satin blue bow and Swarovski crystal.
Laura George is a British luxury brand. Each one of our stunning wedding garters is inspired, designed and handmade in the UK. We start all our designs with our luxurious signature plush backed elastic. This makes our wedding garters unique from any others. It is super soft to touch, feels amazing when worn and gives a special finish to our fabulous products.  Every element is thought about. We only use the highest quality of fabrics and trims and use a beautiful branded woven label to finish each of our garters. 


A Laura George wedding garter will always look stunning when worn. This is because we believe in perfect fit and understand how important it is to a bride on her wedding day. Every detail must be perfect and this is why our gorgeous garters are available in five sizes. 

All of our wedding garters come packaged in a beautiful branded gift box making them a perfect keepsake.

','18.00','9,10','1,2,3,4,5' ), 
 ('136','7','Oliva Special Edition','A stunning heart shaped Swarovski crystal is the perfect highlight on this delicate wedding garter design of soft tulle and satin bow. Laura George is a British luxury brand. Each one of our stunning wedding garters is inspired, designed and handmade in the UK. We start all our designs with our luxurious signature plush backed elastic. This makes our wedding garters unique from any others. It is super soft to touch, feels amazing when worn and gives a special finish to our fabulous products. Every element is thought about. We only use the highest quality of fabrics and trims and use a beautiful branded woven label to finish each of our garters. A Laura George wedding garter will always look stunning when worn. This is because we believe in perfect fit and understand how important it is to a bride on her wedding day. Every detail must be perfect and this is why our gorgeous garters are available in five sizes. All of our wedding garters come packaged in a beautiful branded gift box making them a perfect keepsake.','20.00','9,10','1,2,3,4,5' ), 
 ('138','1','Oliva Special Edition','A stunning heart shaped Swarovski crystal is the perfect highlight on this delicate wedding garter design of soft tulle and satin bow. Laura George is a British luxury brand. Each one of our stunning wedding garters is inspired, designed and handmade in the UK. We start all our designs with our luxurious signature plush backed elastic. This makes our wedding garters unique from any others. It is super soft to touch, feels amazing when worn and gives a special finish to our fabulous products. Every element is thought about. We only use the highest quality of fabrics and trims and use a beautiful branded woven label to finish each of our garters. A Laura George wedding garter will always look stunning when worn. This is because we believe in perfect fit and understand how important it is to a bride on her wedding day. Every detail must be perfect and this is why our gorgeous garters are available in five sizes. All of our wedding garters come packaged in a beautiful branded gift box making them a perfect keepsake.','25.00','9,10','1,2,3,4,5' ), 
 ('139','1','Fleurs','This stunning wedding garter is perfect on its own or as part of the Amour des Fluers wedding lingerie collection. Lovingly handmade from super soft tulle and plush backed elastic. This beautiful wedding garter is embellished with a gorgeous applique. Laser cut flowers are decorated mirror sequins, clear beads, straw beads and pearls. The applique is then delicately embroidered with ivory and silver threads.

Laura George is a British luxury brand. Each one of our stunning wedding garters is inspired, designed and handmade in the UK. We start all our designs with our luxurious signature plush backed elastic. This makes our wedding garters unique from any others. It is super soft to touch, feels amazing when worn and gives a special finish to our fabulous products. Every element is thought about. We only use the highest quality of fabrics and trims and use a beautiful branded woven label to finish each of our garters. 


A Laura George wedding garter will always look stunning when worn. This is because we believe in perfect fit and understand how important it is to a bride on her wedding day. Every detail must be perfect and this is why our gorgeous garters are available in five sizes. 

All of our wedding garters come packaged in a beautiful branded gift box making them a perfect keepsake.','55.00','10','1,2,3,4,5' ), 
 ('140','1','Chantilly','This stunning wedding garter is lovingly handmade from Chantilly lace. With plush backed elastic for comfort and luxury this beautiful garter is embellished with large hand tied satin bow.

Laura George is a British luxury brand. Each one of our stunning wedding garters is inspired, designed and handmade in the UK. We start all our designs with our luxurious signature plush backed elastic. This makes our wedding garters unique from any others. It is super soft to touch, feels amazing when worn and gives a special finish to our fabulous products. Every element is thought about. We only use the highest quality of fabrics and trims and use a beautiful branded woven label to finish each of our garters. 


A Laura George wedding garter will always look stunning when worn. This is because we believe in perfect fit and understand how important it is to a bride on her wedding day. Every detail must be perfect and this is why our gorgeous garters are available in five sizes. 

All of our wedding garters come packaged in a beautiful branded gift box making them a perfect keepsake.','95.00','10','1,2,3,4,5' ), 
 ('151','1','Alexandra','This beautiful wedding garter is hand made from a stunning ivory Guipure embroidery. Highlighted with hand tied satin ribbon bow and crystal pendant.

Laura George is a British luxury brand. Each one of our stunning wedding garters is inspired, designed and handmade in the UK. We start all our designs with our luxurious signature plush backed elastic. This makes our wedding garters unique from any others. It is super soft to touch, feels amazing when worn and gives a special finish to our fabulous products. Every element is thought about. We only use the highest quality of fabrics and trims and use a beautiful branded woven label to finish each of our garters. 


A Laura George wedding garter will always look stunning when worn. This is because we believe in perfect fit and understand how important it is to a bride on her wedding day. Every detail must be perfect and this is why our gorgeous garters are available in five sizes. 

All of our wedding garters come packaged in a beautiful branded gift box making them a perfect keepsake.','65.00','10','1,2,3,4,5' ), 
 ('142','1','Martha','Iris is one of our latest super skinny wedding garters. Handmade from a beautiful lace trim that is no more than 15 mm wide. Embellished with a perfectly hand tied satin bow.

This delicate lace garter is perfect for under tight fitting dresses or for a touch of something blue. Available in white, ivory and baby blue.

Laura George is a British luxury brand. Each one of our stunning wedding garters is inspired, designed and handmade in the UK. We start all our designs with our luxurious signature plush backed elastic. This makes our wedding garters unique from any others. It is super soft to touch, feels amazing when worn and gives a special finish to our fabulous products. Every element is thought about. We only use the highest quality of fabrics and trims and use a beautiful branded woven label to finish each of our garters. 


A Laura George wedding garter will always look stunning when worn. This is because we believe in perfect fit and understand how important it is to a bride on her wedding day. Every detail must be perfect and this is why our gorgeous garters are available in five sizes. 

All of our wedding garters come packaged in a beautiful branded gift box making them a perfect keepsake.','25.00','9,10,11','1,2,3,4,5' ), 
 ('143','1','Fairytale','This gorgeous garter is made from a delicate ivory lace and highlighted with blue satin bow and Swarovski crystal.

Laura George is a British luxury brand. Each one of our stunning wedding garters is inspired, designed and handmade in the UK. We start all our designs with our luxurious signature plush backed elastic. This makes our wedding garters unique from any others. It is super soft to touch, feels amazing when worn and gives a special finish to our fabulous products. Every element is thought about. We only use the highest quality of fabrics and trims and use a beautiful branded woven label to finish each of our garters. 


A Laura George wedding garter will always look stunning when worn. This is because we believe in perfect fit and understand how important it is to a bride on her wedding day. Every detail must be perfect and this is why our gorgeous garters are available in five sizes. 

All of our wedding garters come packaged in a beautiful branded gift box making them a perfect keepsake.','28.00','10','1,2,3,4,5' ), 
 ('144','1','Octavia','This gorgeous garter is truly stunning. With sequins, pearls and beaded embroidery combined with our luxurious silk.

Laura George is a British luxury brand. Each one of our stunning wedding garters is inspired, designed and handmade in the UK. We start all our designs with our luxurious signature plush backed elastic. This makes our wedding garters unique from any others. It is super soft to touch, feels amazing when worn and gives a special finish to our fabulous products. Every element is thought about. We only use the highest quality of fabrics and trims and use a beautiful branded woven label to finish each of our garters. 


A Laura George wedding garter will always look stunning when worn. This is because we believe in perfect fit and understand how important it is to a bride on her wedding day. Every detail must be perfect and this is why our gorgeous garters are available in five sizes. 

All of our wedding garters come packaged in a beautiful branded gift box making them a perfect keepsake.','65.00','9,10','1,2,3,4,5' ), 
 ('145','1','Dahlia','This gorgeous garter is made from stunning french Chantilly lace. Highlighted with ivory organza bow and crystal trinket.

Laura George is a British luxury brand. Each one of our stunning wedding garters is inspired, designed and handmade in the UK. We start all our designs with our luxurious signature plush backed elastic. This makes our wedding garters unique from any others. It is super soft to touch, feels amazing when worn and gives a special finish to our fabulous products. Every element is thought about. We only use the highest quality of fabrics and trims and use a beautiful branded woven label to finish each of our garters. 


A Laura George wedding garter will always look stunning when worn. This is because we believe in perfect fit and understand how important it is to a bride on her wedding day. Every detail must be perfect and this is why our gorgeous garters are available in five sizes. 

All of our wedding garters come packaged in a beautiful branded gift box making them a perfect keepsake.','75.00','9,10','1,2,3,4,5' ), 
 ('146','1','Lana','This beautiful garter is made from 100% silk and highlighted with a stunning crystal button.

Laura George is a British luxury brand. Each one of our stunning wedding garters is inspired, designed and handmade in the UK. We start all our designs with our luxurious signature plush backed elastic. This makes our wedding garters unique from any others. It is super soft to touch, feels amazing when worn and gives a special finish to our fabulous products. Every element is thought about. We only use the highest quality of fabrics and trims and use a beautiful branded woven label to finish each of our garters. 


A Laura George wedding garter will always look stunning when worn. This is because we believe in perfect fit and understand how important it is to a bride on her wedding day. Every detail must be perfect and this is why our gorgeous garters are available in five sizes. 

All of our wedding garters come packaged in a beautiful branded gift box making them a perfect keepsake.','58.00','9,10','1,2,3,4,5' ), 
 ('147','1','Lexi','A beautiful wedding garter made from layer of spot net tulle and highlighted with large hand tied satin blue bow.

Laura George is a British luxury brand. Each one of our stunning wedding garters is inspired, designed and handmade in the UK. We start all our designs with our luxurious signature plush backed elastic. This makes our wedding garters unique from any others. It is super soft to touch, feels amazing when worn and gives a special finish to our fabulous products. Every element is thought about. We only use the highest quality of fabrics and trims and use a beautiful branded woven label to finish each of our garters. 


A Laura George wedding garter will always look stunning when worn. This is because we believe in perfect fit and understand how important it is to a bride on her wedding day. Every detail must be perfect and this is why our gorgeous garters are available in five sizes. 

All of our wedding garters come packaged in a beautiful branded gift box making them a perfect keepsake.','45.00','9,10','1,2,3,4,5' ), 
 ('148','1','Betty','Betty is one of our most gorgeous new designs made from layers of silk tulle, lace and delicate organza blue bow. Highlighted with two tiny pearls

Laura George is a British luxury brand. Each one of our stunning wedding garters is inspired, designed and handmade in the UK. We start all our designs with our luxurious signature plush backed elastic. This makes our wedding garters unique from any others. It is super soft to touch, feels amazing when worn and gives a special finish to our fabulous products. Every element is thought about. We only use the highest quality of fabrics and trims and use a beautiful branded woven label to finish each of our garters. 


A Laura George wedding garter will always look stunning when worn. This is because we believe in perfect fit and understand how important it is to a bride on her wedding day. Every detail must be perfect and this is why our gorgeous garters are available in five sizes. 

All of our wedding garters come packaged in a beautiful branded gift box making them a perfect keepsake.','65.00','9,10','1,2,3,4,5' ), 
 ('149','1','Cricket','This wedding garter is lovingly handmade from beautiful layers of super soft luxurious silk tulle. A stunning floral embroidery then embellished this wedding garter, with flowers highlighted with beautiful, delicate beading.

Laura George is a British luxury brand. Each one of our stunning wedding garters is inspired, designed and handmade in the UK. We start all our designs with our luxurious signature plush backed elastic. This makes our wedding garters unique from any others. It is super soft to touch, feels amazing when worn and gives a special finish to our fabulous products. Every element is thought about. We only use the highest quality of fabrics and trims and use a beautiful branded woven label to finish each of our garters. 


A Laura George wedding garter will always look stunning when worn. This is because we believe in perfect fit and understand how important it is to a bride on her wedding day. Every detail must be perfect and this is why our gorgeous garters are available in five sizes. 

All of our wedding garters come packaged in a beautiful branded gift box making them a perfect keepsake.','65.00','10','1,2,3,4,5' ); 
 -- Struktur Tabel sizes -------------------- 
CREATE TABLE IF NOT EXISTS `sizes` ( 
 `id` int(11) NOT NULL auto_increment, 
`name` varchar(20) NOT NULL  , 
PRIMARY KEY (`id`) 
 ); 
-- Data Tabel sizes -------------------- 
INSERT INTO `sizes` (`id`, `name` ) VALUES ('1','XS' ), 
 ('2','S' ), 
 ('3','M' ), 
 ('4','L' ), 
 ('5','XL' ), 
 ('6','6' ), 
 ('7','8' ), 
 ('8','10' ), 
 ('9','12' ), 
 ('10','14' ), 
 ('11','16' ), 
 ('12','18' ), 
 ('13','20' ), 
 ('14','22' ), 
 ('15','24' ), 
 ('16','30A' ), 
 ('17','30B' ), 
 ('18','30C' ), 
 ('19','30D' ), 
 ('20','30DD' ), 
 ('21','32A' ), 
 ('22','32B' ), 
 ('23','32C' ), 
 ('24','32D' ), 
 ('25','32DD' ), 
 ('26','32E' ), 
 ('27','32F' ), 
 ('28','32G' ), 
 ('29','34A' ), 
 ('30','34B' ); 
 INSERT INTO `sizes` (`id`, `name` ) VALUES ('31','34C' ), 
 ('32','34D' ), 
 ('33','34DD' ), 
 ('34','34E' ), 
 ('35','34F' ), 
 ('36','34G' ), 
 ('37','36A' ), 
 ('38','36B' ), 
 ('39','36C' ), 
 ('40','36D' ), 
 ('41','36DD' ), 
 ('42','36E' ), 
 ('43','36F' ), 
 ('44','36G' ), 
 ('45','38A' ), 
 ('46','38B' ), 
 ('47','38C' ), 
 ('48','38D' ), 
 ('49','38DD' ), 
 ('50','38E' ), 
 ('51','38F' ), 
 ('52','38G' ), 
 ('53','40A' ), 
 ('54','40B' ), 
 ('55','40C' ), 
 ('56','40D' ), 
 ('57','40DD' ), 
 ('58','42A' ), 
 ('59','42B' ), 
 ('60','42C' ); 
 INSERT INTO `sizes` (`id`, `name` ) VALUES ('61','42D' ), 
 ('62','42DD' ), 
 ('63','ONE SIZE' ); 
 -- Struktur Tabel vouchers -------------------- 
CREATE TABLE IF NOT EXISTS `vouchers` ( 
 `id` int(11) NOT NULL auto_increment, 
`code` varchar(20) NOT NULL , 
`percent` varchar(3) NOT NULL , 
`price` decimal(10,2) NOT NULL , 
`active` set('Y','N') NOT NULL  , 
PRIMARY KEY (`id`) 
 ); 
-- Data Tabel vouchers -------------------- 
INSERT INTO `vouchers` (`id`, `code`, `percent`, `price`, `active` ) VALUES ('4','PERCENTOFF','7','0.00','N' ), 
 ('8','BB20','0','0.00','N' ), 
 ('7','BB25','25','0.00','N' ), 
 ('9','FACEBOOK','25','0.00','Y' ), 
 ('10','EasyWeddingSearch','10','0.00','Y' ), 
 ('11','KNICKERSBLOG','20','0.00','Y' ), 
 ('12','IDEAS','20','0.00','Y' ), 
 ('13','TWITTER','20','0.00','Y' ), 
 ('14','lingerieblog','20','0.00','Y' ), 
 ('15','berkeley','75','0.00','N' ), 
 ('16','PERFECT','20','0.00','Y' ), 
 ('17','DREAMS','75','0.00','N' ), 
 ('18','Mrs2Be','10','0.00','Y' ), 
 ('19','STCLEVELEYS','75','0.00','N' ), 
 ('20','STPLUMS','62','0.00','Y' ), 
 ('21','WEDDING','10','0.00','Y' ), 
 ('22','STGRAZIA','75','0.00','N' ), 
 ('23','STDARCY','75','0.00','N' ), 
 ('24','WEDDING GUIDE','10','0.00','Y' ), 
 ('25','STFELICITY','75','0.00','N' ), 
 ('26','BRIDALMAG','15','0.00','Y' ), 
 ('27','REPEAT','20','0.00','Y' ), 
 ('28','carl','20','0.00','Y' ), 
 ('29','STSANDERSON','75','0.00','N' ), 
 ('30','STANNBRIDAL','62','0.00','Y' ), 
 ('31','STPRETTY','75','0.00','N' ), 
 ('32','CURVY','62','0.00','Y' ), 
 ('33','STPLATINUM','75','0.00','N' ), 
 ('34','STBRIDALROOM','62','0.00','Y' ), 
 ('35','BRIDES','20','0.00','Y' ); 
 INSERT INTO `vouchers` (`id`, `code`, `percent`, `price`, `active` ) VALUES ('36','STSTATELYBRIDES','62','0.00','N' ), 
 ('37','STBERKELEY','62','0.00','Y' ), 
 ('38','FREEPOST','','2.50','N' ), 
 ('39','APRIL10','10','0.00','N' ), 
 ('40','STBRIDEZILLAS','62','0.00','Y' ), 
 ('41','STJENNY','62','0.00','Y' ), 
 ('42','STVICKI','62','0.00','Y' ), 
 ('43','BCDONNA','60','0.00','Y' ), 
 ('44','STHAYLEY','62','0.00','Y' ), 
 ('45','STIVORYANDLACE','62','0.00','Y' ), 
 ('46','STMRSJONES','62','0.00','Y' ), 
 ('47','STELLIE','62','0.00','Y' ), 
 ('48','STSTATELY','62','0.00','Y' ); 
 